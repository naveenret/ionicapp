import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

export class TokenInterceptors implements HttpInterceptor {

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        console.log("interceptor called");
        request = request.clone({
            setParams: {
                // 'appKey': JSON.parse(localStorage.getItem('appKey')); // TODO : WILL SET IT LATER 
                'appKey': '22b4e041-2a59-41e4-a954-7d2dce3a1268',
            },
            setHeaders: {
                'Access-Control-Allow-Origin': '*',
                'Access-Control-Allow-Methods': 'GET, POST, OPTIONS, PUT',
                'Content-Type': 'application/json',
                'Accept': 'application/json',
            }
        })
        console.log({ request });
        return next.handle(request).pipe(
            map(res => {
                return res
            }),
            catchError(err => {
                return throwError(err)
            })
        )
    }

}