import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'time'
})
export class TimePipe implements PipeTransform {

  transform(value: any, ...args: any[]): any {
    console.log(args);
    if(args.length >0){
      let time = moment(value).format(args[0]);
      console.log({time});
      return time;
    }
    let time = moment(value).format('hh:mm');
    return time;

  }

}
