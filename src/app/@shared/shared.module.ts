import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { ChartsModule } from 'ng2-charts';
import { DateFormatPipe } from './pipes/date-format.pipe';
import { TimePipe } from './pipes/time.pipe';
import { SafeUrlPipe } from './pipes/safe-url.pipe';
import { IonRefreshNativeModule } from 'ion-refresh-native';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    ChartsModule,
    IonRefreshNativeModule
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    ChartsModule,
    DateFormatPipe,
    TimePipe,
    SafeUrlPipe,
    IonRefreshNativeModule
  ],
  declarations: [DateFormatPipe, TimePipe, SafeUrlPipe]
})
export class SharedModule { }
