import { Component, OnInit, Input, ViewChild, ElementRef, OnChanges, SimpleChanges, Renderer2 } from '@angular/core';

@Component({
  selector: 'nk-progress-circle',
  templateUrl: './nk-progress-circle.component.html',
  styleUrls: ['./nk-progress-circle.component.scss'],
})
export class NkProgressCircleComponent implements OnInit, OnChanges {

  @Input() width: string = '120';
  @Input() height: string = '120';
  @Input() strokeWidth: string = '12';
  @Input() value: any = 0;
  @Input() radius: number = 50;
  @Input() strokeColor: string = '#E1342F';
  @Input() textValue: string = 'ANDE';
  @Input() textStyle: any = {};


  viewBox: any = `0 0 ${this.width} ${this.height}`;
  cx: any = parseFloat(this.width) / 2;
  cy: any = parseFloat(this.height) / 2;
  circumference = 2 * Math.PI * this.radius;
  dashoffset: number;
  constructor() {
    this.progress(this.value);
  }

  ngOnInit() {
    //  TODO : DEMO ONLY REMOVE IN PRODUCTION 
    // setTimeout(() => {
    //   setInterval(() => {
    //     this.value = Math.floor(Math.random() * 70) + 30;
    //     this.progress(this.value);
    //   }, 1000)
    // }, 1000);
  }

  ngOnChanges(changes: SimpleChanges) {
    this.viewBox = `0 0 ${this.width} ${this.height}`;
    this.cx = parseFloat(this.width) / 2;
    this.cy = parseFloat(this.height) / 2;
    this.circumference = 2 * Math.PI * this.radius;

    console.log("this.strokeColor", this.strokeColor)
    if (!this.strokeColor) {
      if (changes.value.currentValue > 75) {
        this.strokeColor = "#4F7942";
      }
      else if (changes.value.currentValue <= 75 && changes.value.currentValue > 50) {
        this.strokeColor = "#FFBA3A";
      }
      else if (changes.value.currentValue <= 50) {
        this.strokeColor = "#E1342F";
      }
    }

    // if (changes.value.previousValue != changes.value.currentValue) {
    setTimeout(() => {
      this.progress(changes.value.currentValue);
    }, 1000)
    // }
  }

  private progress(value: number) {
    const progress = value / 100;
    this.dashoffset = this.circumference * (1 - progress);
  }
}
