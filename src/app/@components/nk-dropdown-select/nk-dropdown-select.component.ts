import { Component, OnInit, DoCheck, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'nk-dropdown-select',
  templateUrl: './nk-dropdown-select.component.html',
  styleUrls: ['./nk-dropdown-select.component.scss'],
})
export class NkDropdownSelectComponent implements OnInit, DoCheck {
  forwardIcon: any = true;
  downIcon: any = false;
  dropdownList: any = false;
  dropdownTitle: any;
  items: any;
  @Input('itemsarray') itemsarray: any[] = [];
  @Input('value') value: any;
  @Output() onSelect = new EventEmitter();
  constructor() {
  }
  
  ngOnChanges() {
    console.log(this.itemsarray);
    this.items = this.itemsarray;
    console.log(this.value);
    console.log('NK_DROPDOWN_ITEMS_ARRAY',this.itemsarray)
    console.log('NK_DROPDOWN_ITEMS_ARRAY',this.items)
    
    // ;
    this.dropdownTitle = this.value && this.value.ProcessDisplayName || this.value && this.value!.label || this.value && this.value!.Name || this.value || '';
    
    if(this.value){
      this.onSelect.emit(this.value);
    }
    
  }
  
  
  ngOnInit() {
    // console.log(this.itemsarray);
    // console.log(this.items);
    // this.dropdownTitle = this.value && this.value.ProcessDisplayName || this.value && this.value!.label || this.value;
    // console.log(this.dropdownTitle);
    // let a = this.items.length; 
    // // this.items = this.items.slice(0, a + 1);
  }
  ngDoCheck() {
    let a = this.items.length;
    this.items = this.items.slice(0, a + 1);
    console.log('NK_DROPDOWN_ITEMS_ARRAY_DOCHECK',this.itemsarray)
    console.log('NK_DROPDOWN_ITEMS_ARRAY_DOCHECK',this.items)
  }
  arrowClick() {
    if (this.forwardIcon == true) {
      this.forwardIcon = false;
      this.downIcon = true;
      this.dropdownList = true;
    } else {
      this.downIcon = false;
      this.forwardIcon = true;
      this.dropdownList = false;
    }
  }
  dropdownClick(item, index) {
    this.dropdownTitle = item!.ProcessDisplayName || item!.Name || item!.label || this.value;
    this.onSelect.emit(item);
    this.items.splice(index, 1);
    this.items.unshift(item);
    this.dropdownList = false;
    this.arrowClick();
  }

  closeMenu(event) {
    console.log(event.path)
    if (this.checkPathThatContainSelector(event.path, 'nk-dropdown-area')) {
      return;
    }
    console.log('click outside');
    // this.toggleMenu();
    this.dropdownList = false;
    this.arrowClick();
  }

  checkPathThatContainSelector(path, selector) {
    for (let i = 0; i < path.length; i++) {
      if (path[i].classList && path[i].classList.contains(selector)) {
        return true;
      }
    }
    return false;
  }

}
