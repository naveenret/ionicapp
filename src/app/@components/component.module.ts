import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { SharedModule } from '../@shared/shared.module';
import { NkProgressCircleComponent } from './nk-progress-circle/nk-progress-circle.component';
import { NkPercentageBarComponent } from './nk-percentage-bar/nk-percentage-bar.component';
import { NkDropdownSelectComponent } from './nk-dropdown-select/nk-dropdown-select.component';

const arr = [
  NkProgressCircleComponent,
  NkPercentageBarComponent,
  NkDropdownSelectComponent
]
@NgModule({
  imports: [
    CommonModule,
    IonicModule,
    SharedModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [arr],
  exports: [arr]
})
export class ComponentModule { }
