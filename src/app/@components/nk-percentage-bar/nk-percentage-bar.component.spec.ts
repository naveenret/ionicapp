import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NkPercentageBarComponent } from './nk-percentage-bar.component';

describe('NkPercentageBarComponent', () => {
  let component: NkPercentageBarComponent;
  let fixture: ComponentFixture<NkPercentageBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NkPercentageBarComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NkPercentageBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
