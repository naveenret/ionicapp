import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';


@Component({
  selector: 'nk-percentage-bar',
  templateUrl: './nk-percentage-bar.component.html',
  styleUrls: ['./nk-percentage-bar.component.scss'],
})
export class NkPercentageBarComponent implements OnInit, OnChanges {

  constructor() { }

  @Input() label: string = ' ';
  @Input() percentage: any = ' ';
  @Input() height: string = ' ';
  @Input() color: string = ' ';

  ngOnChanges(changes: SimpleChanges) {
    if (changes.percentage.currentValue > 75) {
      this.color = "#4F7942";
    }
    else if (changes.percentage.currentValue <= 75 && changes.percentage.currentValue > 50) {
      this.color = "#FFBA3A";
    }
    else if (changes.percentage.currentValue <= 50) {
      this.color = "#E1342F";
    }
  }
  ngOnInit() {
    //  TODO : DEMO ONLY REMOVE IN PRODUCTION 
    //  setTimeout(() => {
    //    setInterval(()=>{
    //    this.percentage = Math.floor(Math.random() * 70) +30;
    //    },1000)
    //  }, 1000);
  }
  // OLD
  // ngOnInit() {}
  // innerDivStyle:any={
  //   'background-color':'#88eba2',
  //   'width':'20%'
  // }
  // outerDivStyle:any={
  //   'height':'5px',
  //   'background-color':'#e6e2e2'
  // }
  // @Input('color') color:any;
  // @Input('percentage') percentage:any;
  // @Input('height') height:any;
  // ngOnChanges(){
  //   console.log(this.color,this.percentage);
  //   this.innerDivStyle["background-color"]=this.color;
  //   this.innerDivStyle["width"]=this.percentage+'%';
  //   this.outerDivStyle["height"]=this.height+'px';
  // }

}
