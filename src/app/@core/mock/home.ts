export const getMechineListDummy = {
    "rows": [
        {
            "IsClickable": true,
            "MachineStatus": true,
            "MachineThingName": "BConfig_Machine_AssemblyLine",
            "MahineDisplayName": "Assembly Line",
            "NavigateTo": "OEE",
            "SNo": 1
        },
        {
            "IsClickable": false,
            "MachineStatus": false,
            "MachineThingName": "--",
            "MahineDisplayName": "Pre Treatment",
            "NavigateTo": "Condition Monitoring",
            "SNo": 2
        },
        {
            "IsClickable": false,
            "MachineStatus": false,
            "MachineThingName": "--",
            "MahineDisplayName": "Water Drying Oven",
            "NavigateTo": "Condition Monitoring",
            "SNo": 3
        },
        {
            "IsClickable": false,
            "MachineStatus": false,
            "MachineThingName": "--",
            "MahineDisplayName": "Manual Touch-Up Booth",
            "NavigateTo": "Condition Monitoring",
            "SNo": 4
        },
        {
            "IsClickable": true,
            "MachineStatus": true,
            "MachineThingName": "BConfig_Machine_RoboPaint",
            "MahineDisplayName": "Robo Paint Booth",
            "NavigateTo": "OEE",
            "SNo": 5
        },
        {
            "IsClickable": false,
            "MachineStatus": false,
            "MachineThingName": "--",
            "MahineDisplayName": "Flash Off Zone",
            "NavigateTo": "Condition Monitoring",
            "SNo": 6
        },
        {
            "IsClickable": false,
            "MachineStatus": false,
            "MachineThingName": "--",
            "MahineDisplayName": "Paint Baking Oven",
            "NavigateTo": "Condition Monitoring",
            "SNo": 7
        },
        {
            "IsClickable": false,
            "MachineStatus": false,
            "MachineThingName": "--",
            "MahineDisplayName": "Cooling Zone",
            "NavigateTo": "Condition Monitoring",
            "SNo": 8
        },
        {
            "IsClickable": true,
            "MachineStatus": false,
            "MachineThingName": "BConfig_Machine_PackagingConveyor",
            "MahineDisplayName": "Packing Conveyor",
            "NavigateTo": "OEE",
            "SNo": 9
        }
    ]
}

export var getMechineDetailDummy = {
    "rows": [
        {
            "AskingRate": "0/Hr",
            "DailyActual": 95.0,
            "DailyPlan": 154.0,
            "DownTime": "00:14",
            "ProrataActual": 6,
            "ProrataTarget": 24,
            "RejectionCount": 2,
            "ShiftDate": 1583433000000,
            "ShiftName": "Shift2"
        }
    ]

}

export var getRejectionDetailDataDummy = {
    "rows": [
        {
            "Customer": "AL",
            "HSG_PartNo": "C3121EC1149",
            "HSG_SerialNo": "MH20C009705",
            "NX_PartNo": "MS14160NAT060617",
            "NX_SerialNo": "MA20C008025",
            "Rejected_At": "Bolt/Nut Driving Manual",
            "Rejected_By": "MohanSV",
            "Rejected_Reason": "bolt wasout",
            "Rejected_Time": 1583486634276,
            "SNo": 1
        },
        {
            "Customer": "A/L",
            "HSG_PartNo": "C3121JC894",
            "HSG_SerialNo": "MH20B006230",
            "NX_PartNo": "MD131497NZ790617",
            "NX_SerialNo": "MA20C008028",
            "Rejected_At": "Sealant Removal",
            "Rejected_By": "MohanSV",
            "Rejected_Reason": "air chamber leak",
            "Rejected_Time": 1583488415027,
            "SNo": 2
        }
    ]

}

export var getDowntimeDetailDataDummy = {
    "rows": [
        {
            "Alert": "P2",
            "AlertDurationInMinutes": 0.0,
            "AlertDurationInSeconds": 0.0,
            "AlertEndTime": 1583490800646,
            "AlertStartTime": 1583490800646,
            "Reason": "N/A",
            "SNo": 1
        },
        {
            "Alert": "ET",
            "AlertDurationInMinutes": 0.08828333333333332,
            "AlertDurationInSeconds": 5.297,
            "AlertEndTime": 1583490800646,
            "AlertStartTime": 1583490795349,
            "Reason": "N/A",
            "SNo": 2
        }
    ]

}
export var getPlantPerformance={
    "rows": [
        {
            "Availability": 40,
            "GreenColourCode": "#4F7942",
            "OLE": 14,
            "Performance": 42,
            "Quality": 86,
            "RedColourCode": "#E1342F",
            "RedColourValueTill": 50,
            "YellowColourCode": "#FFBA3A",
            "YellowColourValueTill": 75
        }
    ]

}