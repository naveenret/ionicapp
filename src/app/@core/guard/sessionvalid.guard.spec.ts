import { TestBed, async, inject } from '@angular/core/testing';

import { SessionvalidGuard } from './sessionvalid.guard';

describe('SessionvalidGuard', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SessionvalidGuard]
    });
  });

  it('should ...', inject([SessionvalidGuard], (guard: SessionvalidGuard) => {
    expect(guard).toBeTruthy();
  }));
});
