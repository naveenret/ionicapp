import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { NavController } from '@ionic/angular';
import { SessionExpired } from '../properties/session';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardGuard implements CanActivate {

  constructor(public navCtrl: NavController) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    console.log(JSON.parse(localStorage.getItem('isUserLoggedIn')));
    let value = JSON.parse(localStorage.getItem('isUserLoggedIn')) || false;

    console.log('session check', SessionExpired.checkSessionValid());
    let isSessionValid = SessionExpired.checkSessionValid() || false;
    if (isSessionValid) {
      if (!value) {
        this.navCtrl.navigateRoot('/login');
        return false;
      }
      return true
    } else {
      this.navCtrl.navigateRoot('/session-expired');
      return false;
    }
  }

}
