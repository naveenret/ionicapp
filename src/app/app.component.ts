import { Component, OnInit, OnDestroy, AfterViewInit } from '@angular/core';

import { Platform, NavController, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AlertController } from '@ionic/angular';
import { Router } from '@angular/router';
import { QRScanner } from '@ionic-native/qr-scanner/ngx';

declare var $: any;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit, OnDestroy, AfterViewInit {

  backButtonSubscription: any;
  isBackEnable: boolean = false;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public toastController: ToastController,
    public router: Router,
    public navCtrl: NavController,
    public qrscanner: QRScanner,

  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.backgroundColorByHexString('#872e2d');
      this.splashScreen.hide();
    });
  }

  ngOnInit() { }
  ngAfterViewInit() {
    this.backButtonAction();
  }

  backButtonAction() {
    this.backButtonSubscription = this.platform.backButton.subscribe(() => {
      // this.hideIfQrCameraIsVisibled(); //? TO DO : NO NEED BUT ....
      let currentPath = window!.location!.pathname || '';
      var activeBs: any = document.querySelectorAll('.modal.show');
      if (activeBs.length > 0) {
        for (var i = 0; i < activeBs.length; i++) {
          console.log(activeBs[i].id);
          $('#' + activeBs[i].id).modal('hide');
        }
      }
      else if (currentPath.toLowerCase() == '/login' ||
        currentPath.toLowerCase() == '/layouts/home') {
        if (navigator['app']) {
          console.log(navigator['app']);
          if (this.isBackEnable) {
            navigator['app'].exitApp();
            return;
          }
          this.presentToast();
        }
      } else if (
        currentPath.toLowerCase() == '/layouts/production' ||
        currentPath.toLowerCase() == '/layouts/condition-monitoring' ||
        currentPath.toLowerCase() == '/layouts/traceability' ||
        currentPath.toLowerCase() == '/layouts/analytics' ||
        currentPath.toLowerCase() == '/layouts/planning' ||
        currentPath.toLowerCase() == '/layouts/summary'
      ) {
        // this.router.navigate(['/layouts']);
        this.navCtrl.navigateRoot('/layouts');
      }
    });
  }
  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Press back again to exit.',
      duration: 1000,
      animated: true,
      color: 'secondary',
      mode: 'ios'
    });
    toast.present();
    this.isBackEnable = true;
    setTimeout(() => {
      this.isBackEnable = false;
    }, 1000)
  }


  hideIfQrCameraIsVisibled() {
    this.qrscanner.getStatus().then(res => {
      if (res.scanning || res.showing || res.previewing) {
        this.qrscanner.destroy().then(res => {
          console.log(res);
          var ionApp = <HTMLElement>document.getElementsByTagName("ion-app")[0];
          ionApp.style.display = 'block';
        }).catch(err => {
          console.log(err);
        })
      }
    });
  }


  ngOnDestroy() {
    this.backButtonSubscription.unsubscribe();
  }


}
