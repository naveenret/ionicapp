import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuardGuard } from './@core/guard/auth-guard.guard';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full' 
  }, 
  {
    path: 'login',
    loadChildren: () => import('./@pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'layouts',
    loadChildren: () => import('./@layouts/layout/layout.module').then( m => m.LayoutPageModule),
    canActivate:[AuthGuardGuard]
  },
  {
    path: 'notification',
    loadChildren: () => import('./@pages/notification/notification.module').then( m => m.NotificationPageModule),
    canActivate:[AuthGuardGuard]
  },
  {
    path: 'downtime-reason',
    loadChildren: () => import('./@pages/downtime-reason/downtime-reason.module').then( m => m.DowntimeReasonPageModule),
    canActivate:[AuthGuardGuard]
  },
  {
    path: 'rejection-reason',
    loadChildren: () => import('./@pages/rejection-reason/rejection-reason.module').then( m => m.RejectionReasonPageModule),
    canActivate:[AuthGuardGuard]
  },  {
    path: 'session-expired',
    loadChildren: () => import('./@core/page/session-expired/session-expired.module').then( m => m.SessionExpiredPageModule)
  },

 



];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
