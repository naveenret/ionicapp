import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { DatePicker } from '@ionic-native/date-picker/ngx';

import { ChartsModule } from 'ng2-charts';

import { QRScanner } from '@ionic-native/qr-scanner/ngx';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TokenInterceptors } from './@shared/interceptors/TokenInterceptor';
import { SessionExpired } from './@core/properties/session';
import { HTTP } from '@ionic-native/http/ngx';

import { IonRefreshNativeModule } from 'ion-refresh-native';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    ChartsModule,
    HttpClientModule,
    IonRefreshNativeModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    DatePicker,
    QRScanner,
    HTTP,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    SessionExpired,
    { provide: HTTP_INTERCEPTORS, useClass: TokenInterceptors, multi: true }
  ], 
  bootstrap: [AppComponent]
})
export class AppModule { }
