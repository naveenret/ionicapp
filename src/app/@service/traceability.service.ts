import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { HTTP, HTTPResponse } from '@ionic-native/http/ngx';


@Injectable({
  providedIn: 'root'
})
export class TraceabilityService {


  constructor(public httpClient: HttpClient, public http: HTTP) { }

  setHeaders() {
    this.http.setHeader('*', 'Access-Control-Allow-Origin', '*');
    this.http.setHeader('*', 'Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    this.http.setHeader('*', 'Accept', 'application/json');
    this.http.setHeader('*', 'content-type', 'application/json');
    //Important to set the data serializer or the request gets rejected
    this.http.setDataSerializer('json');
  }

  getTraceabilityList(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + '/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetTraceabilityList?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268';
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }


  getTraceabilityPartImage(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + '/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetPartImageandStatus?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268';
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }



  getTraceabilityDetails(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + '/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetTraceabilityDetails?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268';
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }

  getDifferentialDetails(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + '/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetDiffCheckDetails?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268';
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }

  getAssemblyDetails(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + '/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetAssemblyDetails?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268';
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }

  getEmployeeDetails(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + '/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetOperatorDetails?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268';
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }

  getNutRunnerDetails(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + '/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetNutRunnerDetails?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268';
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }

  getAirLeakDetails(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + '/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetAirLeakDetails?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268';
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }

  getPostAssemblyDetails(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + '/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetPostAssemblyDetails?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268';
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }
  getPostAssemblyParameterDetails(data): Promise<HTTPResponse> {
    this.setHeaders();

    let url = environment.apiUrl + '/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetPostAssemblyParameterList?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268';
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }

  getPostAssemblyParameterDetailsChartData(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + '/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetParameterGraph?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268';
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }

}
