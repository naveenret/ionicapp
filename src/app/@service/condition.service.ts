import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { map, catchError } from 'rxjs/operators';
import { HTTP, HTTPResponse } from '@ionic-native/http/ngx';

@Injectable({
  providedIn: 'root'
})
export class ConditionService {


  constructor(public httpClient: HttpClient, public http: HTTP) { }


  setHeaders() {
    this.http.setHeader('*', 'Access-Control-Allow-Origin', '*');
    this.http.setHeader('*', 'Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    this.http.setHeader('*', 'Accept', 'application/json');
    this.http.setHeader('*', 'content-type', 'application/json');
    //Important to set the data serializer or the request gets rejected
    this.http.setDataSerializer('json');
  }

  getPostAssemblyMechineList(): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + `/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetPostassemblyMachineList?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268`;
    return new Promise((resolve, reject) => {
      this.http.post(url, {}, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }

  getPostAssemblyMechineParameter(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + `/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetPostAssemblyMachineParameters?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268`;
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }

  getPostAssemblyTrendMechineParameter(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + `/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetPostAssemblyTrendMachineParameters?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268`;
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }

  getTrendDetails(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + `/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetParameterTrend?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268`;
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }

  getProcessImage(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + `/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetPostAssemblyMachineImage?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268`;
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }
}
