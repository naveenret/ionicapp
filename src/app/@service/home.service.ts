import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { HTTPResponse, HTTP } from '@ionic-native/http/ngx';

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(public httpClient: HttpClient, public http: HTTP) { }

  setHeaders(){
    this.http.setHeader('*', 'Access-Control-Allow-Origin', '*');
    this.http.setHeader('*', 'Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    this.http.setHeader('*', 'Accept', 'application/json');
    this.http.setHeader('*', 'content-type', 'application/json');
    //Important to set the data serializer or the request gets rejected
    this.http.setDataSerializer('json');
  }

  getMechineStatusList(): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + `/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetAllMachineList?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268`;
    return new Promise((resolve, reject) => {
      this.http.post(url, {}, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }

  getMechineData(): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + `/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetMachineDetails?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268`;
    return new Promise((resolve, reject) => {
      this.http.post(url, {}, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
    // return of(getMechineDetailDummy);
  }

  getRejectionDetailData(): Promise<HTTPResponse> {

    this.setHeaders();
    let url = environment.apiUrl + `/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetRejectionDetails?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268`;
    return new Promise((resolve, reject) => {
      this.http.post(url, {}, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
    // return of(getRejectionDetailDataDummy);
  }

  getDowntimeDetailList(): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + `/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetDowntimeReason?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268`;
    return new Promise((resolve, reject) => {
      this.http.post(url, {}, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
    // return of(getDowntimeDetailDataDummy);
  }

  getPlantPerformance(): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + `/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetPlantPerformanceValues?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268`;
    return new Promise((resolve, reject) => {
      this.http.post(url, {}, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
    // return of(getPlantPerformance);
  }
}
