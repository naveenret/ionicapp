import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { HTTP, HTTPResponse } from '@ionic-native/http/ngx';

@Injectable({
  providedIn: 'root'
})
export class AnalyticsService {

  constructor(public httpClient: HttpClient, public http: HTTP) { }

  
  setHeaders(){
    this.http.setHeader('*', 'Access-Control-Allow-Origin', '*');
    this.http.setHeader('*', 'Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    this.http.setHeader('*', 'Accept', 'application/json');
    this.http.setHeader('*', 'content-type', 'application/json');
    //Important to set the data serializer or the request gets rejected
    this.http.setDataSerializer('json');
  }

  getAnalyticsHourlyData(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + `/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetAnalyticsHourlyGraphData?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268`;
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }
  getAnalyticsProductionCount(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + `/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetAnalyticsProductionCount?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268`;
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }

  getDowntimePieChart(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + `/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetDowntimePieChart?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268`;
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }

  getShiftListMultipleDate(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + `/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetPlanningShiftDetailsBasedOndate?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268`;
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }

  getTopDowntimeMultipleDate(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + `/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetTop5DowntimeReasonformultiDate?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268`;
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }
}
