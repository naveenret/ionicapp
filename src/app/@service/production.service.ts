import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Config } from '@ionic/angular';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { HTTP, HTTPResponse } from '@ionic-native/http/ngx';


@Injectable({
  providedIn: 'root'
})
export class ProductionService {


  constructor(public httpClient: HttpClient, public http: HTTP) { }

  setHeaders() {
    this.http.setHeader('*', 'Access-Control-Allow-Origin', '*');
    this.http.setHeader('*', 'Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT');
    this.http.setHeader('*', 'Accept', 'application/json');
    this.http.setHeader('*', 'content-type', 'application/json');
    //Important to set the data serializer or the request gets rejected
    this.http.setDataSerializer('json');
  }

  getProductionChart(req): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + `/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetMachineBasedOEE?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268`;
    return new Promise((resolve, reject) => {
      this.http.post(url, req, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }
  noPathData(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + `/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetZonewisePartDetails?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268`;
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }
  getShiftList(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + `/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetShiftList?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268`;
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }

  getSummaryDetail(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + `/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetShiftwiseSummary?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268`;
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }

  getAvailabilityDetails(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + `/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetShiftbasedAvailabilityStatus?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268`;
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }

  getPullCardDetails(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + '/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetPullCardDetails?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268';
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }

  getnoPathCurrentCount(): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + '/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetCurrentShiftTargetandPlannedCount?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268';
    return new Promise((resolve, reject) => {
      this.http.post(url, {}, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }

  getTopDowntimeReasons(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + '/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetTop5DownTimeReasons?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268';
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }

  //  TODO :  PRODUCTION PERFORMANCE SERVICE :


  productionPerformanceStatus(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + '/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetShiftbasedPerformanceStatus?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268';
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }

  getHourlyProtection(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + '/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetHourlyProductionData?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268';
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }


  //  TODO :  PRODUCTION QUALITY SERVICE :


  productionQualityStatus(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + '/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetShiftbasedQualityStatus?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268';
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }

  getProductionQualityRejection(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + '/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetShiftwiseRejectedPartDetails?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268';
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }

  getOLEAndAPQTrend(data): Promise<HTTPResponse> {
    this.setHeaders();
    let url = environment.apiUrl + '/Things/NXLine_Hybrid_Mobile_Application_TH/Services/GetOLEandAPQTrend?appKey=22b4e041-2a59-41e4-a954-7d2dce3a1268';
    return new Promise((resolve, reject) => {
      this.http.post(url, data, {}).then(res => {
        resolve(JSON.parse(res.data));
      })
        .catch(err => {
          reject(err);
        });
    });
  }
}

