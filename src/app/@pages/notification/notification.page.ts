import { Component, OnInit, OnDestroy } from '@angular/core';
import { NotificationService } from 'src/app/@service/notification.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { CompileShallowModuleMetadata } from '@angular/compiler';
import * as moment from 'moment';
import { LoadingProvider } from 'src/app/@core/mock/loading';

@Component({
  selector: 'notification',
  templateUrl: './notification.page.html',
  styleUrls: ['./notification.page.scss'],
})
export class NotificationPage implements OnInit, OnDestroy {
  getNotificationListData: any[] = [];
  username: any = '';
  NotificationListHeader: any;
  activeInterval: any;
  constructor(
    public notificationService: NotificationService,
    public loadingProvider: LoadingProvider
  ) { }

  async  ngOnInit() {
    await this.loadingProvider.showLoaderWithBackDropWithoutTimer();
    await new Promise(async (resolve) => {
      let data = await JSON.parse(localStorage.getItem('userData'));
      if (data.Username) {
        this.username = data.UserName || ''
      }
      resolve(data);
    }).then(async () => {

      let data = await JSON.parse(localStorage.getItem('userData'));
      await this.getNotificationList({ UserName: data.UserName || '' });
      this.activeInterval = await setInterval(() => {
        console.log('setInterval called');
        this.getNotificationList({ UserName: data.UserName || '' });
      }, 15000)
    })
    await this.loadingProvider.hideLoader();
  }

  ngOnDestroy() {
    console.log('clear interval called in notifcation ng on destory')
    clearInterval(this.activeInterval);
  }

  async doRefresh(event) {
    await clearInterval(this.activeInterval);
    console.log('Begin async operation');
    await this.ngOnInit();
    await event.target.complete();
  }


  getNotificationList(username) {
    // this.loadingProvider.showLoaderWithBackDropWithoutTimer('Auto triggered Page Refreshing');
    this.getNotificationListData = [];
    this.notificationService.getNotificationlist(username).then((getNotificationListResponse: any) => {
      console.log({ getNotificationListResponse });
      // this.loadingProvider.hideLoader();
      this.getNotificationListData = getNotificationListResponse!.rows || [];
      this.NotificationListHeader = getNotificationListResponse!.rows[0] || {};
      if (this.NotificationListHeader && this.NotificationListHeader.ShiftDate) {
        this.NotificationListHeader.ShiftDate = moment(this.NotificationListHeader.ShiftDate).format("DD-MM-YYYY")
      }
      console.log(this.NotificationListHeader);
    }).catch((getNotificationListError) => {
      // this.loadingProvider.hideLoader();
      console.log({ getNotificationListError });
      this.getNotificationListData = [];
      this.NotificationListHeader = {};
    }
    )
  }

  clearNotification(item) {
    this.loadingProvider.showLoaderWithBackDropWithoutTimer();
    let data = item.id;
    console.log(data);
    this.notificationService.getAcknowledgementNotification({ id: item.id || '' }).then(
      (clearNotificationResponse: any) => {
        this.loadingProvider.hideLoader();
        console.log({ clearNotificationResponse });
        this.getNotificationListData = this.getNotificationListData.filter(res => res.id != item.id)
        // let data = JSON.parse(localStorage.getItem('userData'));
        // this.getNotificationList({ UserName: data.UserName || '' });
      }).catch((clearNotificationError) => {
        console.log({ clearNotificationError });
        this.loadingProvider.hideLoader();
      })
  }

  clearAll() {
    this.loadingProvider.showLoaderWithBackDropWithoutTimer();
    let data = JSON.parse(localStorage.getItem('userData'));
    this.notificationService.AcknowledgeAllNotifications({ "UserName": data.UserName }).then(
      (acknowledgeAllDataResponse: any) => {
        this.loadingProvider.hideLoader();
        console.log({ acknowledgeAllDataResponse });
        let data = JSON.parse(localStorage.getItem('userData'));
        this.getNotificationList({ UserName: data.UserName || '' });
      })
      .catch((acknowledgeAllDataError) => {
        console.log({ acknowledgeAllDataError }),
          this.loadingProvider.hideLoader();
      }
      )
  }
}
