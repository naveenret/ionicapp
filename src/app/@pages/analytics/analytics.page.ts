import { Component, OnInit } from '@angular/core';
import { Color, Label, Colors } from 'ng2-charts';
import { ChartOptions, ChartDataSets, TimeScale } from 'chart.js';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ProductionService } from 'src/app/@service/production.service';
import { AnalyticsService } from 'src/app/@service/analytics.service';
import { HomeService } from 'src/app/@service/home.service';
import * as moment from 'moment-timezone';
import { LoadingProvider } from 'src/app/@core/mock/loading';


@Component({
  selector: 'analytics',
  templateUrl: './analytics.page.html',
  styleUrls: ['./analytics.page.scss'],
})
export class AnalyticsPage implements OnInit {


  customAlertOptions: any = {
    header: 'Select One',
    translucent: true,
  };

  currentActiveMenu: any = "Hourly";
  currentSegment: any = "Production";
  menuActive: boolean = false;
  getShiftListsData: any;
  pieChartHeadLabel: any = '';
  getAnalyticsHourlyData: any;
  getTopDowntimeReasonsData: any = [];
  getTopMechineDowntimeReasonsData: any = [];
  getMachineShiftListsData: any = [];
  analyticsDDList: any;
  analyticsProductionCountData: any;
  lineChartColorsMachine: Color[];
  hourlyDataForm: FormGroup;
  analyticsForm: FormGroup;
  analyticsDDForm: FormGroup;
  analyticsMachineForm: FormGroup;
  hourlyChartData: ChartDataSets[];
  hourlyProductionChartData: ChartDataSets[];
  mechineRunTimeDownTimeChartData: ChartDataSets[];
  hourlyChartLabels: Label[];
  mechineProductionCountChartLabels: Label[];
  mechineRunTimeDownTimeChartLabels: Label[];
  OLEandAPQChartData: any[] = [];

  constructor(
    public datePicker: DatePicker,
    public fb: FormBuilder,
    public productionService: ProductionService,
    public analyticsService: AnalyticsService,
    public homeService: HomeService,
    public loadingProvider: LoadingProvider
  ) {
    this.analyticsForm = this.fb.group({
      MachineThingName: 'BConfig_Machine_AssemblyLine',
      SelectedDate: moment(new Date()).format('x'),
      ShiftName: ''
    });

    this.analyticsDDForm = this.fb.group({
      MachineThingName: 'BConfig_Machine_AssemblyLine',
      StartdDate: moment(new Date()).format('x'),
      EndDate: moment(new Date()).format('x'),
      ShiftName: ''
    });

    this.analyticsMachineForm = this.fb.group({
      MachineThingName: 'BConfig_Machine_AssemblyLine',
      MahineDisplayName: '',
      StartDate: this.getPreviousWeekDate(),
      EndDate: moment(new Date).format('x'),
      ShiftName: ''
    })
  }


  getPreviousWeekDate() {
    let date = new Date();
    let pastDate = date.getDate() - 7;
    return moment(date.setDate(pastDate)).format('x');
  }

  ngOnInit() {
    if (this.currentActiveMenu == 'Hourly') {
      this.getShiftLists(this.analyticsForm.value);
    } else {
      new Promise((resolve) => {
        this.getAnalyticsMachineDropDown();
        resolve();
      }).then(() => {
        // this.getMachineShiftLists(this.analyticsDDForm.value);
        // this.getMechineActiveData();
      })
    }
  }

  getAnalyticsDate() {
    if (new Date(moment.unix(this.analyticsForm.controls.SelectedDate.value / 1000)).toString() == 'Invalid Date') {
      return new Date();
    } else {
      return new Date(moment.unix(this.analyticsForm.controls.SelectedDate.value / 1000));
    }
  }

  showDate() {
    this.datePicker.show({
      date: this.getAnalyticsDate(),
      mode: 'date',
      maxDate: new Date().valueOf(),
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
    }).then(
      date => {
        new Promise((resolve) => {
          this.analyticsForm.patchValue({
            SelectedDate: moment(date).format('x')
          });
          resolve();
        }).then(async () => {
          console.log('Got date: ', date);
          // this.getHourlyAllData();          
          await this.loadingProvider.showLoaderWithBackDropWithoutTimer();
          // await this.analyticsForm.controls.ShiftName.reset();
          await this.getShiftLists(this.analyticsForm.value);
          await this.loadingProvider.hideLoader();
        })
      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }


  getMechineStartDate() {
    if (new Date(moment.unix(this.analyticsMachineForm.controls.StartDate.value / 1000)).toString() == 'Invalid Date') {
      return new Date();
    } else {
      return new Date(moment.unix(this.analyticsMachineForm.controls.StartDate.value / 1000));
    }
  }


  showMachineStartDate() {
    this.datePicker.show({
      date: this.getMechineStartDate(),
      mode: 'date',
      maxDate: this.getMechineEndDate().valueOf(),
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
    }).then(
      date => {
        new Promise((resolve) => {
          this.analyticsMachineForm.patchValue({
            StartDate: moment(date).format('x')
          });
          resolve();
        }).then(() => {
          console.log('Got End date: ', date);
          // this.getAnalyticsMachineProdcutionCount(this.analyticsMachineForm.value);
          this.getAnalyticsMachineDropDown();
          // this.getMechineActiveData();
        })
      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }

  getMechineEndDate() {
    if (new Date(moment.unix(this.analyticsMachineForm.controls.EndDate.value / 1000)).toString() == 'Invalid Date') {
      return new Date();
    } else {
      return new Date(moment.unix(this.analyticsMachineForm.controls.EndDate.value / 1000));
    }
  }


  showMachineEndDate() {
    this.datePicker.show({
      date: this.getMechineEndDate(),
      mode: 'date',
      minDate: this.getMechineStartDate().valueOf(),
      maxDate: new Date().valueOf(),
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
    }).then(
      date => {
        console.log('Got End date: ', date);
        new Promise((resolve) => {
          this.analyticsMachineForm.patchValue({
            EndDate: moment(date).format('x')
          });
          console.log('this.analyticsMachineForm', this.analyticsMachineForm.value);
          console.log('datemoment(date).format()', moment(date).format('x'));
          console.log('date vs enddate', date, this.analyticsMachineForm.controls.EndDate.value);
          resolve(this.analyticsMachineForm.controls.EndDate.value);
        }).then((date) => {
          console.log('Got End date: changed ', date);
          // this.getAnalyticsMachineProdcutionCount(this.analyticsMachineForm.value);
          // this.getMechineActiveData();
          this.getAnalyticsMachineDropDown();
        })
      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }

  getShiftLists(data) {
    this.getShiftListsData = [];
    this.productionService.getShiftList(data).then(
      (getShiftListsResponse: any) => {
        console.log({ getShiftListsResponse });
        this.getShiftListsData = getShiftListsResponse!.rows || [];
        new Promise((resolve)=>{

          if (this.getShiftListsData.length > 0) {
            this.analyticsForm.patchValue({
              ShiftName: this.getShiftListsData[0].ShiftName
            })
          }
          resolve();
        }).then(()=>{
          this.getHourlyAllData();
        })
      }).catch((getShiftListsError) => {
        console.log({ getShiftListsError });
        this.getShiftListsData = [];
      }
      )
  }

  getMachineShiftLists(data) {
    this.getMachineShiftListsData = [];
    this.productionService.getShiftList(data).then((getShiftListsResponse: any) => {
      console.log({ getShiftListsResponse });
      this.getMachineShiftListsData = getShiftListsResponse!.rows || [];
      if (this.getShiftListsData.length > 0) {
        this.analyticsMachineForm.patchValue({
          ShiftName: this.getShiftListsData[0].ShiftName
        })
      }
    }).catch((getShiftListsError) => {
      console.log({ getShiftListsError });
      this.getMachineShiftListsData = [];
    });
  }



  getAnalyticsHourlyDataList(data) {
    this.getAnalyticsHourlyData = [];
    this.analyticsService.getAnalyticsHourlyData(data).then(
      (getAnalyticsHourlyDataResponse: any) => {
        console.log({ getAnalyticsHourlyDataResponse });
        let getAnalyticsHourlyData = getAnalyticsHourlyDataResponse!.rows || [];
        console.log({ getAnalyticsHourlyData })
        this.setHourlyChartData(getAnalyticsHourlyData);
        this.setProductionChartData(getAnalyticsHourlyData);
      }).
      catch((getAnalyticsHourlyDataError) => {
        console.log({ getAnalyticsHourlyDataError });
        this.getAnalyticsHourlyData = [];
        this.hourlyProductionChartData = [];
      })
  }

  getAnalyticsMachineProdcutionCount(data) {
    console.log('Analytics Machine Form', data);
    this.analyticsService.getAnalyticsProductionCount(data).then(
      (getAnalyticsProductionCountResponse: any) => {
        console.log({ getAnalyticsProductionCountResponse });
        this.setMechineProductionChartData(getAnalyticsProductionCountResponse!.rows || []);
      }).
      catch((getAnalyticsProductionCountError) => console.log({ getAnalyticsProductionCountError })
      )
  }


  setMechineProductionChartData(analyticsProductionCountData) {
    let hourlyTargetData = analyticsProductionCountData.map(e1 => e1.Planned);
    let hourlyAcceptData = analyticsProductionCountData.map(e1 => e1.Accepted);
    let hourlyRejectData = analyticsProductionCountData.map(e1 => e1.Rejected);
    let hourlyChartLabel = analyticsProductionCountData.map(e1 => moment(e1.DateTime).format('DD/MM'));
    this.analyticsProductionCountData = [
      {
        data: hourlyTargetData || [],
        label: 'Planned',
        lineTension: 0,
        type: 'line',
        pointRadius: 4,
        pointBorderColor: analyticsProductionCountData[0].PlannedColourCode || '#7B9AFE',
        pointBackgroundColor: analyticsProductionCountData[0].PlannedColourCode || '#7B9AFE',
        borderColor: analyticsProductionCountData[0].PlannedColourCode || '#7B9AFE',
        backgroundColor: 'transparent'
      },
      {
        data: hourlyAcceptData || [],
        borderJoinStyle: "round",
        barThickness: 10,
        label: 'Accept',
        backgroundColor: analyticsProductionCountData[0].AcceptedColourCode || '#93EE94',
        borderColor: analyticsProductionCountData[0].AcceptedColourCode || '#93EE94',
        stack: 'a',
        type: 'bar',
      },
      {
        data: hourlyRejectData || [],
        barThickness: 10,
        label: 'Reject',
        
        backgroundColor: analyticsProductionCountData[0].RejectedColourCode || '#E2717C',
        borderColor: analyticsProductionCountData[0].RejectedColourCode || '#E2717C',
        stack: 'b',
        type: 'bar',
      }
    ];
    this.mechineProductionCountChartLabels = hourlyChartLabel || [];
  }

  setHourlyChartData(getAnalyticsHourlyData) {
    console.log('hourly Production data => getAnalyticsHourlyData ');
    console.log({ getAnalyticsHourlyData });

    let hourlyTargetData = getAnalyticsHourlyData.map(e1 => e1.TotalTarget);
    let hourlyAcceptData = getAnalyticsHourlyData.map(e1 => e1.AcceptedPart);
    let hourlyRejectData = getAnalyticsHourlyData.map(e1 => e1.RejectedPart);
    let hourlyChartLabel = getAnalyticsHourlyData.map(e1 => moment(e1.HourlyEndTime).format('hh:mm'));
    let hourlyChartTargetColor = getAnalyticsHourlyData.map(e1 => e1.RejectedPart);
    this.hourlyChartData = [
      {
        data: hourlyTargetData || [],
        label: 'Target',
        lineTension: 0,
        type: 'line',
        pointRadius: 2,
        pointBorderColor: getAnalyticsHourlyData[0]!.TargetColourCode || '#7B9AFE',
        pointBackgroundColor: getAnalyticsHourlyData[0]!.TargetColourCode || '#7B9AFE',
        borderColor: getAnalyticsHourlyData[0]!.TargetColourCode || '#7B9AFE',
        // borderWidth: 2,
        backgroundColor: "transparent",
      },
      {
        data: hourlyAcceptData || [],
        borderJoinStyle: "round",
        spanGaps: true,
        barThickness: 10,
        label: 'Accept',
        borderColor:  getAnalyticsHourlyData[0]!.AcceptColourCode || '#93EE94',
        backgroundColor: getAnalyticsHourlyData[0]!.AcceptColourCode || '#93EE94',
        stack: 'a',
        type: 'bar',

      },
      {
        data: hourlyRejectData || [],
        barThickness: 10,
        label: 'Reject',
        borderColor: getAnalyticsHourlyData[0]!.RejectColourCode || '#272D3B',
        backgroundColor: getAnalyticsHourlyData[0]!.RejectColourCode || '#272D3B',
        stack: 'b',
        type: 'bar',
      }
    ];
    this.hourlyChartLabels = hourlyChartLabel || [];
  }

  setProductionChartData(getAnalyticsHourlyData) {
    console.log('hourly downtime data => getAnalyticsHourlyData ');
    console.log({ getAnalyticsHourlyData })
    let hourlyAcceptData = getAnalyticsHourlyData.map(e1 => e1.RunTime);
    let hourlyRejectData = getAnalyticsHourlyData.map(e1 => e1.Downtime);
    let hourlyChartLabel = getAnalyticsHourlyData.map(e1 => moment(e1.HourlyEndTime).format('hh:mm'));
    let hourlyChartTargetColor = getAnalyticsHourlyData.map(e1 => e1.RejectedPart);
    this.hourlyProductionChartData = [
      {
        data: hourlyAcceptData || [],
        borderJoinStyle: "round",
        spanGaps: true,
        barThickness: 10,
        label: 'Runtime',
        borderColor: getAnalyticsHourlyData[0].RunTimeColourCode || '#93EE94',
        backgroundColor: getAnalyticsHourlyData[0].RunTimeColourCode || '#93EE94',
        stack: 'a',
        type: 'bar',
      },
      {
        data: hourlyRejectData || [],
        barThickness: 10,
        label: 'Downtime',
        borderColor: getAnalyticsHourlyData[0].DownTimeColourCode || '#93EE94',
        backgroundColor: getAnalyticsHourlyData[0].DownTimeColourCode || '#E2717C',
        stack: 'b',
        type: 'bar',
      }
    ];
    console.log(this.hourlyProductionChartData);
    this.hourlyChartLabels = hourlyChartLabel || [];
  }

  getTopDowntimeReasons(data) {
    console.log('hourly getTopDowntimeReasons req ', data);
    this.getTopDowntimeReasonsData = [];
    this.productionService.getTopDowntimeReasons(data).then((getTopDowntimeReasonsResponse: any) => {
      console.log('hourly getTopDowntimeReasons res');
      console.log({ getTopDowntimeReasonsResponse });
      this.getTopDowntimeReasonsData = getTopDowntimeReasonsResponse!.rows || [];
      console.log(this.getTopDowntimeReasonsData);
    }).catch((getTopDowntimeReasonsError) => {
      console.log('hourly getTopDowntimeReasons err');
      console.log({ getTopDowntimeReasonsError });
      this.getTopDowntimeReasonsData = [];
    });
  }

  toggleMenu() {
    this.menuActive = this.menuActive == true ? false : true;
    console.log('toggle called', this.menuActive);
  }

  closeMenu(event) {
    console.log(event.path)
    if (this.checkPathThatContainSelector(event.path, 'nk-menu-drop-down-area')) {
      return;
    }
    console.log('click outside');
    this.toggleMenu();
  }

  segmentChanged(value) {
    console.log({ value });
    this.currentSegment = value.detail.value;
  }

  setActiveMenu(value) {
    console.log(value);
    new Promise((resolve) => {
      this.currentActiveMenu = value;
      resolve();
    }).then(async () => {
      if (value == 'Machine') {
        this.currentSegment = "Production";
        await new Promise(async (resolve) => {
          await this.loadingProvider.showLoaderWithBackDropWithoutTimer();
          // await this.analyticsMachineForm.reset();
          // // await this.analyticsDDForm.reset();
          // await this.analyticsMachineForm.patchValue({
          //   MachineThingName: 'BConfig_Machine_AssemblyLine',
          //   MahineDisplayName: '',
          //   StartDate: this.getPreviousWeekDate(),
          //   EndDate: moment(new Date).format('x'),
          //   ShiftName: ''
          // })
          await this.getAnalyticsMachineDropDown();
          await this.loadingProvider.hideLoader();
          resolve();
        }).then(() => {
          // this.getMachineShiftLists(this.analyticsDDForm.value);
          // this.getMechineActiveData();
        })
      } else if (value == 'Hourly') {
        // this.getShiftLists(this.analyticsForm.value);
        this.getHourlyAllData();
      }
    })
    this.toggleMenu();
  }




  ShiftDropdownSelected(event) {
    new Promise((resolve) => {
      this.analyticsForm.patchValue({
        ShiftName: event.detail.value || ''
      });
      resolve();
    }).then(() => {
      this.getHourlyAllData();
    })
  }

  async getHourlyAllData() {
    await this.loadingProvider.showLoaderWithBackDropWithoutTimer();
    let hourlyData: any = {};
    hourlyData.SelectedDate = await this.analyticsForm.controls.SelectedDate.value;
    hourlyData.ShiftName = await this.analyticsForm.controls.ShiftName.value;
    await this.getAnalyticsHourlyDataList(hourlyData);
    await this.getTopDowntimeReasons(this.analyticsForm.value);
    await this.loadingProvider.hideLoader();
  }

  getAnalyticsMachineDropDown() {
    this.homeService.getMechineStatusList().then((response: any) => {
      let mechineStatus = response.rows || [];
      console.log(mechineStatus);
      this.analyticsDDList = mechineStatus.filter(res => res.NavigateTo == 'OEE') || [];
      console.log(this.analyticsDDList);
      new Promise((resolve) => {

        if (this.analyticsDDList.length > 0) {
          this.analyticsMachineForm.patchValue({
            MahineDisplayName: this.analyticsDDList[0].MahineDisplayName,
            MachineThingName: this.analyticsDDList[0].MachineThingName,
          });
          console.log('mechien list set neeed to call shift dd');
          resolve();
        }
      }).then(() => {
        let data = {
          detail: {
            value: this.analyticsMachineForm.controls.MahineDisplayName.value || ''
          }
        }
        this.getMachineShiftListsByMechine(data);
      })
    }).catch((err) => {
      this.analyticsDDList = []
    })

  }

  getMachineShiftListsByMechine(data) {
    console.log('shift dd called multope date')

    new Promise((resolve) => {
      this.analyticsMachineForm.patchValue({
        MechineDisplayName: data.detail.value || ''
      })
      resolve();
    }).then(() => {
      let data = {
        "MachineThingName": this.analyticsMachineForm.controls.MachineThingName.value,
        "FromDate": this.analyticsMachineForm.controls.StartDate.value,
        "ToDate": this.analyticsMachineForm.controls.EndDate.value
      };
      this.getMachineShiftListsData = [];

      this.analyticsService.getShiftListMultipleDate(data).then(
        (getShiftListsResponse: any) => {
          console.log({ getShiftListsResponse });
          this.getMachineShiftListsData = getShiftListsResponse!.rows || [];
          if (this.getShiftListsData.length > 0) {
            new Promise((resolve) => {
              this.analyticsMachineForm.patchValue({
                ShiftName: this.getShiftListsData[0].ShiftName || ''
              });
              resolve();
              console.log('shift agai set')
            }).then(() => {
              this.getMechineActiveData();
              // let data = {
              //   detail: {
              //     value : this.analyticsMachineForm.controls.ShiftName.value || ''
              //   }
              // }
              // this.MachineShiftDropdownSelected(data);
            })
          }
        }).
        catch((getShiftListsError) => {
          console.log({ getShiftListsError });
          this.getMachineShiftListsData = [];
          console.log(' shift dd err')

        })
    });
  }

  MachineShiftDropdownSelected(event) {
    console.log('mechine shift called choosed by user');
    new Promise((resolve) => {
      this.analyticsMachineForm.patchValue({
        ShiftName: event.detail.value || ''
      });
      resolve();
    }).then(() => {
      this.getMechineActiveData();
      // this.getAnalyticsMachineProdcutionCount(this.analyticsMachineForm.value);
    })
  }

  async getMechineActiveData() {
    await this.loadingProvider.showLoaderWithBackDropWithoutTimer();
    await this.getAnalyticsMachineProdcutionCount(this.analyticsMachineForm.value);
    this.OLEandAPQChartData = await [];
    await this.getOLEAndAPQTrend(this.analyticsMachineForm.value);
    await this.getDowntimePieChart(this.analyticsMachineForm.value);
    await console.log('this.analyticsMachineForm.value', this.analyticsMachineForm.value)
    await this.getTopDowntimeMultipleDate(this.analyticsMachineForm.value);
    await this.loadingProvider.hideLoader();
  }


  /*******  Chart Data  *******/



  lineChartData: ChartDataSets[] = [
    {
      data: [8, 8, 8, 8, 8],
      label: 'Target',
      lineTension: 0,
      type: 'line',
      pointRadius: 0
    },
    {
      borderJoinStyle: "round",
      spanGaps: true,
      barThickness: 5,
      data: [2, 8, 3, 6, 2],
      label: 'Accept',
      backgroundColor: '#93EE94',
      stack: 'a',
      type: 'bar',
    },
    {
      barThickness: 5,
      data: [4, 6, 9, 6, 6], label: 'Reject',
      backgroundColor: '#E2717C', stack: 'b', type: 'bar',
    }
  ];


  lineChartLabels: Label[] = ['08:00', '08:00', '08:00', '08:00', '08:00'];

  lineChartOptions: ChartOptions = {
    legend: {

      position: 'bottom',
      labels: {
        usePointStyle: true,
        fontColor: 'white',

      },
    },
    responsive: false,
    maintainAspectRatio: false,
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: false,
          fontColor: '#E2E2E2',
          fontSize: 10,
          fontFamily: 'Montserrat-Header'
        },
        gridLines: {
          display: false
        }
      }],
      xAxes: [{
        ticks: {
          beginAtZero: false,
          fontSize: 10,
          fontColor: '#A2A2A2',
          fontFamily: 'Montserrat-Header'
        },
        gridLines: {
          display: false
        }
      }]
    }
  };

  lineChartColors: Color[] = [
    {
      borderColor: '#93EE94',
      pointBorderColor: '#FFFFFF',
      borderWidth: 2,
      pointStyle: 'circle',
      backgroundColor: 'transparent'
    },
  ];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';


  //Production Count
  lineChartData1: ChartDataSets[] = [
    {
      data: [8, 7, 2, 7, 8, 1], label: 'Target',
      lineTension: 0,
      type: 'line',
    },
    {
      borderJoinStyle: "round",
      spanGaps: true,
      barThickness: 5,
      data: [2, 8, 3, 6, 2, 9], label: 'Accept',
      backgroundColor: '#93EE94', stack: 'a', type: 'bar',
    },
    {
      barThickness: 5,
      data: [4, 6, 9, 6, 2, 5], label: 'Reject',
      backgroundColor: '#E2717C', stack: 'b', type: 'bar',
    }
  ];
  lineChartLabels1: Label[] = ['24/01', '24/01', '24/01', '24/01', '24/01', '24/01'];



  //OEE Chart

  lineChartData2: ChartDataSets[] = [
    {
      data: [8, 7, 2, 7, 8, 1], label: 'Target',
      lineTension: 0,
      type: 'line',
      borderColor: '#93EE94'
    }
  ];

  lineChartLabels2: Label[] = ['24/01', '24/01', '24/01', '24/01', '24/01', '24/01'];
  lineChartColors2: Color[] = [
    {
      borderColor: '#93EE94',
      pointBorderColor: '#FFFFFF',
      borderWidth: 2,
      pointStyle: 'circle',
      backgroundColor: 'transparent'
    },
  ];

  //Availability Chart
  lineChartData3: ChartDataSets[] = [
    {
      data: [0, 7, 2, 8, 3, 1], label: 'Target',
      lineTension: 0,
      type: 'line',
      borderColor: '#93EE94'
    }
  ];
  lineChartLabels3: Label[] = ['24/01', '24/01', '24/01', '24/01', '24/01', '24/01'];
  lineChartColors3: Color[] = [
    {
      borderColor: '#E2717C',
      pointBorderColor: '#FFFFFF',
      borderWidth: 2,
      pointStyle: 'circle',
      backgroundColor: 'transparent'
    },
  ];

  //Performance
  lineChartData4: ChartDataSets[] = [
    {
      data: [0, 4, 2, 7, 8, 1], label: 'Target',
      lineTension: 0,
      type: 'line',
      borderColor: '#93EE94'
    }
  ];
  lineChartLabels4: Label[] = ['24/01', '24/01', '24/01', '24/01', '24/01', '24/01'];
  lineChartColors4: Color[] = [
    {
      borderColor: '#7B9AFE',
      pointBorderColor: '#FFFFFF',
      borderWidth: 2,
      pointStyle: 'circle',
      backgroundColor: 'transparent'
    },
  ];

  //Quality
  lineChartData5: ChartDataSets[] = [
    {
      data: [0, 4, 2, 7, 8, 1], label: 'Target',
      lineTension: 0,
      type: 'line',
      borderColor: '#93EE94'
    }
  ];
  lineChartLabels5: Label[] = ['24/01', '24/01', '24/01', '24/01', '24/01', '24/01'];
  lineChartColors5: Color[] = [
    {
      borderColor: '#93EE94',
      pointBorderColor: '#FFFFFF',
      borderWidth: 2,
      pointStyle: 'circle',
      backgroundColor: 'transparent'
    },
  ];
  // DownTime Chart 1
  lineChartDataDownTime1: ChartDataSets[] = [
    // {
    //   data: [7, 8, 4, 7, 2, 1], label: 'Target',
    //   lineTension: 0,
    //   type: 'line',
    // },
    {
      borderJoinStyle: "round",
      spanGaps: true,
      barThickness: 5,
      data: [2, 8, 3, 6, 2, 9], label: 'Accept',
      backgroundColor: '#93EE94', stack: 'a', type: 'bar',
    },
    {
      barThickness: 5,
      data: [4, 6, 9, 6, 2, 5], label: 'Reject',
      backgroundColor: '#E2717C', stack: 'a', type: 'bar',
    }
  ];
  //DownTime Pie Chart

  pieChartOptions: ChartOptions = {
    plugins: {
      labels: [
        {
          render: 'label',
          fontColor: '#000',
          position: 'outside'

        },
        {
          render: 'percentage',
          fontColor: ['green', 'white', 'red'],
          precision: 2
        }
      ]
    },
    legend: {
      position: 'bottom',


    },
    responsive: true,
    cutoutPercentage: 4,
    maintainAspectRatio: false,
    elements: {
      arc: {
        borderWidth: 0
      }
    }
  };
  pieChartData: ChartDataSets[] = [
    { data: [0, 0, 0, 0, 0, 0], label: 'Account A' },
  ];
  pieChartLabels = [];
  pieChartColors: Color[] = [
    {
      backgroundColor: ['#ABBE5B', '#077057', '#A52B2B', '#268C95', '#141414', '#E9B46F']
    },
  ];



  // Mechine Production OLE AND APQ TRENT : 


  getDowntimePieChart(data) {

    this.analyticsService.getDowntimePieChart(data).then((getDowntimePieChartResponse: any) => {
      console.log({ getDowntimePieChartResponse });
      this.pieChartData[0].data = getDowntimePieChartResponse!.rows.map(el => el.DurationinHrs);
      this.pieChartLabels = getDowntimePieChartResponse!.rows.map(el => el.Reason);
      this.pieChartHeadLabel = `TOTAL HOURS - ${getDowntimePieChartResponse!.rows[0].TotalHours || ''} ` || '';
      let backgroundColor: any[] = [];
      getDowntimePieChartResponse!.rows.forEach(() => backgroundColor.push(this.random_rgba()));
      this.pieChartColors[0].backgroundColor = backgroundColor;
      console.log('pie chart data', this.pieChartData);
      console.log('pie chart data', this.pieChartLabels);
    }).
      catch((err) => {
        console.log({ err });
      })
  }

  async getOLEAndAPQTrend(data) {

    this.OLEandAPQChartData = await [];
    await this.productionService.getOLEAndAPQTrend(data).then((OLEAndAPQTrendResponse: any) => {
      console.log({ OLEAndAPQTrendResponse });
      new Promise(async (resolve) => {
        this.OLEandAPQChartData = await [];
        await this.setOLELineChartData(OLEAndAPQTrendResponse);
        resolve();
      }).then(() => {
        this.setPerformanceLineChartData(OLEAndAPQTrendResponse);
      }).then(() => {
        this.setAvailabilityLineChartData(OLEAndAPQTrendResponse);
      }).then(() => {
        this.setQualityLineChartData(OLEAndAPQTrendResponse);
      }).then(() => {
        this.setRuntimeDowntimeChartData(OLEAndAPQTrendResponse);
      });
    }).catch((error) => {
      this.OLEandAPQChartData = [];
    })
  }


  setOLELineChartData(OLEAndAPQTrendResponse) {
    let data: any = {};
    let ChartDataSets: ChartDataSets[] = [];
    let chartDatas: any = {};
    chartDatas.lineTension = 0;
    chartDatas.data = OLEAndAPQTrendResponse!.rows.map(el => el.OEE) || [];
    chartDatas.label = 'OLE' || '';
    chartDatas.borderWidth = 2;
    chartDatas.pointBorderColor = 'red';
    ChartDataSets.push(chartDatas);
    console.log(ChartDataSets);
    data.ChartDataSets = ChartDataSets;
    // data.chartLabels  = res!.rows.map(el => el.timestamp) || [];;
    data.chartLabels = OLEAndAPQTrendResponse!.rows.map(el => moment(el.DateTime).format('DD/MM')) || [];
    data.label = 'OLE' || '';
    data.color = [
      {
        borderColor: OLEAndAPQTrendResponse!.rows[0]!.OEEColourCode || '#93EE94',
        pointBorderColor: '#FFFFFF',
        borderWidth: 4,
        pointRadius: 4,
        pointBackgroundColor: '#FFFFFF',
        pointStyle: 'circle',
        backgroundColor: 'transparent'
      },
    ];
    if (this.OLEandAPQChartData.length >= 4) {
      this.OLEandAPQChartData = [];
      this.OLEandAPQChartData.push(data);
    } else {
      this.OLEandAPQChartData.push(data);
    }
  }

  setPerformanceLineChartData(OLEAndAPQTrendResponse) {
    let data: any = {};
    let ChartDataSets: ChartDataSets[] = [];
    let chartDatas: any = {};
    chartDatas.lineTension = 0;
    chartDatas.data = OLEAndAPQTrendResponse!.rows.map(el => el.Performance) || [];
    chartDatas.label = 'Performance' || '';
    chartDatas.borderWidth = 2;
    chartDatas.pointBorderColor = 'red';
    ChartDataSets.push(chartDatas);
    console.log(ChartDataSets);
    data.ChartDataSets = ChartDataSets;
    // data.chartLabels  = res!.rows.map(el => el.timestamp) || [];;
    data.chartLabels = OLEAndAPQTrendResponse!.rows.map(el => moment(el.DateTime).format('DD/MM')) || [];
    data.label = 'Performance' || '';
    data.color = [
      {
        borderColor: OLEAndAPQTrendResponse!.rows[0]!.PerformanceColourCode || '#7B9AFE',
        pointBorderColor: '#FFFFFF',
        borderWidth: 4,
        pointRadius: 4,
        pointBackgroundColor: '#FFFFFF',
        pointStyle: 'circle',
        backgroundColor: 'transparent'
      },
    ];
    if (this.OLEandAPQChartData.length >= 4) {
      this.OLEandAPQChartData = [];
      this.OLEandAPQChartData.push(data);
    } else {
      this.OLEandAPQChartData.push(data);
    }
  }

  setAvailabilityLineChartData(OLEAndAPQTrendResponse) {
    let data: any = {};
    let ChartDataSets: ChartDataSets[] = [];
    let chartDatas: any = {};
    chartDatas.lineTension = 0;
    chartDatas.data = OLEAndAPQTrendResponse!.rows.map(el => el.Availability) || [];
    chartDatas.label = 'Availability' || '';
    chartDatas.borderWidth = 2;
    chartDatas.pointBorderColor = 'red';
    ChartDataSets.push(chartDatas);
    console.log(ChartDataSets);
    data.ChartDataSets = ChartDataSets;
    // data.chartLabels  = res!.rows.map(el => el.timestamp) || [];;
    data.chartLabels = OLEAndAPQTrendResponse!.rows.map(el => moment(el.DateTime).format('DD/MM')) || [];
    data.label = 'Availability' || '';
    data.color = [
      {
        borderColor: OLEAndAPQTrendResponse!.rows[0]!.AvailabilityColourCode || '#E2717C',
        pointBorderColor: '#FFFFFF',
        borderWidth: 4,
        pointRadius: 4,
        pointBackgroundColor: '#FFFFFF',
        pointStyle: 'circle',
        backgroundColor: 'transparent'
      },
    ];
    if (this.OLEandAPQChartData.length >= 4) {
      this.OLEandAPQChartData = [];
      this.OLEandAPQChartData.push(data);
    } else {
      this.OLEandAPQChartData.push(data);
    }
  }

  setQualityLineChartData(OLEAndAPQTrendResponse) {
    let data: any = {};
    let ChartDataSets: ChartDataSets[] = [];
    let chartDatas: any = {};
    chartDatas.lineTension = 0;
    chartDatas.data = OLEAndAPQTrendResponse!.rows.map(el => el.Quality) || [];
    chartDatas.label = 'Quality' || '';
    chartDatas.borderWidth = 2;
    chartDatas.pointBorderColor = 'red';
    ChartDataSets.push(chartDatas);
    console.log(ChartDataSets);
    data.ChartDataSets = ChartDataSets;
    // data.chartLabels  = res!.rows.map(el => el.timestamp) || [];;
    data.chartLabels = OLEAndAPQTrendResponse!.rows.map(el => moment(el.DateTime).format('DD/MM')) || [];
    data.label = 'Quality' || '';
    data.color = [
      {
        borderColor: OLEAndAPQTrendResponse!.rows[0]!.QualityColourCode || '#93EE94',
        pointBorderColor: '#FFFFFF',
        borderWidth: 4,
        pointRadius: 4,
        pointBackgroundColor: '#FFFFFF',
        pointStyle: 'circle',
        backgroundColor: 'transparent'
      },
    ];
    if (this.OLEandAPQChartData.length >= 4) {
      this.OLEandAPQChartData = [];
      this.OLEandAPQChartData.push(data);
    } else {
      this.OLEandAPQChartData.push(data);
    }
  }

  setRuntimeDowntimeChartData(OLEAndAPQTrendResponse) {
    console.log('set downtime chart in mechine')
    console.log(OLEAndAPQTrendResponse!.rows)
    let DowntimeData = OLEAndAPQTrendResponse!.rows.map(e1 => e1.Downtime);
    let MachineRunTimeData = OLEAndAPQTrendResponse!.rows.map(e1 => e1.MachineRunTime);
    let rundownChartLabel = OLEAndAPQTrendResponse!.rows.map(e1 => moment(e1.DateTime).format('DD/MM'));
    this.mechineRunTimeDownTimeChartData = [
      {
        data: MachineRunTimeData || [],
        label: 'Runtime',
        spanGaps: true,
        barThickness: 10,
        type: 'bar',
        showLine: true,
        backgroundColor: OLEAndAPQTrendResponse!.rows[0]!.RunTimeColourCode || '#93EE94',
        borderColor: OLEAndAPQTrendResponse!.rows[0]!.RunTimeColourCode || '#93EE94',
        stack: 'a'
      },
      {
        data: DowntimeData || [],
        showLine: true,
        spanGaps: true,
        barThickness: 10,
        label: 'Downtime',
        backgroundColor: OLEAndAPQTrendResponse!.rows[0]!.DownTimeColourCode || '#E2717C',
        borderColor: OLEAndAPQTrendResponse!.rows[0]!.DownTimeColourCode || '#E2717C',
        stack: 'b',
        type: 'bar',
      }
    ];
    this.mechineRunTimeDownTimeChartLabels = rundownChartLabel || [];

    console.log(this.mechineRunTimeDownTimeChartData);
  }


  getTopDowntimeMultipleDate(data) {
    this.getTopMechineDowntimeReasonsData = [];
    console.log('downtime data list with multiople date data', data);
    this.analyticsService.getTopDowntimeMultipleDate(data).then(
      (getTopDowntimeReasonsResponse: any) => {
        console.log({ getTopDowntimeReasonsResponse });
        this.getTopMechineDowntimeReasonsData = getTopDowntimeReasonsResponse!.rows || [];
        console.log(this.getTopMechineDowntimeReasonsData);
      }).
      catch((getTopDowntimeReasonsError) => {
        console.log({ getTopDowntimeReasonsError });
        this.getTopMechineDowntimeReasonsData = [];
      });
  }

  // UTIL AREA

  random_rgba() {
    var o = Math.round, r = Math.random, s = 255;
    return 'rgba(' + o(r() * s) + ',' + o(r() * s) + ',' + o(r() * s) + ',' + r().toFixed(1) + ')';
  }


  checkPathThatContainSelector(path, selector) {
    console.log({ path }, { selector });
    for (let i = 0; i < path.length; i++) {
      if (path[i].classList && path[i].classList.contains(selector)) {
        return true;
      }
    }
    return false;
  }

}


