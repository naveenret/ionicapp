import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RejectionReasonPage } from './rejection-reason.page';

describe('RejectionReasonPage', () => {
  let component: RejectionReasonPage;
  let fixture: ComponentFixture<RejectionReasonPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RejectionReasonPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RejectionReasonPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
