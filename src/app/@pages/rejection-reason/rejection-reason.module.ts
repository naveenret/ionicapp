import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { RejectionReasonPageRoutingModule } from './rejection-reason-routing.module';

import { RejectionReasonPage } from './rejection-reason.page';
import { SharedModule } from 'src/app/@shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    RejectionReasonPageRoutingModule
  ],
  declarations: [RejectionReasonPage]
})
export class RejectionReasonPageModule {}
