import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { RejectionReasonPage } from './rejection-reason.page';

const routes: Routes = [
  {
    path: '',
    component: RejectionReasonPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class RejectionReasonPageRoutingModule {}
