import { Component, OnInit } from '@angular/core';
import { HomeService } from 'src/app/@service/home.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'rejection-reason',
  templateUrl: './rejection-reason.page.html',
  styleUrls: ['./rejection-reason.page.scss'],
})
export class RejectionReasonPage implements OnInit {

  getRejectionDetailDataList: any[] = [];
  currentActiveItem: any;
  constructor(
    public homeService: HomeService,
    public activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.getQueryParams();
  }


  getQueryParams() {
    this.activatedRoute.queryParams.subscribe(res => {
      console.log(res);
      // console.log(JSON.parse(res.item));
      new Promise((resolve) => {
        this.currentActiveItem = res.item || {};
        resolve();
      }).then(() => {
        this.getRejectionDetailData();
      })
    })
  }


  getRejectionDetailData() {
    this.homeService.getRejectionDetailData().then((getRejectionDetailDataResponse: any) => {
      this.getRejectionDetailDataList = getRejectionDetailDataResponse!.rows || [];
      console.log({ getRejectionDetailDataResponse });
    }).catch((getRejectionDetailError) => {
      console.log({ getRejectionDetailError });
    })
  }

}
