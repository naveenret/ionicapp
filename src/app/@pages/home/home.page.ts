import { Component, OnInit, AfterViewInit, OnDestroy } from '@angular/core';
import { HomeService } from 'src/app/@service/home.service';
import { NavController } from '@ionic/angular';
import * as moment from 'moment';
import { ActivatedRoute } from '@angular/router';
import { NotificationService } from 'src/app/@service/notification.service';
import { LoadingProvider } from 'src/app/@core/mock/loading';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit, AfterViewInit, OnDestroy {

  getMechineListData: any[] = [];
  getMechineDataList: any = {};
  getPlantPerformanceData: any = [];
  getNotificationListCount: any;
  activeInterval: any;

  constructor(
    public homeService: HomeService,
    public navCtrl: NavController,
    public activatedRoute: ActivatedRoute,
    public notificationService: NotificationService,
    public loadingProvider: LoadingProvider
  ) { }

  async ngOnInit() {
    await this.loadingProvider.showLoaderWithBackDropWithoutTimer();
    console.log(this.activatedRoute.snapshot.data);
    await this.getMechineList();
    await this.getMechineData();
    await this.getPlantPerformance();
    let data = await JSON.parse(localStorage.getItem('userData'));
    this.activeInterval = await setInterval(() => {
      console.log('intervalcalled');
      this.getNotificationList({ UserName: data.UserName || '' });
    }, 1000)
    await this.loadingProvider.hideLoader();
  }

  ngOnDestroy() {
    clearInterval(this.activeInterval);
    console.log('notifcation desstory called')
  }



  ngAfterViewInit() {
    console.log('afterw view inti');

  }


  navigateToNotification() {
    this.navCtrl.navigateForward('/notification', { animated: true, animationDirection: 'forward' }).then((value) => {
      console.log('navigated');
    },
      error => {
        console.log('navigation rejected', error);
      }).catch(err => {
        console.log('navigation catch', err);
      })
  }

  ionViewWillEnter() {
    console.log('viw will enter');
    let data = JSON.parse(localStorage.getItem('userData'));
    this.getNotificationList({ UserName: data.UserName || '' });
  }

  ionViewDidEnter() {
    console.log('viw did enter');
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad');
  }

  async doRefresh(event) {
    console.log('Begin async operation');
    await this.ngOnInit();
    await event.target.complete();
  }

  getMechineList() {
    this.homeService.getMechineStatusList().then((getMechineListResponse: any) => {
      console.log({ getMechineListResponse });
      this.getMechineListData = getMechineListResponse!.rows || [];
    }).catch((getMechineListError) => {
      this.getMechineListData = []
      console.log({ getMechineListError });
    });
  }

  getMechineData() {
    this.homeService.getMechineData().then((getMechineDataResponse: any) => {
      console.log({ getMechineDataResponse });
      this.getMechineDataList = getMechineDataResponse!.rows[0] || {};
    }).catch((getMechineDataError) => {
      this.getMechineDataList = {}
      console.log({ getMechineDataError });
    });
  }
  getPlantPerformance() {
    this.homeService.getPlantPerformance().then((getPlantPerformanceResponse: any) => {
      console.log({ getPlantPerformanceResponse });
      this.getPlantPerformanceData = getPlantPerformanceResponse!.rows[0] || {};
    }).catch((getPlantPerformanceError: any) => {
      this.getMechineDataList = {}
      console.log({ getPlantPerformanceError });
    });
  }

  navigateRejection(item) {
    this.navCtrl.navigateForward('/rejection-reason', { animationDirection: 'forward', queryParams: { item } });
  }


  navigateDowntime(item) {
    this.navCtrl.navigateForward('/downtime-reason', { animationDirection: 'forward', queryParams: { item } });
  }

  navigateMechineList(mechineList) {
    console.log({ mechineList });
    clearInterval(this.activeInterval);
    if (mechineList.IsClickable) {
      switch (mechineList.NavigateTo) {
        case 'OEE':
          this.navCtrl.navigateForward('/layouts/production', { animationDirection: 'forward', queryParams: { mechineList } });
          break;
        case 'Condition Monitoring':
          this.navCtrl.navigateForward('/layouts/condition-monitoring', { animationDirection: 'forward', queryParams: { mechineList } });
          break;
      }
    }
  }

  getNotificationList(username) {
    this.notificationService.getNotificationlist(username).then(
      (getNotificationListResponse: any) => {
        console.log({ getNotificationListResponse });
        this.getNotificationListCount = getNotificationListResponse!.rows.length || [];
        console.log('Notification count', this.getNotificationListCount);
      }).catch((getNotificationListError) => {
        console.log({ getNotificationListError });
        this.getNotificationListCount = '';
      }
      )
  }
}
