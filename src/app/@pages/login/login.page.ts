import { Component, OnInit } from '@angular/core';
import { NavController, AlertController } from '@ionic/angular';
import { LoginService } from 'src/app/@service/login.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { environment } from 'src/environments/environment';
import { LoadingProvider } from 'src/app/@core/mock/loading';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  hidden: boolean = true;
  loginForm: FormGroup;
  loginResponseData: any;

  constructor(
    public navCtrl: NavController,
    public loginService: LoginService,
    public fb: FormBuilder,
    public toastController: ToastController,
    public alertController: AlertController,
    public loadingProvider : LoadingProvider
  ) {
    this.loginForm = this.fb.group({
      Username: '',
      Password: ''
    })
  }


  ngOnInit() {
  }

  async login() {
    let loginData =await this.loginForm.value;
    console.log({ loginData });
    // this.presentToast('Please wait');
    await this.loadingProvider.showLoaderWithBackDropWithoutTimer();
    await this.loginService.ValidateUser(loginData).then(
      (loginResponse: any) => {
        this.loadingProvider.hideLoader();
        console.log({ loginResponse });
        this.loginResponseData = loginResponse!.rows[0] || [];
        console.log('logged',this.loginResponseData.Response)
        console.log('logged',this.loginResponseData.Response == 'true' || this.loginResponseData.Response == true)
        if (this.loginResponseData.Response == 'true' || this.loginResponseData.Response == true) {
          // this.presentToast('Logged in Successfully');
          console.log('logged')
          localStorage.setItem('userData', JSON.stringify(this.loginResponseData));
          localStorage.setItem('isUserLoggedIn', JSON.stringify(true));
          this.navCtrl.navigateRoot('/layouts');
        } else {
          let error = this.loginResponseData.Response
          this.presentToast(error);
        }
      }).
      catch((loginError) => {
        console.log({ loginError })
        this.loadingProvider.hideLoader();
      })
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 1000,
      color: 'secondary'
    });
    toast.present();
  }

  async presentAlertConfirm(msg) {
    const alert = await this.alertController.create({
      header: 'ERROR!',
      message: msg,
      buttons: [
        {
          text: 'ok',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }
      ]
    });

    await alert.present();
  }


  async presentToastWithOptions(msg) {
    const toast = await this.toastController.create({
      header: 'error',
      message: msg,
      position: 'top',
      buttons: [
        {
          text: 'Done',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    toast.present();
  }

}
