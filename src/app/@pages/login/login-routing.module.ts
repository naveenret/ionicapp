import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginPage } from './login.page';
import { SessionvalidGuard } from 'src/app/@core/guard/sessionvalid.guard';

const routes: Routes = [
  {
    path: '',
    component: LoginPage,
    canActivate : [SessionvalidGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginPageRoutingModule {}
