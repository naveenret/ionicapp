import { Component, OnInit, OnDestroy } from '@angular/core';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { ProductionService } from 'src/app/@service/production.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as moment from 'moment-timezone';
import { LoadingProvider } from 'src/app/@core/mock/loading';

@Component({
  selector: 'summary',
  templateUrl: './summary.page.html',
  styleUrls: ['./summary.page.scss'],
})
export class SummaryPage implements OnInit, OnDestroy {


  currentInterval: any;
  customAlertOptions: any = {
    header: 'Select One',
    translucent: true,
  };
  summaryForm: FormGroup;
  getShiftListsData: any[] = [];
  getSummaryDetailData: any;
  constructor(
    private datePicker: DatePicker,
    public productionService: ProductionService,
    public fb: FormBuilder,
    public loadingProvider: LoadingProvider

  ) {
    this.summaryForm = this.fb.group({
      MachineThingName: 'BConfig_Machine_AssemblyLine',
      SelectedDate: moment(new Date()).format('x'),
      ShiftName: ''
    })
  }

  async ngOnInit() {
    await this.loadingProvider.showLoaderWithBackDropWithoutTimer();
    await this.getShiftLists(this.summaryForm.value);
    await this.loadingProvider.hideLoader();

  }

  async doRefresh(event) {
    console.log('Begin async operation');
    await this.ngOnInit();
    await event.target.complete();
  }

  ngOnDestroy() {
    console.log('ng destoru summary page')
    // clearInterval(this.currentInterval);
  }


  ShiftDropdownSelected(event) {
    console.log(event);
    new Promise((resolve) => {
      this.summaryForm.patchValue({
        ShiftName: event.detail.value || ''
      })
      resolve();
    }).then(async () => {
      await this.loadingProvider.showLoaderWithBackDropWithoutTimer();
      await this.getSummaryDetail(this.summaryForm.value);
      await this.loadingProvider.hideLoader();
    })
  }


  getDate() {
    if (new Date(moment.unix(this.summaryForm.controls.SelectedDate.value / 1000)).toString() == 'Invalid Date') {
      return new Date();
    } else {
      return new Date(moment.unix(this.summaryForm.controls.SelectedDate.value / 1000));
    }
  }


  showDate() {
    this.datePicker.show({
      date: this.getDate(),
      mode: 'date',
      maxDate: new Date().valueOf(),
      // androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_DARK
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
      // androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT
      // androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => {
        console.log('Got date: ', date);
        new Promise((resolve) => {
          this.summaryForm.patchValue({
            SelectedDate: moment(date).format('x')
          })
          resolve();
        }).then(async () => {
          await this.loadingProvider.showLoaderWithBackDropWithoutTimer();
          await this.getShiftLists(this.summaryForm.value);
          await this.loadingProvider.hideLoader();
        })
      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }

  getShiftLists(data) {
    this.productionService.getShiftList(data).then((getShiftListsResponse: any) => {
      console.log({ getShiftListsResponse });
      this.getShiftListsData = getShiftListsResponse!.rows || [];
      new Promise((resolve) => {

        if (this.getShiftListsData.length > 0) {
          this.summaryForm.patchValue({
            ShiftName: this.getShiftListsData[0].ShiftName
          })
        }
        resolve();
      }).then(() => {
        this.getSummaryDetail(this.summaryForm.value);
      })
    }).catch((getShiftListsError) => {
      // this.getShiftListsData = {};
      this.getShiftListsData = [];
      console.log({ getShiftListsError });
    }
    )
  }

  getSummaryDetail(data) {
    this.loadingProvider.showLoaderWithBackDropWithoutTimer();
    console.log('get summary called based on interval');
    this.productionService.getSummaryDetail(data).then((getSummaryDetailResponse: any) => {
      console.log({ getSummaryDetailResponse });
      this.loadingProvider.hideLoader();
      this.getSummaryDetailData = getSummaryDetailResponse!.rows[0] || {};
    }).catch((getSummaryDetailError) => {
      // this.getShiftListsData = {}; 
      this.loadingProvider.hideLoader();
      this.getSummaryDetailData = {};
      console.log({ getSummaryDetailError });
    })
  }


}
