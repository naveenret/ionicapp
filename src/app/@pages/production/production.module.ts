import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductionPageRoutingModule } from './production-routing.module';

import { ProductionPage } from './production.page';
import { ComponentModule } from 'src/app/@components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductionPageRoutingModule,
    ComponentModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  declarations: [ProductionPage]
})
export class ProductionPageModule {}
