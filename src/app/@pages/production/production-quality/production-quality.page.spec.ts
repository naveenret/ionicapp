import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProductionQualityPage } from './production-quality.page';

describe('ProductionQualityPage', () => {
  let component: ProductionQualityPage;
  let fixture: ComponentFixture<ProductionQualityPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductionQualityPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProductionQualityPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
