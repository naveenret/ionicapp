import { Component, OnInit } from '@angular/core';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ProductionService } from 'src/app/@service/production.service';

import * as moment from 'moment-timezone';
import { ActivatedRoute } from '@angular/router';
import { LoadingProvider } from 'src/app/@core/mock/loading';
@Component({
  selector: 'production-quality',
  templateUrl: './production-quality.page.html',
  styleUrls: ['./production-quality.page.scss'],
})
export class ProductionQualityPage implements OnInit {

  productionQualityForm: FormGroup;
  getShiftListsData: any[] = [];
  productionQualityRejectionData: any[] = [];
  productionQualityDetailData: any;
  customAlertOptions: any = {
    header: 'Select One',
    translucent: true,
  };
  currentActiveItem: any;
  constructor(
    public datePicker: DatePicker,
    public productionService: ProductionService,
    public fb: FormBuilder,
    public activatedRoute: ActivatedRoute,
    public loadingProvider: LoadingProvider
  ) {
    this.productionQualityForm = this.fb.group({
      MachineThingName: 'BConfig_Machine_AssemblyLine',
      SelectedDate: moment(new Date()).format('x'),
      ShiftName: ''
    })
  }

  async ngOnInit() {
    await this.loadingProvider.showLoaderWithBackDropWithoutTimer();
    await this.getQueryParams();
    await this.loadingProvider.hideLoader();
  }

  async doRefresh(event) {
    console.log('Begin async operation');
    await this.ngOnInit();
    await event.target.complete();
  }

  getQueryParams() {
    this.activatedRoute.queryParams.subscribe(res => {
      console.log(res);
      console.log(JSON.parse(res.currentActiveItem));
      new Promise((resolve) => {
        this.currentActiveItem = JSON.parse(res.currentActiveItem);
        this.productionQualityForm.patchValue({
          MachineThingName: res.currentActiveItem.MachineThingName || 'BConfig_Machine_AssemblyLine'
        });
        resolve();
      }).then(() => {
        let shiftReq: any = {};
        shiftReq.MachineThingName = this.productionQualityForm.controls.MachineThingName.value,
          shiftReq.SelectedDate = this.productionQualityForm.controls.SelectedDate.value,
          this.getShiftLists(shiftReq);
        this.getProductionQualityData(this.productionQualityForm.value);
        this.getProductionQualityRejection(this.productionQualityForm.value);
      })


    })
  }

  getShiftLists(data) {
    this.productionService.getShiftList(data).then((getShiftListsResponse: any) => {
      console.log({ getShiftListsResponse });
      this.getShiftListsData = getShiftListsResponse!.rows || [];
      new Promise((resolve)=>{

        if (this.getShiftListsData.length > 0) {
          this.productionQualityForm.patchValue({
            ShiftName: this.getShiftListsData[0].ShiftName
          })
        }
        resolve();
      }).then(()=>{
        this.getProductionQualityData(this.productionQualityForm.value);
        this.getProductionQualityRejection(this.productionQualityForm.value);
     
      })
    }).catch((getShiftListsError) => {
      // this.getShiftListsData = {};
      this.getShiftListsData = [];
      console.log({ getShiftListsError });
    }
    )
  }


  ShiftDropdownSelected(event) {
    console.log(event);
    new Promise((resolve) => {
      this.productionQualityForm.patchValue({
        ShiftName: event.detail.value || ''
      })
      resolve();
    }).then(async () => {
      await this.loadingProvider.showLoaderWithBackDropWithoutTimer();
      await this.getProductionQualityData(this.productionQualityForm.value);
      await this.getProductionQualityRejection(this.productionQualityForm.value);
      await this.loadingProvider.hideLoader();
    })
  }


  getProductionQualityRejection(data) {
    this.productionService.getProductionQualityRejection(data).then((productionQualityRejectionResponse: any) => {
      console.log({ productionQualityRejectionResponse });
      this.productionQualityRejectionData = productionQualityRejectionResponse!.rows || [];
    }).catch((productionQualityRejectionError) => {
      console.log({ productionQualityRejectionError });
      this.productionQualityRejectionData = [];
    })
  }

  getProductionQualityData(data) {
    this.productionService.productionQualityStatus(data).then((productionQualityDetailResponse: any) => {
      console.log({ productionQualityDetailResponse });
      this.productionQualityDetailData = productionQualityDetailResponse!.rows[0] || {};
    }).catch((productionQualityRejectionError) => {
      console.log({ productionQualityRejectionError });
      this.productionQualityDetailData = {};
    }
    )
  }

  getDate() {
    if (new Date(moment.unix(this.productionQualityForm.controls.SelectedDate.value / 1000)).toString() == 'Invalid Date') {
      return new Date();
    } else {
      return new Date(moment.unix(this.productionQualityForm.controls.SelectedDate.value / 1000));
    }
  }

  showDate() {
    this.datePicker.show({
      date: this.getDate(),
      mode: 'date',
      maxDate: new Date().valueOf(),
      // androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_DARK
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
      // androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT
      // androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => {
        console.log('Got date: ', date);
        new Promise((resolve) => {
          this.productionQualityForm.patchValue({
            SelectedDate: moment(date).format('x')
          })
          resolve();
        }).then(async () => {
          await this.loadingProvider.showLoaderWithBackDropWithoutTimer();
          let shiftReq: any = {};
          shiftReq.MachineThingName = this.productionQualityForm.controls.MachineThingName.value,
            shiftReq.SelectedDate = this.productionQualityForm.controls.SelectedDate.value,
            await this.getShiftLists(shiftReq);
          await this.loadingProvider.hideLoader();
        })
      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }

}
