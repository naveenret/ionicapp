import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductionQualityPageRoutingModule } from './production-quality-routing.module';

import { ProductionQualityPage } from './production-quality.page';
import { ComponentModule } from 'src/app/@components/component.module';
import { SharedModule } from 'src/app/@shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    ProductionQualityPageRoutingModule,
    SharedModule
  ],  
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [ProductionQualityPage]
})
export class ProductionQualityPageModule {}
