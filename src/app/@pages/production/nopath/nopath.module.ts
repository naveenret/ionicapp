import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NopathPageRoutingModule } from './nopath-routing.module';

import { NopathPage } from './nopath.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NopathPageRoutingModule
  ],
  declarations: [NopathPage]
})
export class NopathPageModule {}
