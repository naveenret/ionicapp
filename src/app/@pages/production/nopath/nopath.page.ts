import { Component, OnInit } from '@angular/core';
import { ProductionService } from 'src/app/@service/production.service';
import { HomeService } from 'src/app/@service/home.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'nopath',
  templateUrl: './nopath.page.html',
  styleUrls: ['./nopath.page.scss'],
})
export class NopathPage implements OnInit {
  noPathDataList1: any;
  noPathDataList2: any;
  noPathDataList3: any;
  noPathDataList4: any;
  title: any = null;
  getMechineListDataDD: any[] = [];
  noPathCurrentCountData: any;
  constructor(
    public productionService: ProductionService,
    public homeService: HomeService,
    public activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.noPathData({ "ZoneNumber": 1 });
    this.noPathData({ "ZoneNumber": 2 });
    this.noPathData({ "ZoneNumber": 3 });
    this.noPathData({ "ZoneNumber": 4 });
    this.getQueryParams();
    this.getnoPathCurrentCount();
  }

  getnoPathCurrentCount() {
    this.productionService.getnoPathCurrentCount().then((noPathCurrentCountResponse:any) => {
        console.log({ noPathCurrentCountResponse });
        this.noPathCurrentCountData = noPathCurrentCountResponse!.rows[0] || {};
        // console.log(this.noPathCurrentCountData);
      }).catch((noPathCurrentCountError) => {
        console.log({ noPathCurrentCountError });
        this.noPathCurrentCountData = {};
      })
  }
  getQueryParams() {
    this.activatedRoute.queryParams.subscribe(res => {
      console.log(res);
      let a = JSON.parse(res.currentActiveItem);
      this.title = a.MahineDisplayName;
    })
  }

  // TODO: OLD /
  // rows = [
  //   {
  //     zoneName: 'zone 1',
  //     zoneList: [
  //       {
  //         HSG_NX_PartNo: "MR141497NCN060617",
  //         HSG_NX_SerialNo: "MA20C008289",
  //         Model: "NPD",
  //         PartStatusCode: 0,
  //         RejectedColourCode: "#872E2D",
  //         ReworkColourCode: "#F7D83B",
  //         StationNo: 16,
  //       },
  //       {
  //         HSG_NX_PartNo: "MR141497NCN060617",
  //         HSG_NX_SerialNo: "MA20C008289",
  //         Model: "NPD",
  //         PartStatusCode: 0,
  //         RejectedColourCode: "#872E2D",
  //         ReworkColourCode: "#F7D83B",
  //         StationNo: 16,
  //       },
  //       {
  //         HSG_NX_PartNo: "MR141497NCN060617",
  //         HSG_NX_SerialNo: "MA20C008289",
  //         Model: "NPD",
  //         PartStatusCode: 0,
  //         RejectedColourCode: "#872E2D",
  //         ReworkColourCode: "#F7D83B",
  //         StationNo: 16,
  //       },
  //       {
  //         HSG_NX_PartNo: "MR141497NCN060617",
  //         HSG_NX_SerialNo: "MA20C008289",
  //         Model: "NPD",
  //         PartStatusCode: 0,
  //         RejectedColourCode: "#872E2D",
  //         ReworkColourCode: "#F7D83B",
  //         StationNo: 16,
  //       },
  //       {
  //         HSG_NX_PartNo: "MR141497NCN060617",
  //         HSG_NX_SerialNo: "MA20C008289",
  //         Model: "NPD",
  //         PartStatusCode: 0,
  //         RejectedColourCode: "#872E2D",
  //         ReworkColourCode: "#F7D83B",
  //         StationNo: 16,
  //       },
  //       {
  //         HSG_NX_PartNo: "MR141497NCN060617",
  //         HSG_NX_SerialNo: "MA20C008289",
  //         Model: "NPD",
  //         PartStatusCode: 0,
  //         RejectedColourCode: "#872E2D",
  //         ReworkColourCode: "#F7D83B",
  //         StationNo: 16,
  //       },
  //       {
  //         HSG_NX_PartNo: "MR141497NCN060617",
  //         HSG_NX_SerialNo: "MA20C008289",
  //         Model: "NPD",
  //         PartStatusCode: 0,
  //         RejectedColourCode: "#872E2D",
  //         ReworkColourCode: "#F7D83B",
  //         StationNo: 16,
  //       },
  //       {
  //         HSG_NX_PartNo: "MR141497NCN060617",
  //         HSG_NX_SerialNo: "MA20C008289",
  //         Model: "NPD",
  //         PartStatusCode: 0,
  //         RejectedColourCode: "#872E2D",
  //         ReworkColourCode: "#F7D83B",
  //         StationNo: 16,
  //       },
  //     ]
  //   },
  //   {
  //     zoneName: 'zone 2',
  //     zoneList: [
  //       {
  //         HSG_NX_PartNo: "MR141497NCN060617",
  //         HSG_NX_SerialNo: "MA20C008289",
  //         Model: "NPD",
  //         PartStatusCode: 0,
  //         RejectedColourCode: "#872E2D",
  //         ReworkColourCode: "#F7D83B",
  //         StationNo: 16,
  //       }
  //     ]
  //   }
  // ]

  noPathData(data) {
    this.productionService.noPathData(data).then((noPathDataResponse:any) => {
      console.log({ noPathDataResponse });
      if (data.ZoneNumber == 1) {
        this.noPathDataList1 = noPathDataResponse!.rows || {};
      }
      if (data.ZoneNumber == 2) {
        this.noPathDataList2 = noPathDataResponse!.rows || {};
      }
      if (data.ZoneNumber == 3) {
        this.noPathDataList3 = noPathDataResponse!.rows || {};
      }
      if (data.ZoneNumber == 4) {
        this.noPathDataList4 = noPathDataResponse!.rows || {};
      }
    },
      noPathDataError => {
        this.noPathDataList1 = [];
        this.noPathDataList2 = [];
        this.noPathDataList3 = [];
        this.noPathDataList4 = [];
        console.log({ noPathDataError });
      });
  }



}
