import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NopathPage } from './nopath.page';

const routes: Routes = [
  {
    path: '',
    component: NopathPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NopathPageRoutingModule {}
