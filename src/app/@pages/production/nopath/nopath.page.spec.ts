import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NopathPage } from './nopath.page';

describe('NopathPage', () => {
  let component: NopathPage;
  let fixture: ComponentFixture<NopathPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NopathPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NopathPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
