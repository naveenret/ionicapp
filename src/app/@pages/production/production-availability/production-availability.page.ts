import { Component, OnInit, DoCheck } from '@angular/core';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { ProductionService } from 'src/app/@service/production.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import * as moment from 'moment-timezone';
import { ActivatedRoute } from '@angular/router';
import { LoadingProvider } from 'src/app/@core/mock/loading';


@Component({
  selector: 'production-availability',
  templateUrl: './production-availability.page.html',
  styleUrls: ['./production-availability.page.scss'],
})
export class ProductionAvailabilityPage implements OnInit {


  customAlertOptions: any = {
    header: 'Select One',
    translucent: true,
  };

  forwardIcon: any = true;
  downIcon: any = false;
  dropdownList: any = false;
  items: any = ['Shift 1', 'Shift2']
  dropdownTitle: any = this.items[0];
  getShiftListsData: any;
  getAvailabilityDetailsData: any;
  getPullCardDetailsData: any;
  productionAvailabilityForm: FormGroup;
  currentActiveItem: any;
  getTopDowntimeReasonsData: any[] = [];
  constructor(
    public datePicker: DatePicker,
    public productionService: ProductionService,
    public fb: FormBuilder,
    public activatedRoute: ActivatedRoute,
    public loadingProvider: LoadingProvider
  ) {
    this.productionAvailabilityForm = this.fb.group({
      MachineThingName: 'BConfig_Machine_AssemblyLine',
      SelectedDate: moment(new Date()).format('x'),
      ShiftName: ''
    })
  }

  async ngOnInit() {
    //  TODO : TIME STAMP TO DATE OR DATE TO TIME STAMP CHECK 
    await this.loadingProvider.showLoaderWithBackDropWithoutTimer();
    console.log(moment("10-02-2020").format('x'));
    console.log(moment.unix("1601577000000/1000").format('DD-MM-YYYY'));

    await this.getQueryParams();
    await this.loadingProvider.hideLoader();
  }

  async doRefresh(event) {
    console.log('Begin async operation');
    await this.ngOnInit();
    await event.target.complete();
  }
  getQueryParams() {
    this.activatedRoute.queryParams.subscribe(res => {
      console.log(res);
      console.log(JSON.parse(res.currentActiveItem));
      new Promise((resolve) => {
        this.currentActiveItem = JSON.parse(res.currentActiveItem);
        this.productionAvailabilityForm.patchValue({
          MachineThingName: res.currentActiveItem.MachineThingName || 'BConfig_Machine_AssemblyLine'
        });
        resolve();
      }).then(() => {
        let shiftReq: any = {};
        shiftReq.MachineThingName = this.productionAvailabilityForm.controls.MachineThingName.value,
          shiftReq.SelectedDate = this.productionAvailabilityForm.controls.SelectedDate.value,
          this.getShiftLists(shiftReq);
        this.getAvailabilityDetails(this.productionAvailabilityForm.value);
        this.getPullCardDetails(this.productionAvailabilityForm.value);
        this.getTopDowntimeReasons(this.productionAvailabilityForm.value);
      })


    })
  }

  getDate() {
    if (new Date(moment.unix(this.productionAvailabilityForm.controls.SelectedDate.value / 1000)).toString() == 'Invalid Date') {
      return new Date();
    } else {
      return new Date(moment.unix(this.productionAvailabilityForm.controls.SelectedDate.value / 1000));
    }
  }

  showDate() {
    this.datePicker.show({
      date: this.getDate(),
      mode: 'date',
      maxDate: new Date().valueOf(),
      // androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_DARK
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
      // androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT
      // androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => {
        console.log('Got date: ', date)
        new Promise((resolve) => {
          this.productionAvailabilityForm.patchValue({
            SelectedDate: moment(date).format('x'),
          })
          resolve();
        }).then(async () => {
          await this.loadingProvider.showLoaderWithBackDropWithoutTimer();
          let shiftReq: any = {};
          shiftReq.MachineThingName = this.productionAvailabilityForm.controls.MachineThingName.value,
            shiftReq.SelectedDate = this.productionAvailabilityForm.controls.SelectedDate.value,
            await this.getShiftLists(shiftReq);
          // TODO : REMOVE IT LATER 
          // await this.getAvailabilityDetails(this.productionAvailabilityForm.value);
          // await this.getPullCardDetails(this.productionAvailabilityForm.value);
          await this.loadingProvider.hideLoader();
        })
      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }

  ShiftDropdownSelected(event) {
    console.log(event);
    new Promise((resolve) => {
      this.productionAvailabilityForm.patchValue({
        ShiftName: event.detail.value || ''
      })
      resolve();
    }).then(async () => {
      await this.loadingProvider.showLoaderWithBackDropWithoutTimer();
      await this.getAvailabilityDetails(this.productionAvailabilityForm.value);
      await this.getPullCardDetails(this.productionAvailabilityForm.value);
      await this.getTopDowntimeReasons(this.productionAvailabilityForm.value);
      await this.loadingProvider.hideLoader();
    })
  }

  getShiftLists(data) {
    this.productionService.getShiftList(data).then(
      (getShiftListsResponse: any) => {
        console.log({ getShiftListsResponse });
        this.getShiftListsData = getShiftListsResponse!.rows || [];
        new Promise((resolve) => {

          if (this.getShiftListsData.length > 0) {
            this.productionAvailabilityForm.patchValue({
              ShiftName: this.getShiftListsData[0].ShiftName
            })
          }
          resolve();
        }).then(() => {
          this.getAvailabilityDetails(this.productionAvailabilityForm.value);
          this.getPullCardDetails(this.productionAvailabilityForm.value);
          this.getTopDowntimeReasons(this.productionAvailabilityForm.value);
        })

      }).catch((getShiftListsError) => {
        // this.getShiftListsData = {}; 
        this.getShiftListsData = [];
        console.log({ getShiftListsError });
      })
  }

  getAvailabilityDetails(data) {
    this.productionService.getAvailabilityDetails(data).then((getAvailabilityDetailsResponse: any) => {
      console.log({ getAvailabilityDetailsResponse });
      this.getAvailabilityDetailsData = getAvailabilityDetailsResponse!.rows[0] || {};
    }).catch((getAvailabilityDetailsError) => {
      console.log({ getAvailabilityDetailsError });
      this.getAvailabilityDetailsData = {};
    }
    )
  }


  getPullCardDetails(data) {
    this.productionService.getPullCardDetails(data).then((getPullCardDetailsResponse: any) => {
      console.log({ getPullCardDetailsResponse });
      this.getPullCardDetailsData = getPullCardDetailsResponse!.rows[0] || {};
      console.log(this.getPullCardDetailsData);
    }).catch((getPullCardDetailsError) => {
      console.log({ getPullCardDetailsError });
      this.getPullCardDetailsData = {};
    });
  }

  getTopDowntimeReasons(data) {
    this.productionService.getTopDowntimeReasons(data).then((getTopDowntimeReasonsResponse: any) => {
      console.log({ getTopDowntimeReasonsResponse });
      this.getTopDowntimeReasonsData = getTopDowntimeReasonsResponse!.rows || [];
      console.log(this.getTopDowntimeReasonsData);
    }).catch((getTopDowntimeReasonsError) => {
      console.log({ getTopDowntimeReasonsError });
      this.getTopDowntimeReasonsData = [];
    });
  }

}
