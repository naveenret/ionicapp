import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductionAvailabilityPage } from './production-availability.page';

const routes: Routes = [
  {
    path: '',
    component: ProductionAvailabilityPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductionAvailabilityPageRoutingModule {}
