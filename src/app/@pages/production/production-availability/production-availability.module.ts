import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductionAvailabilityPageRoutingModule } from './production-availability-routing.module';

import { ProductionAvailabilityPage } from './production-availability.page';

import { ComponentModule } from 'src/app/@components/component.module';
import { ReactiveFormsModule }  from '@angular/forms';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductionAvailabilityPageRoutingModule,
    ComponentModule,
    ReactiveFormsModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [ProductionAvailabilityPage]
})
export class ProductionAvailabilityPageModule {}
