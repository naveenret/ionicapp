import { Component, OnInit } from '@angular/core';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { ProductionService } from 'src/app/@service/production.service';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder } from '@angular/forms';

import * as moment from 'moment-timezone';
import { LoadingProvider } from 'src/app/@core/mock/loading';
@Component({
  selector: 'production-performance',
  templateUrl: './production-performance.page.html',
  styleUrls: ['./production-performance.page.scss'],
})
export class ProductionPerformancePage implements OnInit {


  customAlertOptions: any = {
    header: 'Select One',
    translucent: true,
  };

  productionPerformanceData: any;
  getHourlyProtectionData: any[] = [];
  getShiftListsData: any[] = [];
  currentActiveItem: any;
  productionPerformanceForm: FormGroup;
  constructor(
    public datePicker: DatePicker,
    public productionService: ProductionService,
    public activatedRoute: ActivatedRoute,
    public fb: FormBuilder,
    public loadingProvider: LoadingProvider
  ) {

    this.productionPerformanceForm = this.fb.group({
      MachineThingName: 'BConfig_Machine_AssemblyLine',
      SelectedDate: moment(new Date()).format('x'),
      ShiftName: ''
    })
  }

  async ngOnInit() {
    await this.loadingProvider.showLoaderWithBackDropWithoutTimer();
    await this.getQueryParams();
    await this.loadingProvider.hideLoader();
  }

  async doRefresh(event) {
    console.log('Begin async operation');
    await this.ngOnInit();
    await event.target.complete();
  }


  getQueryParams() {
    this.activatedRoute.queryParams.subscribe(res => {
      console.log(res);
      console.log(JSON.parse(res.currentActiveItem));
      new Promise((resolve) => {
        this.currentActiveItem = JSON.parse(res.currentActiveItem);
        this.productionPerformanceForm.patchValue({
          MachineThingName: res.currentActiveItem.MachineThingName || 'BConfig_Machine_AssemblyLine'
        });
        resolve();
      }).then(() => {

        let shiftReq: any = {};
        shiftReq.MachineThingName = this.productionPerformanceForm.controls.MachineThingName.value,
          shiftReq.SelectedDate = this.productionPerformanceForm.controls.SelectedDate.value,
          this.getShiftLists(shiftReq);
        this.getHourlyProtection(this.productionPerformanceForm.value);
        this.getPerformanceDetails(this.productionPerformanceForm.value);
      })


    })
  }


  getDate() {
    if (new Date(moment.unix(this.productionPerformanceForm.controls.SelectedDate.value / 1000)).toString() == 'Invalid Date') {
      return new Date();
    } else {
      return new Date(moment.unix(this.productionPerformanceForm.controls.SelectedDate.value / 1000));
    }
  }


  showDate() {
    this.datePicker.show({
      date: this.getDate(),
      mode: 'date',
      maxDate: new Date().valueOf(),
      // androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_DARK
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
      // androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT
      // androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => {
        console.log('Got date: ', date);
        new Promise((resolve) => {
          this.productionPerformanceForm.patchValue({
            SelectedDate: moment(date).format('x')
          });
          resolve();
        }).then(async () => {
          await this.loadingProvider.showLoaderWithBackDropWithoutTimer();

          let shiftReq: any = {};
          shiftReq.MachineThingName = this.productionPerformanceForm.controls.MachineThingName.value,
            shiftReq.SelectedDate = this.productionPerformanceForm.controls.SelectedDate.value,
            await this.getShiftLists(shiftReq);
          // await this.getHourlyProtection(this.productionPerformanceForm.value);
          // await this.getPerformanceDetails(this.productionPerformanceForm.value);
          await this.loadingProvider.hideLoader();
        })
      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }


  getShiftLists(data) {
    this.productionService.getShiftList(data).then(
      (getShiftListsResponse: any) => {
        console.log({ getShiftListsResponse });
        this.getShiftListsData = getShiftListsResponse!.rows || [];
        new Promise((resolve) => {

          if (this.getShiftListsData.length > 0) {
            this.productionPerformanceForm.patchValue({
              ShiftName: this.getShiftListsData[0].ShiftName
            })
          }
          resolve();
        }).then(() => {
          this.getHourlyProtection(this.productionPerformanceForm.value);
          this.getPerformanceDetails(this.productionPerformanceForm.value);
        })

      }).catch((getShiftListsError) => {
        // this.getShiftListsData = {};
        this.getShiftListsData = []
        console.log({ getShiftListsError });
      }
      )
  }

  ShiftDropdownSelected(event) {
    console.log(event);
    new Promise((resolve) => {
      this.productionPerformanceForm.patchValue({
        ShiftName: event.detail.value || ''
      })
      resolve();
    }).then(async () => {
      await this.loadingProvider.showLoaderWithBackDropWithoutTimer();
      await this.getHourlyProtection(this.productionPerformanceForm.value);
      await this.getPerformanceDetails(this.productionPerformanceForm.value);
      await this.loadingProvider.hideLoader();
    })
  }


  getPerformanceDetails(data) {
    this.productionService.productionPerformanceStatus(data).then(
      (productionPerformanceResponse: any) => {
        console.log({ productionPerformanceResponse });
        this.productionPerformanceData = productionPerformanceResponse!.rows[0] || {};
      }).catch((getAvailabilityDetailsError) => {
        console.log({ getAvailabilityDetailsError });
        this.productionPerformanceData = {};
      }
      )
  }

  getHourlyProtection(data) {
    this.productionService.getHourlyProtection(data).then((getHourlyProtectionResponse: any) => {
      console.log({ getHourlyProtectionResponse });
      this.getHourlyProtectionData = getHourlyProtectionResponse!.rows || [];
      // this.getHourlyProtectionData.map(el=>moment(el.HourlyEndTime).format('hh:mm'));
      console.log(this.getHourlyProtectionData);
    }).catch((getAvailabilityDetailsError) => {
      console.log({ getAvailabilityDetailsError });
      this.getHourlyProtectionData = [];
    }
    )
  }


}
