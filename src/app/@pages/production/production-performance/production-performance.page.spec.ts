import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProductionPerformancePage } from './production-performance.page';

describe('ProductionPerformancePage', () => {
  let component: ProductionPerformancePage;
  let fixture: ComponentFixture<ProductionPerformancePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductionPerformancePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProductionPerformancePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
