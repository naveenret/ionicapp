import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductionPerformancePage } from './production-performance.page';

const routes: Routes = [
  {
    path: '',
    component: ProductionPerformancePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductionPerformancePageRoutingModule {}
