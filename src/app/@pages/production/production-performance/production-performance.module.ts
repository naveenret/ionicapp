import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductionPerformancePageRoutingModule } from './production-performance-routing.module';

import { ProductionPerformancePage } from './production-performance.page';
import { ComponentModule } from 'src/app/@components/component.module';
import { SharedModule } from 'src/app/@shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    SharedModule,
    ProductionPerformancePageRoutingModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  declarations: [ProductionPerformancePage]
})
export class ProductionPerformancePageModule {}
