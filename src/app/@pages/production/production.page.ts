import { Component, OnInit, DoCheck } from '@angular/core';
import { ProductionService } from 'src/app/@service/production.service';
import { HomeService } from 'src/app/@service/home.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadingProvider } from 'src/app/@core/mock/loading';

@Component({
  selector: 'production',
  templateUrl: './production.page.html',
  styleUrls: ['./production.page.scss'],
})
export class ProductionPage implements OnInit, DoCheck {

  forwardIcon: any = true;
  downIcon: any = false;
  dropdownList: any = false;
  productionDDList: any[] = [];
  dropdownTitle: any = "";
  getProductionChartData: any;
  currentSelectedItem: any = null;
  currentActiveItem: any;
  constructor(
    public productionService: ProductionService,
    public homeService: HomeService,
    public activatedRoute: ActivatedRoute,
    public router: Router,
    public loadingProvider: LoadingProvider
  ) { }

  async ngOnInit() {
    await this.loadingProvider.showLoaderWithBackDropWithoutTimer();
    await this.getMechineListDD();
    await this.loadingProvider.hideLoader();
  }

  async doRefresh(event) {
    console.log('Begin async operation');
    await this.ngOnInit();
    await event.target.complete();
  }

  ngDoCheck() {
    let a = this.productionDDList.length;
    this.productionDDList = this.productionDDList.slice(0, a + 1);
  }

  arrowClick() {
    if (this.forwardIcon == true) {
      this.forwardIcon = false;
      this.downIcon = true;
      this.dropdownList = true;
    } else {
      this.downIcon = false;
      this.forwardIcon = true;
      this.dropdownList = false;
    }
  }

  dropdownClick(item, index) {
    console.log(item);
    this.currentSelectedItem = item;
    this.dropdownTitle = item.MahineDisplayName;
    this.productionDDList.splice(index, 1);
    this.productionDDList.unshift(item);
    this.dropdownList = false;
    this.arrowClick();
    this.getProductionChart({ "MachineThingName": item.MachineThingName });
    this.currentActiveItem = JSON.stringify(item);
  }
  closeMenu(event) {
    console.log(event.path)
    if (this.checkPathThatContainSelector(event.path, 'nk-prod-dropdown-list')) {
      return;
    }
    console.log('click outside');
    this.dropdownList = this.dropdownList == true ? false : true;
  }
  checkPathThatContainSelector(path, selector) {
    for (let i = 0; i < path.length; i++) {
      if (path[i].classList && path[i].classList.contains(selector)) {
        return true;
      }
    }
    return false;
  }

  getProductionChart(data) {
    this.productionService.getProductionChart(data).then((getProductionChartResponse: any) => {
      console.log({ getProductionChartResponse });
      this.getProductionChartData = getProductionChartResponse!.rows[0] || {};
    }).catch((getProductionChartError) => {
      this.getProductionChartData = {}
      console.log({ getProductionChartError });
    });
  }

  getMechineListDD() {
    this.homeService.getMechineStatusList().then((response: any) => {
      let mechineStatus = response.rows || [];
      console.log(mechineStatus);
      this.productionDDList = mechineStatus.filter(res => res.NavigateTo == 'OEE') || [];
      console.log(this.productionDDList);
      this.selectDropdownFromRouter();
    }).catch((err: any) => {
      this.productionDDList = []
    })
  }


  selectDropdownFromRouter() {
    this.activatedRoute.queryParams.subscribe(res => {
      console.log(res);
      this.currentSelectedItem = res;
      if (res && res.mechineList) {
        this.currentActiveItem = JSON.stringify(res.mechineList);
        this.productionDDList = this.productionDDList.filter(re => re != res.mechineList);
        this.dropdownTitle = res.mechineList.MahineDisplayName || '';
        console.log(res.mechineList.MachineThingName);
        this.getProductionChart({ "MachineThingName": res.mechineList.MachineThingName });
      } else {
        if (this.productionDDList.length > 0) {
          let dummy = this.productionDDList || [];
          // TODO : CONTINUE
          // this.dropdownTitle = dummy[0].MahineDisplayName || '';
          // this.currentActiveItem = JSON.stringify(dummy[0]) ;
          // this.productionDDList = this.productionDDList.filter(re => re.MahineDisplayName != dummy[0].MahineDisplayName);
          // this.getProductionChart({ "MachineThingName": dummy[0].MachineThingName });
          // TODO :  // if its not working try with todo continue and remove this 
          new Promise((resolve) => {
            this.dropdownClick(dummy[0], 0);
            //? this.dropdownClick working here checked 
            resolve();
          }).then(() => {
            this.arrowClick();
          });
        }
      }
    })
  }

  noPathClick() {
    console.log(this.currentActiveItem);
    this.router.navigate(['/layouts/nopath', { queryParams: { currentItem: this.currentActiveItem } }]);
  }

}
