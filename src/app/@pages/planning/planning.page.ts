import { Component, OnInit } from '@angular/core';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { FormGroup, FormBuilder } from '@angular/forms';
import * as moment from 'moment-timezone';
import { PlanningService } from 'src/app/@service/planning.service';
import { LoadingProvider } from 'src/app/@core/mock/loading';


declare var $: any;
@Component({
  selector: 'planning',
  templateUrl: './planning.page.html',
  styleUrls: ['./planning.page.scss'],
})
export class PlanningPage implements OnInit {
  planningFooter: any = false;
  planningFooterData: any;
  getPlanningUnplannedData: any[] = [];
  items: any[] = [
    { expanded: false },
    { expanded: false },
    { expanded: false },
  ];
  planningShiftForm: FormGroup;
  getPlanningShiftDetailsData: any;

  constructor(
    public datePicker: DatePicker,
    public fb: FormBuilder,
    public planningService: PlanningService,
    public loadingProvider: LoadingProvider
  ) {
    this.planningShiftForm = this.fb.group({
      SelectedDate: moment(new Date()).format('x'),
      ShiftName: null
    })
  }

  getDate() {
    if (new Date(moment.unix(this.planningShiftForm.controls.SelectedDate.value / 1000)).toString() == 'Invalid Date') {
      return new Date();
    } else {
      return new Date(moment.unix(this.planningShiftForm.controls.SelectedDate.value / 1000));
    }
  }


  showDate() {
    this.datePicker.show({
      date: this.getDate(),
      mode: 'date',
      maxDate: new Date().valueOf(),
      // androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_DARK
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
      // androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT
      // androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => {
        console.log('Got date: ', date);
        new Promise((resolve) => {
          this.planningShiftForm.patchValue({
            SelectedDate: moment(date).format('x')
          });
          resolve();
        }).then(async () => {
          this.planningFooter = false;
          await this.loadingProvider.showLoaderWithBackDropWithoutTimer();
          await this.getPlanningShiftDetails(this.planningShiftForm.value);
          await this.loadingProvider.hideLoader();
        })

      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }

  getShiftbyItem(item, id, i) {
    if (item.expanded == false) {
      let data: any = {};
      data.ShiftName = item.ShiftName;
      data.SelectedDate = this.planningShiftForm.controls.SelectedDate.value;
      // let data = { "SelectedDate": "1583735656000", "ShiftName": "Shift1" }
      this.planningService.getPlanningShiftDetailsByItem(data).then((getDifferentialDetailsRes: any) => {
        console.log({ getDifferentialDetailsRes });
        this.getPlanningShiftDetailsData[i].shiftList = getDifferentialDetailsRes!.rows || [];
        this.expandItem(item, id);
        this.getFooterData(item);
      }).catch((err) => {
        this.getPlanningShiftDetailsData[i].shiftList = [];
        console.log({ err });
        this.planningFooter = false;
      })
    } else {
      this.planningFooter = false;
      this.expandItem(item, id);
    }
  }


  getFooterData(item) {
    let data: any = {};
    data.ShiftName = item.ShiftName;
    data.SelectedDate = this.planningShiftForm.controls.SelectedDate.value;
    // let data = { "SelectedDate": "1583735656000", "ShiftName": "Shift1" }
    this.planningService.getFooterPlanningData(data).then((getFooterPlanningDataRes: any) => {
      console.log({ getFooterPlanningDataRes });
      this.planningFooterData = getFooterPlanningDataRes!.rows[0] || {};
      this.planningFooter = true;
    }).catch((err) => {
      this.planningFooterData = {};
      console.log({ err });
      this.planningFooter = false;
    })
  }

  expandItem(item, id): void {
    // 
    console.log(item, id)
    if (item.expanded) {
      item.expanded = false;
    } else {
      this.getPlanningShiftDetailsData.map(listItem => {
        if (item == listItem) {
          listItem.expanded = !listItem.expanded;
        } else {
          listItem.expanded = false;
        }
        let scroolll = document.getElementById(id);
        if (scroolll) {
          scroolll.scrollIntoView({ behavior: "smooth", block: "end" });
        }
        return listItem;
      });
    }
    // if (index == 0) {
    //   this.planningFooter = this.planningFooter == true ? false : true;
    // }
  }

  async ngOnInit() {
    this.planningFooter = false;
    await this.loadingProvider.showLoaderWithBackDropWithoutTimer();
    await this.getPlanningShiftDetails(this.planningShiftForm.value);
    await this.loadingProvider.hideLoader();
  }

  async doRefresh(event) {
    console.log('Begin async operation');
    await this.ngOnInit();
    await event.target.complete();
  }

  getPlanningShiftDetails(data) {
    this.planningService.getPlanningShiftDetails(data).then((getPlanningShiftDetailsResponse: any) => {
      console.log({ getPlanningShiftDetailsResponse });
      this.getPlanningShiftDetailsData = getPlanningShiftDetailsResponse!.rows || [];
      this.getPlanningShiftDetailsData.map(el => el.expanded = false);
    }).catch((getPlanningShiftDetailsError) => {
      this.getPlanningShiftDetailsData = [];
      console.log({ getPlanningShiftDetailsError })
    }
    )
  }

  openUnplannedModal(item) {
    console.clear();

    let data: any = {};
    data.ShiftName = item.ShiftName;
    data.SelectedDate = this.planningShiftForm.controls.SelectedDate.value;
    // let data = { "SelectedDate": "1583735656000", "ShiftName": "Shift1" }
    this.planningService.getPlanningUnplannedData(data).then((getPlanningUnplannedDataResponse: any) => {
      console.log({ getPlanningUnplannedDataResponse });
      this.getPlanningUnplannedData = getPlanningUnplannedDataResponse!.rows || [];
      console.log('getPlanningUnplannedData', this.getPlanningUnplannedData);
      $('#unplanned').modal('show');
    }).catch((getPlanningUnplannedDataError) => {
      $('#unplanned').modal('hide');
      this.getPlanningUnplannedData = [];
      console.log({ getPlanningUnplannedDataError })
    }
    )
  }
}
