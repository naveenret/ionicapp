import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TraceablityDetailsPageRoutingModule } from './traceablity-details-routing.module';

import { TraceablityDetailsPage } from './traceablity-details.page';
import { SharedModule } from 'src/app/@shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TraceablityDetailsPageRoutingModule,
    SharedModule
  ],
  declarations: [TraceablityDetailsPage]
})
export class TraceablityDetailsPageModule {}
