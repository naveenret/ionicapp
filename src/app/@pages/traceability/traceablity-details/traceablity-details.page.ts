import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TraceabilityService } from 'src/app/@service/traceability.service';
import { NavController } from '@ionic/angular';

declare var $: any;

@Component({
  selector: 'traceablity-details',
  templateUrl: './traceablity-details.page.html',
  styleUrls: ['./traceablity-details.page.scss'],
})
export class TraceablityDetailsPage implements OnInit {


  currentSelectedItem: any;
  currentImage: any;
  traceDetailData: any;
  traceAssemblyData: any;
  traceEmployeeData: any;
  traceNutRunnerData: any;
  traceDifferentialData: any;
  traceAirLeakData: any;
  tracePostAssemblyData: any;
  constructor(
    public router: Router,
    public activatedRoute: ActivatedRoute,
    public traceabilityService: TraceabilityService,
    public navCtrl: NavController
  ) {

  }



  ngOnInit() {

    var dropClass = document.getElementsByClassName('planning-header-wrapper');
    for (var i = 0; i < dropClass.length; i++) {
      dropClass[i].addEventListener('click', (e: any) => {
        console.log(e.target.id);
        console.log(e);
        let nextChild = e.target.id
      })
    }
    this.getQueryParams();
  }

  getQueryParams() {
    new Promise((resolve) => {

      this.activatedRoute.queryParams.subscribe(res => {
        console.log(res);
        if (res && res.item)
          this.currentSelectedItem = res.item;
      })
      resolve();
    }).then(() => {
      this.getTracePartImage({ "NXPartNumber": this.currentSelectedItem.NXPartNumber })
    })
  }



  getTracePartImage(data) {
    this.traceabilityService.getTraceabilityPartImage(data).then((getTraceabilityPartImageRes:any) => {
      console.log({ getTraceabilityPartImageRes });
      this.currentImage = getTraceabilityPartImageRes!.rows[0] || {};
      // console.log(JSON.stringify(this.currentImage.PartImage));
    }).catch((getTraceabilityPartImageErr:any) => {
      this.currentImage = {};
      console.log({ getTraceabilityPartImageErr });
    });
  }

  getPostAssemblyDetails(item, id) {
    if (item.expanded == false) {
      let data: any = {};
      data.StartDate = this.currentSelectedItem.StartTime;
      data.EndDate = this.currentSelectedItem.EndTime;
      data.IoTSerialNumber = this.currentSelectedItem.IoTSerialNumber;
      this.traceabilityService.getPostAssemblyDetails(data).then((getPostAssemblyDetailsRes:any) => {
        console.log({ getPostAssemblyDetailsRes });
        this.tracePostAssemblyData = getPostAssemblyDetailsRes!.rows || [];
        this.expandItem(item, id);
      }).catch((err) => {
        this.tracePostAssemblyData = [];
        console.log({ err });
      })
    } else {
      this.expandItem(item, id);
    }
  }

  getAirLeakDetails(item, id) {
    if (item.expanded == false) {
      let data: any = {};
      data.StartDate = this.currentSelectedItem.StartTime;
      data.EndDate = this.currentSelectedItem.EndTime;
      data.IoTSerialNumber = this.currentSelectedItem.IoTSerialNumber;
      this.traceabilityService.getAirLeakDetails(data).then((getAirLeakDetailsRes:any) => {
        console.log({ getAirLeakDetailsRes });
        this.traceAirLeakData = getAirLeakDetailsRes!.rows || [];
        this.expandItem(item, id);
      }).catch((err) => {
        this.traceAirLeakData = [];
        console.log({ err });
      })
    } else {
      this.expandItem(item, id);
    }
  }

  getNutRunnerDetails(item, id) {
    if (item.expanded == false) {
      let data: any = {};
      data.StartDate = this.currentSelectedItem.StartTime;
      data.EndDate = this.currentSelectedItem.EndTime;
      data.IoTSerialNumber = this.currentSelectedItem.IoTSerialNumber;
      this.traceabilityService.getNutRunnerDetails(data).then((getNutRunnerDetailsRes:any) => {
        console.log({ getNutRunnerDetailsRes });
        this.traceNutRunnerData = getNutRunnerDetailsRes!.rows || [];
        this.expandItem(item, id);
      }).catch((err) => {
        this.traceNutRunnerData = [];
        console.log({ err });
      })
    } else {
      this.expandItem(item, id);
    }
  }

  getEmployeeDetails(item) {
    console.log({ item });
    console.log(this.currentSelectedItem);
    let data: any = {};
    data.ProcessStartDate = item.AssemblyStartTime;
    data.ProcessEndDate = item.AssemblyEndTime;
    data.ShiftName = this.currentSelectedItem.IoTSerialNumber;
    data.ShiftSelectedDate = this.currentSelectedItem.ShiftSelectedDate;
    data.StationNo = item.StationNo;
    data.ProcessName = item.ProcessName;
    this.traceabilityService.getEmployeeDetails(data).then((getTraceabilityDetailsRes:any) => {
      console.log({ getTraceabilityDetailsRes });
      this.traceEmployeeData = getTraceabilityDetailsRes!.rows || [];
    }).catch((err) => {
      this.traceEmployeeData = [];
      console.log({ err });
    })
  }

  getAssemblyDetails(item, id) {
    if (item.expanded == false) {
      let data: any = {};
      data.StartDate = this.currentSelectedItem.StartTime;
      data.EndDate = this.currentSelectedItem.EndTime;
      data.IoTSerialNumber = this.currentSelectedItem.IoTSerialNumber;
      this.traceabilityService.getAssemblyDetails(data).then((getTraceabilityDetailsRes:any) => {
        console.log({ getTraceabilityDetailsRes });
        this.traceAssemblyData = getTraceabilityDetailsRes!.rows || [];
        this.expandItem(item, id);
      }).catch((err:any) => {
        this.traceAssemblyData = [];
        console.log({ err });
      })
    } else {
      this.expandItem(item, id);
    }
  }

  getTraceabilityDetails(item, id) {
    if (item.expanded == false) {
      let data: any = {};
      data.StartDate = this.currentSelectedItem.StartTime;
      data.EndDate = this.currentSelectedItem.EndTime;
      data.IoTSerialNumber = this.currentSelectedItem.IoTSerialNumber;
      this.traceabilityService.getTraceabilityDetails(data).then((getTraceabilityDetailsRes:any) => {
        console.log({ getTraceabilityDetailsRes });
        this.traceDetailData = getTraceabilityDetailsRes!.rows[0] || {};
        this.expandItem(item, id);
      }).catch((err:any) => {
        this.traceDetailData = {};
        console.log({ err });
      })
    } else {
      this.expandItem(item, id);
    }
  }

  getDifferentialDetails(item, id) {
    if (item.expanded == false) {
      let data: any = {};
      data.StartDate = this.currentSelectedItem.StartTime;
      data.EndDate = this.currentSelectedItem.EndTime;
      data.IoTSerialNumber = this.currentSelectedItem.IoTSerialNumber;
      this.traceabilityService.getDifferentialDetails(data).then((getDifferentialDetailsRes:any) => {
        console.log({ getDifferentialDetailsRes });
        this.traceDifferentialData = getDifferentialDetailsRes!.rows || [];
        this.expandItem(item, id);
      }).catch((err) => {
        this.traceDifferentialData = [];
        console.log({ err });
      })
    } else {
      this.expandItem(item, id);
    }
  }

  detailItems: any = [
    { model: 'HSGR No.', number: 'MNJFS324345MJ' },
    { model: 'HSGR No.', number: 'MNJFS324345MJ' },
    { model: 'HSGR No.', number: 'MNJFS324345MJ' },
    { model: 'HSGR No.', number: 'MNJFS324345MJ' },
    { model: 'HSGR No.', number: 'MNJFS324345MJ' },
    { model: 'HSGR No.', number: 'MNJFS324345MJ' }
  ]
  assemblyList: any = [
    { item1: 'HSG Drop/Clean', item2: '-', item3: '18-03-2002  14:75', item4: '18-03-2002  14:75' },
    { item1: 'HSG Drop/Clean', item2: '-', item3: '18-03-2002  14:75', item4: '18-03-2002  14:75' },
    { item1: 'HSG Drop/Clean', item2: '-', item3: '18-03-2002  14:75', item4: '18-03-2002  14:75' },
    { item1: 'HSG Drop/Clean', item2: '-', item3: '18-03-2002  14:75', item4: '18-03-2002  14:75' },
    { item1: 'HSG Drop/Clean', item2: '-', item3: '18-03-2002  14:75', item4: '18-03-2002  14:75' },
  ]
  postAssemblyList: any = [
    { item1: 'Degreasing', item2: '-', item3: '18-03-2002  14:75', item4: '18-03-2002  14:75' },
    { item1: 'Degreasing', item2: '-', item3: '18-03-2002  14:75', item4: '18-03-2002  14:75' },
    { item1: 'Degreasing', item2: '-', item3: '18-03-2002  14:75', item4: '18-03-2002  14:75' },
    { item1: 'Degreasing', item2: '-', item3: '18-03-2002  14:75', item4: '18-03-2002  14:75' },
    { item1: 'Degreasing', item2: '-', item3: '18-03-2002  14:75', item4: '18-03-2002  14:75' },
  ]

  items: any[] = [
    { label: 'Details', expanded: false },
    { label: 'Assembly details', expanded: false },
    { label: 'Nut Runner Details', expanded: false },
    { label: 'Air leak details', expanded: false },
    { label: 'Post assembly details', expanded: false },
    { label: 'Differential Check Details', expanded: false },
  ]

  modalItems: any = [
    { name: 'Shilpamanikumari', id: '550239' },
    { name: 'Shilpamanikumari', id: '550239' },
    { name: 'Shilpamanikumari', id: '550239' },
    { name: 'Shilpamanikumari', id: '550239' },
    { name: 'Shilpamanikumari', id: '550239' },
  ]

  expandItem(item, id): void {
    console.log(item, id)
    if (item.expanded) {
      item.expanded = false;
    } else {
      this.items.map(listItem => {
        if (item == listItem) {
          listItem.expanded = !listItem.expanded;
        } else {
          listItem.expanded = false;
        }
        let scroolll = document.getElementById(id);
        if (scroolll) {
          scroolll.scrollIntoView({ behavior: "smooth", block: "end" });
        }
        return listItem;
      });
    }
  }

  expandItems(id) {
    console.log(id);
    let targetId = id + '-expand';
    console.log(document.getElementById(targetId).classList.contains('collapsed'));
    if (document.getElementById(targetId).classList.contains('collapsed')) {
      document.getElementById(targetId).classList.remove('collapsed');
    } else {
      document.getElementById(targetId).classList.add('collapsed');
    }
  }

  toggleTabs() {

  }

  postAssemblyListClick(item) {
    console.log(item);
    this.navCtrl.navigateForward('/layouts/post-assembly-details', { animationDirection: 'forward', queryParams: { item } });
  }

  openAssesmblyDetailModal(item) {
    console.log(item);
    $('#assembly-details-modal').modal('show');
    this.getEmployeeDetails(item);
  }


  // traceIMage = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAXEAAACICAMAAAAmsyvzAAAAhFBMVEX///8cY7cAWLMAVrIAWrQAVbIXYbbu8vjI1usAXLQAU7ESX7bb5fL3+v3y9vvA0Ojf5/Pl7PbR3e64yuVmj8mxxeKmvN6Zs9ooarpUg8Ryl82GpdMxb7zM2eyMqdWuwuF7ndA+db9ciMZPgMNDecCVsNiCotKft9t2ms4ubbtsk8sAQKu1Pp/rAAAVP0lEQVR4nO1deXeqPhO+bEFAVNxrq9alWn2///d72ZRkZhISiO05Pb/nr3sPFcJDMnsm//79h7+Baaa8HCqvpurL/4HALo73isvz+KAgdejGO+sj+utYMxbIZ3kYO9FJ/uP3yIlnLxjUn8Zb4iRn6dWR6zjeUnp5FTnMnZg8bjj7QTmUpqMsm0yGObIwTX/uwUrMCk4X0svHxGG+dA1kgeMka4NX2cRuPDAanznScLq4fs5PH+soiIPAKxEEceQcTvPLdbENf5l7xhyWSMcw8BzVGsg/iBO9aT9rVdxtbTY+E6TT5efZcT03ipLivRwe+f9ZEkWuF7CP424/1Vlsk7sTJQlz1h+H09t8c3nffV2vX7v3y2b+djp8rJ3ijred2Se8+jlp39LLH0wlV4oP4rjvmo9Kg4KDYGo0Pl2kg93B9fyCnzYUzPue68y/BmraR24uNmskOaIn8v+w4puWcI9mI/WZioV9/kGYK5Ur6+L9dLXnV1S8b3Q1Gp8WsuVb4EbtXIvEJ5Ebs9P3arbNaCk/9vRuFJlN8kvOQ/IhvVx8kERqr6wKEnW1Z0VJdDEaXjvC5Tmf22ZsN2yV8z3wkvXhDS3lSazzEZmCPRKF+nP8lezyruDUlcmVsJwFetpz6ZcDTMzWYBsGc983nNwkbTlx3hzefMEK8SNfPMVl3z8YmWs55sX8iGUSLSw+iNxmL3+spz3X1cDZwXB8Kuxvnobg1gVBwiicjL9O9BJK3NNqPOlg+kwLThMpZ5viaUxmr2yD8uF+u/ZcuPW88I1HKMOSuTTdhaLj1Vyh5TQgnVeTD+L3zHhuP3EubufJ1N+wFByuTN/dqrG0a8/noGNL9vDe8QkaCnXo307HzWW3+1pdV1+73JQ7fUSx50dty0E+shT/1FRf8ihVMnNkNzgVD5PaK7V0Zn7LB2/0vtd5avCYHtD8ZjnZt8/lgBzpaDr7mjuxG8l1LIvkj9ujr9vL5ip9BakNUXEl84PSWm+xFu15fr6pt+0x1MdT7zFgjkXe+jIetfxuNNidfV8y15nC5hghQ1ERjmrHshSxwVByuVJ5MrnyHdXfXKk9a3lf3kgeVNDFOIkAWd56Jxs+RDi7Jy4105kiaFeJXv6Pkz4vkJb2j9SIqAQH8+iPOnx8faX2PDWvGEktUV18BuLbJ/FpbHSDdPYWg29W3Oau+Mk3XFOqz9OO0uiWW93VC8q+yFNeKLTnkOOorwsUfohksfioO705ZJcY2tnRp+Lvv8AXUn6edoRupR0lYvC9eppLT87FQ6kwT/rmc26G9HSBtqLGZN6pA98F0h2QLdGX4q+h6lR+Hg2URreTIKerQlZPcomV8VREUu054fWOSkG1Yy+qsMjpkRIJ5wH/9Xx5JqBxJ54P1o3fSVALY08SvK6nqMReuTwXnEx7fvNLspfOWQkcsXjTz7gfJ9w091UfbwbnuGpB6KDSbEwSvJ7WYpiWK1ncDJr+8mtBEERtNpwcO0FnJpGZwiQwOjWTwVXdbQDneN8Q6KCa5L4kU/ywjQJSrpwaQmntKSYH5MHfNoiE+6fun67B/Tl5XZWjMAUGua9KyGvh4a3Tonr2CIqQ9sqsGQ1zKT0mhiW8rjmJlUC499nxNgDzSGdciPHeXkWti2Uhq+eDSPHFjYTUnieBcaW8VGAR83eJ+wrSJz5qWS43tf5hxt3+RQx1eM2jV8vqMRFIz3THaUYqfTEXzLCOInAqEO5ZI/zpLCj99iGc4/0ZrzllCSkc08eCJuVKxqsVQnt+Cu5DNxcoFO6hnVzVQWUbO4Eq6zmxz3hYu2DRhrz8JI2UK0d+EmPtuRLY6uYCHfhHRBLPoRsetrGK8QxEFvzeZlLDaUyq7MlzTVP2ykBwcVDeU3TYpOkN5ej4W9guwahMMZnLXQIyrjQlNfFYNxKj/DmNyUSqYHGjvOdYNGY78DXmhbjM+e2Makb4P834k1PaHtk+p7FLXBflBvQ9RUUvURUqpEL2WBpy64oqmav0zF7C+INTSb7n8HxpwowaxeJ4RE8qE9WOJO6rwF0IE3SRSmoUS/QXGH9ySqeZG2FM2St3MQ4XCwMaJf1coIHo+nQMFipQRL/VjIcvYfzpO9Jp5iYhTgQDpuKIwDpx+rlAgpZQ1Ox1xjUyZtxOxerzzRgVkuOC8jGeZgcxvs9u/EXRzY8M5fBKMHUCG9EUgEK1M/YLjF8fr0bG2zlZndzQ1SUMZ/Ims+jmG8aWR4LatF5FV6BQ7fJqhgIvYjx9JqKISfz0zQpguTKCSSzeDxelvCzzIcG7YAcpPcOuSPPJlCiLxV7EePNy5OMnnEGC4yufMFfLFefvhGtmWVnRDEpol7gvjn4SK2XdqxhvAiQ+FW56a6Yq9oOGYEy5Knpqz6vIuJELJE7xzqFeNUb3kzq+9irGmygfi4jVyxdUY7lyhkUgzUJZiELepPQwFaSVeum/EC9jvDHyyHjTjXt7JOoXuFLsUWIAclbSSl4CV+Gu5NL7CbyMca72KCZs/CXHHJYNuNA6qGUjiC6biIabmLH7rT27r2OcCzoxfFWoMUWB8Hdc51Tvd8nEOW4Q6hyIIRn7Dr4mXsc4599RZrNAKtw+NBGDKyVFlZGbiowbuEB3G+kjC3gh41wsmwhghDypyA96wwWUdYwm6egCpaI4omsJfgIWGZ+Bt0+b8Am1hoWUJZQr1OYwr6xxEd18/aI9URv3K2jtBYuMn2GSjPNWiDSzmN+NgVxZY8arnQfi7NcXx+LvXuT+6MAi42hrQNiEMahwmlBIDe2VHdadlRUJi4E1B5eKmqF/WU5nWGQ8RuFPLnxCZHBFyQHkSojFCnOLC+BTxJqDA+m6F0TGdWGP8SK3AWwO3uQg0sxrVXbhiHRnFetbiozrhqNArMbt+JIWYI/xMk4JxCq3oYEIgYhOIIivDNAkr1JsoDRV1wUSv66iJ8PLYY/xcUEFSNXySS6cZhazvDC+sgZ+Z63stuKX0MxZAcfpFxWnRcbLqB4DhQN8PgdXKlyATBbm6xUEV+qfAzdf0wUCK6P/DqLusMd45USCHAG/IQDXEWaiASEKnpG4aeRxY5Drj/T6a4Fva6HyrDPsMV570UBDCrMU1e0C9Sh6kKJb/kxciAPWlA9isu5VsXEt2GO8LucDGpIv+MHbobfg6QITQjVQU4ch+kZ6OjAFbYBeknDThD3GHwEPUUMKSUtc+3kQJ7mYt+eVQCPjYbpCZ2ygKqfPTvjesMb4M1IECkwEQxitZrjxS/CDuFAYZ3aKJeR65IE8hjrZ/mJYY7ypTxNzPoJ2xKkuB4C3VzjrMWiGBX0ZnUI4UJDBcMnGz8Ea45zUFXtdCNoRtRiCO3gFNfBJFQSAX2j568BU+b10xD+LjHMWryswLlS1MZjsGsFWG3zdzvCxPjzOzwF7frVcoDlQF3+Cca6sAfjzQowQpZk3MH7Cy/r6p8LmZBCTUu4PfgAYhz2bD/SDNcabmB4smJ0JXmIA5iRKsPFCtlasQlsPsFdMa8vvDTCu38vSPqwx3ixcVMIMIiTgKkqw+ZxcqdNI/N9nHVwg9hcZb8xklAkTIySwwBIHCTl7pVw5ouAAuWUtCQFe8m9IlcYVRDWrYjEUsi5QezTOXim2FDFQfCH6jzoNKEAC6G9ozlGzcHFhglh+At8XVi8L8ZVc5kCLUqwv1/FmQsj4b1XAlYOxxDgXgMYbzcEbg4bBKe4h2OSSxi5q+AwMD43iKrj1pl9jlseww3A0Gsk/dzoKQ2pothjnLAjCQhbDgLDFEM4ic/YKQxvigHGt4QIhxvt7+bNbXKPojXo7nE/H432zmR+r5taRW1+co4y6LcbHXDkQDoUOQVBb1K1EFrnJBy3RFg/gQGq4QJBx9RZXHfCtf+tm1kkFVjW3flzzUZmrLcY5YUz1AYDxafEpc1yBRW6tqHAF2YX2Qgj4kv0rsmYgAicFTu/aYpyTDNSOXVBjBcYxJMoM5bX4oNZZwwUawfv3rvXLYtx7n6CbMRcVjdlivJmmtJAEJiBIM8Oui468CREKveq4QJDx/gVC27dDcb6ESxQy1YjcgH0cLogMW4w3YoMOhUITUBQ9ZJmhTK6A3LK8GXQD1JDQzja3NBxcHI+c6ZGzG2ekfrbFeBO5kOTBgAkI0sywVMJRGM2hq/d3/O3BvXW+ki7GlHCRdDoph2+Hcc6tlDQ9gYFwMc0MSyUK0H0U0E5x6JJSQL1iNX6jjREeumqTkSXGudsk9N5rpL2E3cwp1fBf1vwIzNi43dRDxlDcpx8yBOxgqLZYLTHOOUAy2wEGwsXdzBdCBcny9GAbuYYLhO5ut2AFTRdX4WFZYpxzgGRVUigQLpSFZoTulHXwewNufvuQUejG7g5xWJWqjJRZYpx7Jen0geMShR2upZU20QerRaNhIOyjaTm3DDeKKXduWGKce6a08z0KhAut36fQL5R7QSAOo+EC4TiCyUbQVoAKa/UKssQ4F6qSt546QItB6JkGr8o3uQI3P/lsHx/a5NK/kyYHo+a/lhjn4ibyEwphOZBoSKJQhfT8C/CCOj34UGrPsGuFGmPY/FeV7bbEeDOHVEVScKIJVRHoqvT4LFBCriOToTdgtxBuCyvLVAvIEuPNXVTOxQq991p+VX70JOyKqbHFBCdT7bSnqoCa/6psTzuMc0ke1YyD+Uax/DsUzVp5ZThw83VcINQlx6pYQc1/VV/TDuPcI5V5cuzn8Ca32EBf0d4LMK7TJhI1EGG+PWsFNv9VkmiHcU51KG1RlIwRnIUJf1VlYYGomLLBeg0kyPWKufSAGFcNyA7jnEGq3imPsz38bmbeolAV1QNDUsfSQ32JbCb0IePKLRh2GOd8EvWmJuzn8NKD02/KXoRwM4tOIRxRMGDhkLIKv8A439tNnV7B2R4+zdxE2ZXBPaAOtIIkuGWLvRNVf4FxTi+1WF1ESpaba89KZXVPCSCUtajDYsVeyBYxropm2mGcS4O0bSPD2R7ennysfelhfCXglgetgh98Fqm1fbS/wDg3cdsKE3DNG98Uu1YILcYy8Kr1mqUQaabup9uI+HnG+eB2qztCZGKb5V2dC9eWZIAeB9XfDyHE51b3PMTuiX6Md3F++Se2tt8gOqdwid5SB7clflELb62CH7Qbw1rTDyPGrfQf59u+te6vRAVSOYJnIKIcfJvdBqMFeofRErrTUk7/5xnnZLOGX0HMNa6qKDcf28u7gYDQPIz2hB8cWMl39mK801kpnKDQsNSIucaZ1GOP37wpATB4NFvpTXGxHdWK0RxGjKPzgLowzmWAdBIyJ6JSoqnsXGt0lAV30M0TEw/uex5piX6Md1lm3JvodN+gat4aabT8X/tHB2dN6HqP1CS30Ybi5xnnFrlWh5kbMcmb7kMaRjLcg6wblCLKpm3s0Tdi3Ma5bvy2Eq3fw6OGyzfXMqprwGSSbhvyDO6QdqycM9aL8S5nF/I5GT0PCjsjaOOEEqCEXH04CY8d8a293i1sjRhHCaMOVQX8A/V8CpwfcIyipzB5rjyeUQBRxCs5f84ARoxvoVTpUMvOlcBpVt6g/gcFDOqkpjDvps3ZlrIQ9XxWOYwYh/VhXXJRfHBKVePI4Zua5NJyCQS4QdNA+6BjQQrKWb+QVi/Gu/Tl5hygdie/woTwghym3b4qhWVnBqO+EQWOCes1y40YR+VEHToBctaxtp1GFXYaZGVAVZtJqeyQ2kZCn6Otf0sDxlHJnF7/QAGcA6Td1QHqjwqKPYUiQG7ZqPhkQa0vsTzMEEaMw2NJDM/tKsFlV/Qjzriw0zGIo4EScrM+tJ+EiZiLtO5GohHj0BnpEGfgD+HQX96o7LMcqq4GA26+rvqocST3BHqd8xNGjF9hsbl57m/ErVIDNZDgSU6d104DpjUMy8EPJOXRR0eTxYhxOPQO5Xj88wzMeSLvqG9Wwxp5/V+WSA+U4s71Z7edtUaMQ/O0Q3sd3twxMIyJ4kv9KimkfgzdiJSe5cw7dqlGNGL8DjcNmdeG8arAJIWEyj4NzAXoRpgLwzOlPvMbdXFIjBgHSr/L3lJeMJmUAoAu7EZ9ZmCQucNpMxvSPs0ly4dxOYMR46huRvcojAb8MjGSp7A1jUHYMoQzVF1SRGJF2uU55/HR8GZwxSnLq5Ew1Q/DPXDuyrhY9mm04Q91euriuQ0iUn/mosU7GmWGoHxUMZ6hUJqh1s/BLymzHJZQ9mlmJ8DNa5122o9OdOOInPPgrL/ioFBRzlq0tdQ8eDjszjif8DQ7Tha7rN3O27y6kmnuJK570RMuMxR6VkXkcGrAWOsLPpRhnparEDX80lDjm2XtGmRn2TTP7xisL9uW6PNof8A3ULXhwvUE0l2UMgi617BssYmsmz4WMW7gsIrYJ/I+QCzykrfVQPItR9PrW0QkEFXJahjZL2CYBRJLIQzDMk2TD/1sRAVcm+xEHSlPL7FMtBT0scj12en9OptOworJUTYc7HfzW+RFRKhC/e2h/1M+Qmcj0xMZeKRhWdnDljfd0Dqk4mAJ67j1O9t4Cs4r2iPf84K6xWHguT5NdvnH1CnHJdLJN+l3Mfc+zvSS46M92l4TbMYG9mVdvay3lef51GxGVbzkcNl8OezSy3Cy8aieOl2QBJRG2n69389M+mET34vWh+Pm/Us6Yxe73efbzScyxLmSX5/nl52ey16VfcKjshQYnNaJR1VflGDl0E/z7/fd1UyTZu+u9K76YFF8pybcPo6iRLoqqp/mg0+i/Pf0+G6e6g7VTz91XrQq+9TPgwxzmdtCTNlfM5cCxqeF52ZHv4me+O6FXuBk/oUGveWBqkGAYExLNBfWksH+YaqcS4IOebThbt2Z9NyYPC9lr0ycGy27DR0hymJkDqOfJnqBqWIPp4GFk3F2Rd0vVtJbk0l6PrVgujvEvnr9UzT5weFL8bxh5PklXM+rNbBXNhMOAi/wPNfNr5TXPXjqwwPL2Cv+xnXzvy9+H0ReqcNz1D/2fP2TM022+U3PTVNk53Y4vZ0/csFeD96rBl8OvXvzsWx/jwI/wntuZWx77nzZ9gqD/X6/mI0H2+lwkoW15EhHWTYcTrfbwXi2WCzyPxlIBUM6WJQ3mA6HWW2m5j+f5L8uf7xfaJuY4zg2zGanoxSPaxTmTx9Oy+fPFvtZzyLlyeJyiDw3Knoty6hmSVTo6c3y944Q7obtLx7dqEQ6Ga82p1tSLpyoMBUq5P/MF7fnHI67/fQXz+T7sxiFw8Fsv9p9fm/u9/n9/n35Ws7Gw/AXz4b7D2r8H7zdFs15RGECAAAAAElFTkSuQmCC`
  traceIMage = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAASQAAACaCAIAAACPLKIKAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAC0LSURBVHhe7Z0FeFRH24YpLnF3dyUhTiDuhLgnJMQTQlxISEiIu7u7J0BwKVAotJQWCoUW2kLdPoqVAsWa/zm7S5oPKVK6tN8/9/VchyNzds+ZmWfed052lxmTBAKBKRCzEQhMgpiNQGASxGwEApMgZiMQmAQxG4HAJIjZCAQmQcxGIDAJYjYCgUkQsxEITIKYjUBgEsRsBAKTIGYjEJgEMRuBwCSI2QgEJkHMRiAwCWI2AoFJELMRCEyCmI1AYBLEbAQCkyBmIxCYBDEbgcAkiNkIBCZBzEYgMAliNgKBSRCzEQhMgpiNQGASxGwEApMgZiMQmAQxG4HAJIjZCAQmQcxGIDAJYjYCgUkQsxEITIKYjUBgEsRsBAKTIGYjEJgEMRuBwCSI2QgEJkHMRiAwCWI2AoFJELMRCEyCmI1AYBLEbAQCkyBmIxCYBDEbgcAkiNkIBCZBzEYgMAliNgKBSRCzEQhMgpiNQGASxGwEApMgZiMQmAQxG4HAJIjZCAQmQcxGIDAJYjYCgUkQsxEITIKYjUBgEsRsBAKTIGYjEJgEMdu/ngcPHvz+++9YMrYJ/1SI2f6tHDx4qLSkfGTT1pGJvWPb9/WPb9978OjJjz55/8Ozn3/x9fUbv9648SujKOGfATHbv5KzZz8OC4uKi0vy8w8qr22tbeltH9jcPjDR1r+ltX9z1/C25u6R7qGJTVt29PT0X758hXEa4bVCzPYv4/33P/D3D7SxWRESEpGZmZOcnBoQEOjl5bs2NtnLL9Ddxx9ydPUKi04qLK93dHSxtrKtqKhinEx4rRCz/ZtISkpZutTIwsLa1tZeT8/AxMRs+XJjBwdnDw9vc3NLHW1dcXExNlYWYWHhoIjYwoqG6Ph1IeHRuYXljPMJrxVitn8Bv925+8lnX1TVNC5W13B19XB2dtPU1FqwYCELC+uiRSy8vHymphZOTq76+kvNzCxEREQlpaQjYtcVVDT0je8cmtjX0T/+2YWLjNcivD6I2f7RXLxwsaqytrWzv6V33Md3tZ2tvbf3KmVlVQ4OThcX96io2ODgME1N7dmz58Bmnp6+Dg5OEVGxhZVNOSU10ckZyRvysb4+uzhuXeaJ059cuXad8bqE1wEx2z+Um7dufXD6k8Tk9daWNsZLl1fUtgYEhoSEhPv4rJKSkkEci4yMzsrKzc8vtrNz4OcXRKwLC1tjv8IheV3GyJa9VY0daxPTE9NzoxLWY/4WEZtSWNVSUtu6eduuK1evMd6DwFyI2f6JnD57fmRib/vg1tyS2jVr4sLyiofOfRoUscbdzdPLyzc0NMLfP0hNTcPKyhYBTUFBycaGinheXn7mphbh8UmDh94trmqKik9LySpMyy7JLKhcn1PiH7rWYLmpiISUqrrGli1bGe9EYCLEbP8sfvvtzuF3TxRUtUQnbwiNio+NS0mtazo0OfnW5O8rnF2X6i+1tLSBqRDfkEYKCYmIiIiZmJjDgatXBxsbm5kbmdgFBx+YnGw/djw2PjWDlkA6uHopKKtycnOzsrNzcnHPmTdfQVGlobX762++Y7wrgSkQs/2DuHrtemPXUPvgxMaiKjFxKSU5+biQ8NyWto8nJ6vf3L9EU1tdXUNFRd3JycXV1d3U1NzY2FRPz8DOzt7e3tHc3MrZ2W3pEl0VB4fyW7+WvHUoLig0OCJKWFqGlY1NQEhYSESMl1+ALrjOxSsgNavgw9NnGO9N+PshZvun8OvNm6Nb92zILSmvbW3uGEwsLFvb0mYen5YwOI5I5V1VZaq/zNPLR0ND08LCCtZKSUkrLa0qKiqLjo7z9vYzM7PEfmtTS0kd3aj3jmYde98qLnXD9h2eUdGuHn6+QRHmNvbCouIsrKxsHJyLWFnNre2rm3sS1ufs23+QcQWEvxlitn8El69cbekZCQxbGx4asSYqtrl9YPDt945MTsJmTbdupv/8k0dXBxcbR2JCiq+vv4KCori4JKZq+vpLjYxMlZRUtLR0jIxMAgND5KVkRXR1Gn69fmJy8t3JydiRYQcP38T1uSFRCauCI61WOOouNdLSXRoendg5uKV/fGdlY1dsSua5858yroPwd0LM9l/8/vvkb3fvP67bd+7fvf93fdL3zp27g+Pbyxu7UtZv9PZZ5e3jX9fc03r+Uxhm/+Rk4NFDlgW5RskJXAJCPOxcwcHh2dn5a9bE2NissLa2s7a2RVYZHR0fGRmtrqb5xowZKg4r077/avzOb8cmJ6NralcFhIbHJAdFxlHPJOPWhUcnB4SuDYqMTVyf3dY33ju2o6alLzk954OTHzKu5u/kxq+3Tp759PTHnz+iEx+d/+7HS4xC/7v875vt7r37X3z9LWPjWfz0n0t+geE+AaF+QeF/KDDc2XNVXVMbo9Cr4/fffz915pPGzsGq5p6m7uGekW19YzsQc1o6h+rOnftgcvLo5KT7+LCCk4Oik4O6uxuHgOC8mbOVlFRdXD1CQiIiItaGhET6+Pjr6hpwsHPOnvEGv5SU3prwwP17Rn67fXxyMmtkbLGaJi+/oISUjJyiymLM6NQ1ePj4BASFBYVFl5lY5BWWlVbUlte3x6dt3L5735UrVxlX9hzcu3fv/oPfGRvPx/lPLwRGxkXEpkT+t/xC1g6PTzAKPYtN2/Zs2baTsfGv4nWa7fbt2++88+7NmzcZ238PE9t2ldd33Ll7l7H9p5w+/ZGbT4C3f7BPQMiUfFeH2jt7Rq2NZhR6DvYeOJxbXLV554HLV578R62fLl3+4NTZzsFNZQ1dbf2b2vs3VTd3w2ZYgdq6R3M3b30bke2rrzs+PR+yecx7oM+6tFjV1ZlfVnb+vPmzZrwxb/YclkUs8+fOh8dmvzGTjYNDfImmmoebVWVZ+80bx+4/2PblVxuGRmxWOkclpCZl5GYWVlQ0dkUlpLFzcHBwcnHx8CxkYbG1sw8MCKqqb61tG0hIz92QX7Z5++6LX36NYMu40Kdw/Zcbg5t2FVU0FJRW37t/n7H3Wfzw409ZhRX5ZbWPaH128Z79bzEKPYu61t72ngHGxt/DhQsXkpJSfv75Z8b2K+J1mu3zzz9nZeXcvXsPY/vvobahOTGz8NJTPvn+682bP1+5duzE6Xff/xCqbWpv7RtDeOkd2z4lerQprqw79sEplKTrvWl6/8MzH545P7V55L2TEbHrvANCm7pHC0orj5/86INTH588c+7jz748cvzU4JbdwzvebBvY0tg92j26Y/PuwxN7j9S19TV1DXcMbIbT8F71Td0h2Xk1R47EB0cUV9fBdQhTaR+d1I5dq+7prmRvJ66lJaSkyM7FzS8lLaapIamnp+bmstjbQysyLPb4O0ggc4bHIlcFeQSG2q4OqaxvL6lpLapqLqmqT6sZtM4YlRQXFxAU4uTmERETT0zJKCyvr2nuqWnpLa5pzSqq3lhc09Q12NE/3tw1hPx26r5o+gg3C+05cLR9YAuCkqu3P+6XvpOqh7PnIWr91FkIA8qUsPPQOx/gHjsGN3dO19CWxs6htp5BWrGPj394Bg3x82VE2SeMUw8ePGjrGxvfvveRMfouFWef1/PP5NChwywsnF999TVj+xXxOs12+fJlQUHRyMgoxvZf4/79++jQaCq0x/37EMWt27drm7vyK5uOHj9JbT94cOPXX899duHsuc+37j6w+8CRruGJzqEJZHFQdUtfQ+dQa+8oekPHwKbpQp9o799c3dKLMnRhqkMXYkJxdUtiei6O1rb1N3QO17cPdg9vxXQIzkE3ah+c6BnbWdnUbW3vZLvSaYWTW5iHj7u7t6unj5uXr72jS2xSWlPXyJTZkEwiYhiYW3oUFTsGBIXmFx+enMT87Z3fHyS8e0QvJVHNy2Oxr7eGr7eau+tiHy9Nfz8sVT3cdGLWhr21b+uD+ycnJ4f/czkoKcUyIXGZX4CLqzfCtYdfoKf3Kufw9abr+hVkZQWEhDm4uBcsXOjuu7q4qrmqsaua5jcIK8gFSuvay+raS2rbCqta6CqobMZO3CPtNodwqb2j2/vHd+F+MazUdwxB6bml6TmlrX2b6toHUDPThXvMKqjMKihDaKpq6pqmzurmLhytaxvAWajG3LJ6VGl+eUPP0Obe4S1Hjn1w5pPzN2/dQst++92PqPzsktpde/djE2AYPfPJp/2j24Y37zx77tNLl36mNzQWyNIZneMFefvtI6qqGt999z1j+xXx2sx2/fr1tLR0LS295OR1jF0vyJWr1/ccePvA2+9Cm7bvrW7poXeIuvZ+BIra1r66Vriip6yhE3kaMih0I8yOcDS3vDG7tC63vKGougUdCw3c3D2Cjo5eXl7X5h8csTZh3Zq4lGlKjohJrG7qgoWogfm/1Te+MyUjV8/QmHJL/6bCiga8Cz0hbOsbxynD297ctOtQfVufnLy8hKSkuITEYhkZKQkJrIiKiopJiLv5+KFYa+8YTqGbrbCyUVFOSdHYpO7iBTgHke3g5CTiFWZxzT985zc8sDw9TTM8RN3fD9IMCVq6Lsm9t7Pg4me7abY8NDmZd/it4J4uDS8PTS1d25UuMvKKYpJSsnLywrKqM4XU582dy8XDKyQqJiOvYGhsVt7QgRqoaupGbfyJUKC+fYB+kVNCDWCZW1KNCu8Z2R6/LjM5IzcXXumhqnR6SdwjLG1mZbfUyHS5icWUcAHLTMwxb8SN00uiKuo70FIDpfWdRbXtGQVVcWk5pbWtjR2DaMTmnhEcrWzubugYgEpr29LzK7NL6jaW1G4orM4sKK9q7Kxv7y+tb7/41TeMvvKCHD/+vp2dg6mpxZ49+xi7XgWvzWyenj5z5y5audI5IyPz2tWrVy9fZhx4yC1Epd9+e0TnPr1w8vTHp86cg7buOpCWW7GxuDaruDanrKG4pg3jLlSElAmqplRc24aGaeoawnhZUN1S0dTd3DuOVkzKyMssqMwpqc0rq88vr4c5p3oG2qkWMar1v9XS29Iz+kjveUToTCU1LY5u3pWNnV1DE9hDN9vozoNb9x3FcC4uLiEjI6ulo7tEV19bV1+HtlTX0FwVGNo1vLWlh2G27pFtqRuLlNU0uDi5kRk2f3kRfjtCfYiEct3R3+4g0PVdu1p4/pMNHxxP/+B4/rmP23+5tu3e3Xdobtz+642Q3m7b1cHSKqqYyOkvNfILirBxcJaSlWdlY+fmFzSwWBkUEYMBoqiyEbeGm+oY3FJR31FJC27UINWG0eoJwiEMKLi1rqGtU9NL6vSBzV6rgjNyS2C23rEdA5t2+wdHNnUP495phf8QImHn4ASq5XHRCuBlJ/rGkVofGt2+v6FzsKKho6a1NyUzPzI+Nauoqryhs6KxG8MWDtW09pWgues7ynHZVMZBCTvR4jll9TiUX9lc29R56swnpz8+f/rjT0+ePnvi1Bl0mw/PfDKlE6fP/nz56sPedefWrVu/XL/+yy+/jIyMon8qKqpKS8t+/fVLOvZxXo/ZcnML2Ng49fQMnZ3dvfxWFZSXZ+Tk9I1s2vnm4Z1vHtr15uGBsa0YHKkanBpZW3pQ75hRZJfWw1pQQUUTWrSldxRLJDb0zv1QY9gPYQKGOJNTXIUhGW7p37Rrx4F3W3qG/UMioxLSnD39nNx9kEeV1Ld3jG1vG55oH9naPbYdQ+z0LkJpeGvbyETr8ATKdIxQ8Y3e1aZ629Qe9LCpjoiuU9vSE5uSkbh+Y2RskpGphYm5dXBkTGQsFSrpwmZmXik6MT2NhPB2wWviFJTV+PkFRUTE1JwcMw/uh9nOTE62HH0nNTmtqm/wPdrmKZqwcuDylcLC0pbB4c3/+cl+Q0bk+qzlhsZsrOzcPLyQlIycqZWd56rgJXoGvHz8IZExg1v2Iv1DbSAs44IRMbKLqmA21NK6zPyA0KjgyNjHBYuGRsVHxaWERyek5xSjTui3CWFai8tu7x/HOhzl5rUqKHxtVDyVIKxNSI1clx6SlBqRsC4iOW1tWmZccnp0Yhr2TwmboSlp4YnrwpNSozdkp2UXtPeP1bb2pG3Mz8wvRa7h4rUqu7gar48kP6e4urCiHisInpR6R6faHbVX1dwNi6IJIDQx+klRTVtJXcf63LKkzEI4EHnNlHA0s6ACXYsScpyO3uaB/o6x0XW5udbWK+ztHbm5+VRU1L/55nmfZv85r8dsiYkprKxcRkam2kv0PFcH7T9/buepD/v2vFlW34mBqqqZmqljwoD+hzGMLgSo5t7RisZOuovQqAggZbWtNS1Ib7qQdyEDLJtSbeuUMvPL18QjVFRif0MHPDmOFlqXVYidaIy4dZmJmXllpQ1Ddf2jDYP9tb11la1osMZOvDUmIYPof1g2tPbj0Fjj4GB9f0tlO9XDaO5C67Z0j0xlgPQ9U+v05u8e3d49Qk3hxnYcHN1xYGDzbvTy6cJOhIXKhk70D5SHRb0DQuUUlPkFhJD6wXL8yso2aam1H51K273XyNwmPDV96Pvva89/0nDx8+rzn/R8/23bmbO2Kxw8UtIjhwZtVjhqamotZGHh5uUTFBYREBJh5+Dk4OKSkJbRX26irb8sKDIWbwGPoQI3ZOWVlFXDP7Wt/fRBraKhE4lZ2ZNUWtOKUzALhTmRdk7d5iPCXcCKiJlNnUMohnSuOi5n+4aGtsqOzqyaksj0lJziwqqmourmwqpmZPJFNS25pXVb02r6yzq68xpaQ9KLCqq6aCMaHIVXSM0q9A+JiknOKKxsyCutiUpIjUlO31hUlV9el1dWm1cK1dCVX1a3Prs4PbekpKa5uKoJAwR1GV1DGGdRDKeg9fGCzd3DdKGDpW0sotoX84vWvu4tO/adO3f4yy9rB4cV5JUNDY1ERMTnz1/4wQfIGF4Bryuy5SOyKSioiAiLWTk4bn7vvZG33+7ctgP1VVBej/Qmp6QqbWMhHJJVUEEXPJNbUpOaVYD967OL0nNKEtI2RsSmwDBR8alwTgTCxTRFxqXQhb7lExgevCY+Mm4dGixxfTaWQRGxsSkbMJvPr2jIqazf6pJ42DZ+o3NI3jL3HEM337Co1OzinNI65C01rf3Frb0tObVv2ScWukUmWXnnL7bPC07sHKeef/SMbs/ML4ONaeM61dWwp2Pg4RN8WnaEOdvw1n1P0wiOTuxdv7GoGBZHVKQC6dbA8BgZOUV+QWFpWQUePn4udk5OXj4RHW394EC1wCDX4iLnjHS7mBin5BSHxCSLyEiXwgKLlBQ5Xz/ZpQZcnFxwF6ZkONfI3HqZiYWcojIvvwAnFzcnNw/shz0YHRDTympaLC2sbKxsbOxWFpQ3IFGE2ejPSJ4oHMXQAw/QRb9HuvrGdiKHxP3SN5Ea0AeOrk07q/Oqt+j49a9cu8bSPUzHOsxghU9YVEpmAWoeo2pT92hNx2B+SX2rS3SytXeogV2ipGGqR1hD3xgG0IzcUoyGGHocXL0RmRHw0XDY9Fkdtjo8mgq2NKG66MJR5MwQSgZHxlnbO6+JWxedlI7kYnX42sDwaLQ+XnBK6AOrgiNTNuRCiWkbszDijY62Tkwk5eYKCYpISEhzc/OysrJ/+OGr+Yv/6zGbj48fCwu7sLAYH6+AmoZmWEJCcEyMf3iks4efi+cqyN03EJkD0rw/5OHn6uXv7hPo5OHj5OELuVBJIJUHYt3V29/R3We6qP00UZtu3o5uWPqgmJd/sJvPajefgLWJaRgXm/vH+poHvNWNJOUVzF3dNQyXiQiImFutgDNzy+qaesY6h7fBda4u3uLiUvrWtmZOLujK7tbOo9sPoEsNTuzNKqzAVA2moo3EYxsLK6ke+ccDkm2bdh/atOstaBzaeXBsxwFMSOAxuuA3bCLziUlKR5dt6R5GN01Ynw2riElKi0vJIEDBKjw8vPy8/Fiq6uiy8/FJSMuqqqgvW26ip28oLSUjJikloajEJyIMp1Hm5OEVl5Q2MrMytbTDEu5S09ASEBLGIbzagkWLwtYmIJPsHNickp7t5RsQsiYW10w9WW2lJmZ0a9Enb9jfgIGfJqwjLGA0QaKIcYHuKwj3HoeRK6cYN0vfg6pAGahv8+6BrnFLXWN2MRHfNTGGtis4+PmWGpkFRsRk5JVhilXZ1J2Qlm1obL6Il8c9NNI1KGzOooU+rj4VTV3rMgvCohM9/AKt7ByNzKwtbFeucPZY4eRu7+xpT1uZkt3DFeyHwaxWOK1wQgEPHf1l2LR1cEUBE1SFubWto5vNSpcpWa90wcVY2ztBOMvW0WWlu7uTp6eJlRUfnyAvLz87O+eCBYtOnsSs+RXweszm4OCEe0CCxMvDLyQkrKisLKeoqIjsWFNHXVMbPUNvqZGuwXINbT1NbX26sK6lZ7jMxFJb3xDS0jVANaEnYYnOZGJhi+V0GRozZGxuY2GzElWKksYWNmgzc2t7C1sHn4DQuHUb8irqm0qbNAQkecXFCyubopM3cPLwIgigN2vq6OMUfUMjcUmpRWxsmACtTViPYMjCzq5nZFrd1F1U2VRa24p+Vl7Xjg4HkyAtsXN0zS6qRI+k9zlEq+LqJgzSBeV1CNpIhAoqqUcySHIaOgY6h7Ygq4QP61p7MXZk5hZnZeVuzCnILanWXWokJSuPJBAOoczGxy8sKs7HL4hr4+HlwwKZIRs7BxsHJ+IVN03Yj5I4LCEls9TIfLmZlcFyE/qHITW0dKVk5Hn5qI/8I0wibLb0jiGKbtp1CINOUEQ0NquauvPK6pCDpWzIQwSgC+lDfGoWlJC6EZvI6JBiIOktqmpCLkq/RygkMi4+NRPuwh7UA5wJ7yH7QGjaWFItq6IK88dQYWQNrpydk4tPQAjxFm0tLSePC2ZhY8OtIVfEGIcOoWVgiLiEMRSNhSZYomOgungJOoCeoZGeobGJhY2xubX+MurW6MJOurBTW9dwiY4++o/e0uXyiipKahoqizVVF2thXVpOQUlFXVFZ7Q+pqKGuUBuQrLySpLSskLCwsAgQhdlQs6ysbPPnLzhx4gSj4/41Xo/Z3N09Z82aixjNw8PHzs4xZ87smTNnzp07l42Di42Dg4WNHZ1MREwCDYB5C118AoLYif4nKCwKUSmWnIKCsqq0rDyqCe2G2nyi1DQwTzFBa6GYktpidDtNbT1VDS00gJrGEkV1DQM1HTUxmUV8vCpaOnJq6txCQux8vKzc3As52OezsUJY5+TnF5KUlFRQRAFOAQERccnFS3QxKCipLsaggAiAoEQf1JGeobdB1Lg+tnNjYYWElKyMvKK8Eq5ES8dgGQYCDKKI2yFr4NM8zC4wkYBXkfCYW9o4rHRycHCubGgPXZsoLaeIGAV70IUMEJUwtflEoeOiWhAo0Ed1DJYv0TXAPSqraUBiElIoQA9urGxsCalZm3cdKq9vX2pkmpFTVN/ej46OVA0JGy4PHVp36XIV9SWyCkoYd2AVcUkZrEhKy0nKyKEhVji6YbaJy8b9Unf9MI3EXQ9vfRODBcKypIwsHC4hI8clKMglIMAjLMwnKsrFzw9x8vKycXGxcHBgycnHx8HDKyYrJyghwS8mxovbFBQSFhFDK2MpKiGFtxYSEccKWh93R40d2nqoUikZOeyhltIPJSOHSxUVlxCXkoaLBB8OVehI1F2zczwijFaz586bMXMW9Mas2TMeMmvWLF5eAYQ1FhbWmTNn/7vNlpCQNHfufOqrjFw8GDlERITRk+Xk5JTV1JRVVZVVVCWlZWbOnjNrzlzUxZSwOWfefCwhHMXZU7WJhkFHnC5+QSEIK6LikoiYWPLyU3ZF+8EqwmLiOBcnYqDl4uNXEpVeLCChwiemyiOqziumwS/xJImr8our8ImqC0gI8QlywIHcPCxsrHBRXVsfgtvU3wbgtJrmnsy8ssHN1FdmFrGwctDmS3gvBB/qksQkFJTVEHaQ4WDOgAQSERKBzsrOISQypqG9H4ER0QMlcY9U4KKFLPrtYAWiewavhiUMRq3z8cGZMKS23lJYWn2JDgyGsQDjPVxtZGaJwpRQXQKCc+fPX6yl29o37um72nNVYHPX8KqgCAR/ZBQ4BT0Y9cPDR6sciOehuHlwMdRds7IqqKjBWhX17Qho9FEG907NYPPK8kpri6sakWwjCLNzc4vxCuoISauievnF1PnEFTgFxdl5Jf5b4mw8qrxUxaJ6UZIfb83Ly7haWAV3TYvJdMkqKGOEndqEUOxxYf9CFlZ6z0FvgbWUVdG7pktVXlEpKCQkNS2NpvUpKeviaRgYLF20CCMSO4wXHh7xqj5R+HrMhqtXUVHFndB+qcb8P//5D/bcvHmLtqT46quvhoaHx8bHH9XYH+sxcXEzZryBBA/V8ieCxzCoozui6hEzaX2UaglKtCbEoGsmougtrO4hpOogpaEvq6YpKrNETE5rmpaIy1soabkKq3gIq3kKq8sIUCM0zkUup6SqXtXU5e6zGm4Z33FwaGLf5t2HkTE6unqNbT+wIa8UhqG3PUTvB9y8lD2QFiqrLUaii6kIpvvI4uLXZSLjxSRweNt+SzsHuBRORpgSFBHl5OZGeH/4IsjCROhzMNpoIiEkKoZ1FFNUUVfT1EY2juQKTka0rGzsGt1x0MLGnoUVeQO3g4tHbFyyn3+Qt3+I7UoXI1PL4upmTOGQbiEUCImI0t+F5t6HtfSYcEOIb8iZswoqkP1S085t+7tHtmEiFxoVh2wzKT0bNYNX4ODjUxaVDpDUdhNSRfUaiSlZL7OwsrSzMre1svhDtjYOFksMnQSVPIXV3ITVxASEuWnVC+FFINp38KjWhGeEaEEPtTrVxFNC+y5YxPLG7DkQeldMTBy9q6Av7T9wgNG3pvHrr0/+0eh33z02ixboli83un79lf1K0usxG9izZ4+qKuW34OAQxq4X5MKFC7p6+lo6urr6Bk8TjqLBMORLSEljU32xxrwFC+fOXzCLNuCh5TBOc/DyGgnLuwmruggoGcio+kTFJucVJWbnJ0xTalG5i2+AGY+0K4oJq0o/NBvVFXj5TCysA8PXevsHT8l3dWhAyBpXTz9ZeUV0XHq/eUT0/ei12nqGlnaOETHJ9R0DTu7e8ACCQ3BkrKmlbUZeaXlDB7I7TDDgK7oH+PgF4CXMagLDo1eHrfULDHfzCXD28PX0C0SchHNSMwtKaloQuDoHNtc2tPUMbTGzspu/YIGQqERUbFJE+JrAoLDAsCgkiolpGzFvhaUx7mAMwotPv8KniboGAUGMFN4BwavDonC/q4LCl5mYyyooqmlqLV6igwBLvzuqpIiolbiqq5CKs4DSEknF8JT0ksb2/Oqmh2rMq26oaunxdPO15JF2EVZxFFYR5OFHhkkPSmivOfMXKKmo0BtUFWk8Fw/GTXqDPqIl2jpW1jZFxSVQZtbGL774ktFXXpCjR4/OmTMnNTUVAYCx61Xw2swGvvzyS5jN3d2Dsf1SPKD9txJP4/KVK3IKigiAsXHx2Pzll1/27tvX1Ny8KiAgJDRMQ3OJsKgYGw/PUn5pmM1ZWMVeUNFGWsNZxcBJWd+Rkh5dK5X0bMRUnIRUnIRV3ETU+BayIygjOZk5m0pocRd/wkxa0kvXvIWLMPRCHEj+aH0RnRIpLmaSFrYOMckZJdVNkbHJmGtFxCS29W0anthXU99aVFqVnlOMyR7CBXIzzD8xrWrpHWvrGWnrHcXEqa1/vI2WyCG29Ixsw8SJWo7t2JCVH7Q6GNYyMrOaOWuWo5s3MlufVUHSMnI6+oa+gaHrs4sRCekenvISBiDaRbIiaZ+68sfFuL0nMnMW/a7fmD177qJFlqLKGKRQdU5CymaC8sZC0yVnJCRnISTvKKiEAmgCmE1KVEJBRSUYLRQRMbF16+7dey5dukRv0IsXL4pJYICS37V7N33PdNAZGN3ir/HWW2/NnTvvypVX/LPtr9NsP/30E24pLCyMsf33kLo+A2bDWMXYnsaNGze+//6HgdERfV4JN0EVhCz0CVdBZRd+JYYE/hDGZnoBSz65rIwNjc3NTc0tj6umtg6zsKCI6Kqa2kcOQW4ensampuaWViJi4ujK8xcuond0eB5TLCNz67WJ65GUBkdEI+x4+AbGJ6f7+wX4+a4qq2nKKam2tnfS0l0KK5pZ22fmFicmJMfHJ7d0j9A/P0V/MIiVLspy20e27W/pHtbWNdDWM1BUUVvp4tE3tgMxEwHTYJmxh+/qqPhUmBzvjrwUkQoruB5cFS4GV7jcyDglNfWR639EuEeE9JDI2Lr6hkcOTSktY4MJvwyVONDkJqTyuGh1S1WvI+pZVO30e+9fvfrUb9aZmJnFJSQwNv4eDhw4gBHj888/Z2y/Il6n2UBHR8eZM3/vb87AZqi44++/z9h+jO9++nGZqPwKfoUVwkrP1EohJS1O0bPnPmGc/CTQp7uHtzE2nsJnn33e29dnaW2DmQYyJXR0dHdJaTkdg+X+IWtKa1uQniEcIYhpaeuuDg5v6Rkdmtjb3r8pu7gaZRYv0cV0y8HBOS4xFcEKh7Ac3LJnaMtemK2+faC0piUmKR0TKjWNJZjeKCirFVc3efgFIsdzcPVC8ol3UVRV5+ahHrHgremTInisu6f3/Kcv8CsJtIeQm7774anfs/74/DkdTlHU2yM1+UTZCMivFFW5/P0PjJOfxGJNTf/Vqxkbfw+Yp7377ru3b99mbL8iXrPZmMBXX30VFh7xJ18EvHjxC0FBIV70d34BAUqCTxMKYMLExs5x7L33GCc/xvVfblQ1dpbXtV+5+lwT60OHDjk4Oc2YORvvj34vICSioKRqvcIpJSN3Y2GFvbP7clMLRDn/4Mi8khrYeGzHgfTcElsHVyERMU1t3RWOrgh3NpScrewcrKlfGVkuJSOHWRMrOzvtQSgX9WkSOQWDZSbG5lbwW1RCqp2jm4iYBNJFusmRNOKth0dHGdf03Fy5eq2yoSOvtO7w0aeOZe+9d5ydnRMTOFr1PlqljwitICQojBZhnPwkdu/efezYMcbGv4r/fbM9ky+//EpYTJz6WzYPLw/sJCSMJScP35SwyQc3Cgii13Lz8bNycv252aqbuirqO65dv8HY9Sww0wiPiET+NjWLExQWVVbTdPVaFZ2UHhadABctMzGHi5YZm8N1MOGq4EgUg+1hp0UsLLQlJern/9G1qef1vOjeeDXM8RaxsuoaLHP28IXTsERKiQkjrQD1VJ2FjV1KRvadd95hXM2LcO36LxhZCsobjh576l+iTpw4yYVUWUCQX1gEdctN1S1V1dOFGuYVFOKjHUUZTMwYJ/9vQcw2eeHCxbnzF/AJivisDlNQUUfOycbBJS4pNSVObt43Zs3h4Rd09lxlsNwMBd4+8oQZIJ0ps/106fLde/cYe58FJvfGpmYzZs6iP6KAkWAGfkEhTORWuniFxyRhauS7OszMynYx9QmbJTLyCqxs7FwPn7LQBXdhE07j5OZGQEP2KCAsoqy62NDIDJHQys5RbbEW9dczHiqg0Z2G1JGDi/vlMvl79+7959LPzzTb+ydOvEH9UZRHd6mRoIg4GyeXmITklETFIQnMFectXKSpYyAiIc3KzvHSTxH/4RCzTV67di2/sMjCysbR2dXd06u8omLrtm3fT2PfvjfTMzJgBhc3D8xqEpOSf/jhqZMKmI36JnJjZ0Pn4MjEnuf/rjAS3ZUOjvAb3QkMy9H+oi0mKa1jsGyFk7tfULhvYFhQRDTyRsy45BSVURiemRLOUlBWRewyWG5iamlLfW7Q0c3Q2FxGTgG+xKtNvTgKI3uEpUfHxhlX8CJgdBjdure5e6Shvb+8vuPY+6cYBx7jzp07A4ODcQkJZhaWqLr9Bw4yqnUaI6OjpWVlpmbmsfHxWyYm7t17ZT9w8I+CmG0aL/st+ulcv34D05iS6uaMvLLqpk50SsaB5wCxAtNL+A3RiW4JCPaAJbAH+zBPg5e09QxNLDBHc7dZ6WJobGZuY29usxLWMqI+DIl/Hcys7BBGVBcvEZeUxjSQOpeXmpNOvSY0Z94CCSnpI0eOMN77BcF9ldU0J67PxdwyKj5t117yS6/PhpjtFfPDjz8am1npGxpr6Rl6+Qa8kNnoZG3MRsxBZvWIPbBJSy+pD0zRPj9FPU5BGkYlYxJSWBERkxCmfZSE9hkrzMqoqPjIi0BsHJyz5szV0tZ5oaeOj4B5prO7J+3zpdqiEtLVtfWMA4SnQ8z2ivn6668x12Jj55wx4w1lFbWXMBs4fvx9axtb+G3+IpbH3QIvcXByYYVuvynRP0z4ePkpYXo2c85cUXHxisrKGzee9/nNE4HZFJWVMX1FhMQyY0Mm4wDh6RCzvWK++eZbzPgxI2puaTl48Hl/C/GJdHZ1LdHSnjt/AcROcxevgCALK6uGxhIjIxN2Ds7pRvoTIcotZGGdOXuOoLBIaFg45kiMN/gLYBA5ePBgb1+fn7+/o5PTyMgL/9ng/yHEbK+Y+/fvf/vtt99882p+JebOnTvjmzY5ODnLyMlTH+mcMxe2ERAUkpaRRSh7xFTTBYMhjs2ZN3/2vPlYNzBctjE7+wL5z35fK8Rs/w5+/OmnHTt3xsTFaevqiYiJU588pH3VaN6ChVS2+VBwF3bSH2lKSsu4uXtU19SefEXf6if8RYjZ/mXcvn37xx9/HBwc6urq7uru9vDysrG1W7FyJWRhZZ2dk9PT09va1o5Z3+XHfh2Q8HohZiMQmAQxG4HAJIjZCAQmQcxGIDAJYjYCgUkQsxEITIKYjUBgEsRsBAKTIGYjEJgEMRuBwCSI2QgEJvEyZvvtt9/oP6n33XffkQ/gEQjPycuY7e7du2+++eb4+PjExMQr/CV0AuF/m5dPI7Ozs1/57zMTCP/DvLzZTp8+jXySsUEgEJ4FeUBCIDCJv2q2Hyd/vDH5l346hkD4f8JfNVv3ZPeHk+Rb9wTCs/mrZuuf7D89eZqxQSAQng4xG4HAJIjZCAQmQcxGIDAJYjYCgUkQsxEITGLGzZs36Z9vPHz48Ev8ZwvEbATCczLj1q1bg4ODpaWlp0+ffvDgAWP3c0PMRiA8J5TZBgYGioqKzpw5Q8xGIPx9UGnktWvXsHbo0KGXSCO7JsfIJ0gIhOfhsQckly5N7tjx/Dqwa/0Xt88xziUQCE/nMbNdvz554sTza2f22c8+euHkk0D4f8hfffTfMzr54SnGOoFA+BP+8t/Z+idPk+cjBMJzQMxGIDAJYjYCgUm8pNnu3r177949rPT1/U7MRiA8Dy9jtgcPHoyPj3d2dm7YkFRRcensWcZ+AoHwJ7xkZIPfduzY8fXXHx84MPnZZ4ydBALhT3gZs926dau0tHRiYuLChQt37txm7CUQCH/Ky5gNE7YtW7bs3LkTS/pHvQgEwjN5yTSSQCC8KMRsBAKTIGYjEJgEMRuBwCSI2QgEJkHMRiAwCWI2AoFJELMRCEyCmI1AYBLEbAQCkyBmIxCYwuTk/wEPnX9nsfM14wAAAABJRU5ErkJggg==`

}
