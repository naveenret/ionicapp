import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TraceablityDetailsPage } from './traceablity-details.page';

const routes: Routes = [
  {
    path: '',
    component: TraceablityDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TraceablityDetailsPageRoutingModule {}
