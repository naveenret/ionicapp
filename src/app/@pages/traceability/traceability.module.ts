import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { TraceabilityPageRoutingModule } from './traceability-routing.module';
import { TraceabilityPage } from './traceability.page';
import { ReactiveFormsModule }  from '@angular/forms';
import { SharedModule } from 'src/app/@shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    // QRScannerStatus,
    TraceabilityPageRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [TraceabilityPage]
})
export class TraceabilityPageModule { }
