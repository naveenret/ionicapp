import { Component, OnInit } from '@angular/core';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner/ngx';
import { Platform, NavController } from '@ionic/angular';
import { FormBuilder, FormGroup } from '@angular/forms';
import * as moment from 'moment-timezone';
import { TraceabilityService } from 'src/app/@service/traceability.service';
import { LoadingProvider } from 'src/app/@core/mock/loading';
import { async } from '@angular/core/testing';


declare var $: any;

@Component({
  selector: 'traceability',
  templateUrl: './traceability.page.html',
  styleUrls: ['./traceability.page.scss'],
})
export class TraceabilityPage implements OnInit {
  traceabilityListForm: FormGroup;
  getTraceabilityListData: any[] = [];
  constructor(
    public datePicker: DatePicker,
    public qrscanner: QRScanner,
    public platform: Platform,
    public fb: FormBuilder,
    public traceabilityService: TraceabilityService,
    public loadingProvider: LoadingProvider,
    public navCtrl: NavController,
  ) {
    this.createTraceabilityForm();
    console.log('Captial X', moment(new Date()).format('x'));
    console.log('Small x', moment(new Date()).format('x'));
  }

  createTraceabilityForm() {
    this.traceabilityListForm = this.fb.group({
      SelectedDate: moment(new Date()).format('x'),
      NXSerialNumber: ""
    })
  }

  getDate() {
    if (new Date(moment.unix(this.traceabilityListForm.controls.SelectedDate.value / 1000)).toString() == 'Invalid Date') {
      return new Date();
    } else {
      return new Date(moment.unix(this.traceabilityListForm.controls.SelectedDate.value / 1000));
    }
  }


  showDate() {
    this.datePicker.show({
      date: this.getDate(),
      mode: 'date',
      maxDate: new Date().valueOf(),
      // androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_DARK
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
      // androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_LIGHT
      // androidTheme: this.datePicker.ANDROID_THEMES.THEME_HOLO_DARK
    }).then(
      date => {
        new Promise((resolve) => {
          console.log('Got date: ', date)
          this.traceabilityListForm.patchValue({
            SelectedDate: moment(date).format('x'),
            NXSerialNumber: '',
          })
          resolve();
        }).then(async () => {
          await this.getTraceabilityList(this.traceabilityListForm.value);
        })
      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }


  qrScanner() {
    $('#qrscannerModal').modal('show');
    this.traceabilityListForm.controls.NXSerialNumber.setValue('');
  }


  async ngOnInit() {
    // window.document.querySelector('ion-app').classList.add('transparentBody');
    this.platform.backButton.subscribe(() => {
      this.qrscanner.getStatus().then(res => {
        if (res.scanning || res.showing || res.previewing) {
          this.qrscanner.destroy().then(res => {
            console.log(res);
            var ionApp = <HTMLElement>document.getElementsByTagName("ion-app")[0];
            ionApp.style.display = 'block';
          }).catch(err => {
            console.log(err);
          })
        }
      })
    });
    await this.loadingProvider.showLoaderWithBackDropWithoutTimer();
    await this.getTraceabilityList(this.traceabilityListForm.value);
    await this.loadingProvider.hideLoader();
  }


  async doRefresh(event) {
    console.log('Begin async operation');
    await this.ngOnInit();
    await event.target.complete();
  }

  qrscannerClick() {
    this.qrscanner.prepare()
      .then((status: QRScannerStatus) => {
        console.log('prepare called', status);
        console.log({ status });
        if (status.authorized) {
          // camera permission was granted
          // start scanning
          let scanSub = this.qrscanner.scan().subscribe((text: string) => {
            console.log('Scanned something', text);
            this.traceabilityListForm.patchValue({
              NXSerialNumber: text || ''
            })
            this.qrscanner.hide(); // hide camera preview
            scanSub.unsubscribe(); // stop scanning
            ionApp.style.display = 'block';
            this.qrscanner.destroy().then(res => {
              console.log(res);
            }).catch(err => {
              console.log(err);
            });
          }, err => {
            console.log({ err });
          });
          var ionApp = <HTMLElement>document.getElementsByTagName("ion-app")[0];
          ionApp.style.display = 'none';
          this.qrscanner.show().then(res => {
            console.log({ res });
          }).catch(err => {
            console.log({ err })
          })

        } else if (status.denied) {
          // camera permission was permanently denied
          // you must use QRScanner.openSettings() method to guide the user to the settings page
          // then they can grant the permission from there
        } else {
          // permission was denied, but not permanently. You can ask for permission again at a later time.
        }
      })
      .catch((e: any) => console.log('Error is', e));
  }

  // ionViewDidLeave() {
  //   window.document.querySelector('ion-app').classList.remove('transparentBody')
  // }

  qrOkClicked() {
    console.log(this.traceabilityListForm.value);
    this.getTraceabilityList(this.traceabilityListForm.value);
    // this.traceabilityListForm.controls.NXPartNumber.reset(); 
  }

  traceabilityDetails(item) {
    item.ShiftSelectedDate = this.traceabilityListForm.controls.SelectedDate.value;
    this.navCtrl.navigateForward('/layouts/traceablity-details', { animationDirection: 'forward', queryParams: { item } });
    // [routerLink]="['/layouts/traceablity-details']"

  }

  async getTraceabilityList(data) {
    console.log('tracelist response',data);
    await this.loadingProvider.showLoaderWithBackDropWithoutTimer();
    await this.traceabilityService.getTraceabilityList(data).then(async (getTraceabilityListResponse: any) => {
      console.log({ getTraceabilityListResponse });
      await new Promise((resolve) => {
        this.getTraceabilityListData = getTraceabilityListResponse!.rows || [];
        resolve();
      }).then(() => {
        console.log(this.getTraceabilityListData);
        $('#qrscannerModal').modal('hide');
      });
    }).catch((getTraceabilityListError) => {
      console.log({ getTraceabilityListError });
      this.getTraceabilityListData = [];
    }
    )
    await this.loadingProvider.hideLoader();
  }


}
