import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TraceabilityPage } from './traceability.page';

const routes: Routes = [
  {
    path: '',
    component: TraceabilityPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TraceabilityPageRoutingModule {}
