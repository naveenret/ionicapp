import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PostAssemblyDetailsPage } from './post-assembly-details.page';

const routes: Routes = [
  {
    path: '',
    component: PostAssemblyDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PostAssemblyDetailsPageRoutingModule {}
