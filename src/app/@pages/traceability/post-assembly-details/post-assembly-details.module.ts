import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PostAssemblyDetailsPageRoutingModule } from './post-assembly-details-routing.module';

import { PostAssemblyDetailsPage } from './post-assembly-details.page';

import { ChartsModule } from 'ng2-charts';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PostAssemblyDetailsPageRoutingModule,
    ChartsModule
  ],
  declarations: [PostAssemblyDetailsPage]
})
export class PostAssemblyDetailsPageModule {}
