import { Component, OnInit } from '@angular/core';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import { TraceabilityService } from 'src/app/@service/traceability.service';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment-timezone';
import { LoadingProvider } from 'src/app/@core/mock/loading';

@Component({
  selector: 'post-assembly-details',
  templateUrl: './post-assembly-details.page.html',
  styleUrls: ['./post-assembly-details.page.scss'],
})
export class PostAssemblyDetailsPage implements OnInit {


  traceabilityChartData: any[] = [];
  parameterDetailList: any;
  parameterChartList: any[] = [];
  currentActiveItem: any;
  constructor(
    public traceabilityService: TraceabilityService,
    public activatedRoute: ActivatedRoute,
    public loadingProvider : LoadingProvider
  ) { }

  ngOnInit() {
    this.getQueryParams();
  }

  getQueryParams() {
    new Promise((resolve) => {
      this.activatedRoute.queryParams.subscribe(res => {
        console.log(res);
        if (res && res.item)
          this.currentActiveItem = res.item;
      })
      resolve();
    }).then(async () => {
      await this.loadingProvider.showLoaderWithBackDropWithoutTimer();
      this.getPostAssemblyParameterDetails({ ProcessName: this.currentActiveItem.ProcessName || '' });
      await this.loadingProvider.hideLoader();
    })
  }

  getPostAssemblyParameterDetails(data) {
    this.traceabilityService.getPostAssemblyParameterDetails(data).then((getPostAssemblyParameterDetailsRes:any) => {
      console.log(getPostAssemblyParameterDetailsRes);
      new Promise((resolve) => {
        this.parameterDetailList = getPostAssemblyParameterDetailsRes!.rows || [];
        resolve();
      }).then(() => {
        this.parameterDetailList.filter((el, pos) => {
          console.log({ el });
          let data: any = {};
          data.StartDate = this.currentActiveItem.AssemblyStartTime;
          data.EndDate = this.currentActiveItem.AssemblyEndTime;
          data.ParameterName = el.ActualName;
          // let data = {"StartDate":"1583830200000","EndDate":"1583830800000","ParameterName":"Degrease_Gas"}
          this.getParametersDetailsChartData(el, data, pos);
        })
      }).catch((err:any) => {
        this.parameterDetailList = [];
      });
    })
  }

  getParametersDetailsChartData(item, data, pos) {
    this.traceabilityService.getPostAssemblyParameterDetailsChartData(data).then((res:any) => {
      console.log(res);
      
      let data: any = {};
      let ChartDataSets: ChartDataSets[] = [];
      let chartDatas: any = {};
      chartDatas.lineTension = 0;
      chartDatas.data = res!.rows.map(el => el.Line1) || [];
      chartDatas.label = item.ActualName || '';
      chartDatas.borderWidth = 1 ;
      chartDatas.pointBorderColor = 'red' ;
      // chartDatas.chartLabels = res!.rows.map(el => el.timestamp) || [];
      console.log(chartDatas); 
      ChartDataSets.push(chartDatas);
      console.log(ChartDataSets);
      data.ChartDataSets = ChartDataSets;
      // data.chartLabels  = res!.rows.map(el => el.timestamp) || [];;
      data.chartLabels  = res!.rows.map(el => moment(el.timestamp).format('DD-MM-YYYY')) || [];;
      data.label = item.ActualName || '';
      this.traceabilityChartData.push(data);
      // this.traceabilityChartData[pos].push(chartDatas);
      console.log(this.traceabilityChartData[pos]);
      console.log(this.traceabilityChartData);
    }).catch((err) => {
      this.traceabilityChartData[pos] = [];
      console.log(err);
    })
  }


  lineChartData: ChartDataSets[] = [
    {
      data: [2, 8, 3],
      label: 'GAS Flow',
      lineTension: 0
    },
  ];

  lineChartLabels: Label[] = ['23-01-2020', '23-01-2020', '23-01-2020'];

  lineChartOptions: ChartOptions = {

    // title: {
    //   fontSize: 15,
    //   padding: 5,
    //   fontFamily : 'Montserrat-Header',
    //   display: true,

    //   text: 'Degrease burner pt',
    //   fontColor: '#ffffff',  // chart title color (can be hexadecimal too)
    // },

    responsive: false,
    maintainAspectRatio: false,
    scales: {
      yAxes: [{
        ticks: {
          fontColor: '#E2E2E2',
          fontSize: 10,
          fontFamily: 'Montserrat-Header'
        },
        gridLines: {
          display: false
        }
      }],
      xAxes: [{
        ticks: {
          fontSize: 10,
          fontColor: '#A2A2A2',
          fontFamily: 'Montserrat-Header'
        },
        gridLines: {
          display: false
        }
      }]
    }
  };

  lineChartColors: Color[] = [
    {
      borderColor: '#93EE94',
      pointBorderColor: '#FFFFFF',
      borderWidth: 1,
      pointStyle: 'circle',
      pointRadius: 0,
      backgroundColor: 'transparent'
    },
  ];

  lineChartLegend = false;
  lineChartPlugins = [];
  lineChartType = 'line';
}
