import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { TraceabilityPage } from './traceability.page';

describe('TraceabilityPage', () => {
  let component: TraceabilityPage;
  let fixture: ComponentFixture<TraceabilityPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TraceabilityPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(TraceabilityPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
