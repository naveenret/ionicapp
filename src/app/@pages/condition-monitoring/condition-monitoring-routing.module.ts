import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConditionMonitoringPage } from './condition-monitoring.page';

const routes: Routes = [
  {
    path: '',
    component: ConditionMonitoringPage
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConditionMonitoringPageRoutingModule {}
