import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'condition-monitoring',
  templateUrl: './condition-monitoring.page.html',
  styleUrls: ['./condition-monitoring.page.scss'],
})
export class ConditionMonitoringPage implements OnInit {
  items: any = ['Main Conveyor', 'IOT Devices', 'Post Assembly']
  constructor(
    public router: Router,
    public menu : MenuController
  ) { }

  ngOnInit() {
  }

  selectedValue(selectedValue) {
    console.log({ selectedValue });
  }
  conditionMonitoringListClick(item, index) {
    console.log({ item }, { index })
    if (index == 2) {
      this.router.navigate(['/layouts/condition-details-list']);
    }
  }
  showMenu() {     this.menu.enable(true,'first').then((value1)=>{        console.log('show menu clcked',value1);       this.menu.open('first').then(value => {         console.log('menu', value);       }).catch(e=>{         console.log('menu e', e);       })     })   }

}
