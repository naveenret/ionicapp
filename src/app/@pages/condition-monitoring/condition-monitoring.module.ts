import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConditionMonitoringPageRoutingModule } from './condition-monitoring-routing.module';

import { ConditionMonitoringPage } from './condition-monitoring.page';
import { ComponentModule } from 'src/app/@components/component.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConditionMonitoringPageRoutingModule,
    ComponentModule
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  declarations: [ConditionMonitoringPage]
})
export class ConditionMonitoringPageModule {}
