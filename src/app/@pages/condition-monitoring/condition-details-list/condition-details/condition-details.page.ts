import { Component, OnInit, Sanitizer } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ConditionService } from 'src/app/@service/condition.service';
import { Color, Label } from 'ng2-charts';
import { ChartOptions, ChartDataSets } from 'chart.js';
import { FormGroup, FormBuilder } from '@angular/forms';
import * as moment from 'moment';
import { DatePicker } from '@ionic-native/date-picker/ngx';
import { DomSanitizer } from '@angular/platform-browser';
import { LoadingProvider } from 'src/app/@core/mock/loading';
import { ToastController } from '@ionic/angular';

declare var $: any;
@Component({
  selector: 'condition-details',
  templateUrl: './condition-details.page.html',
  styleUrls: ['./condition-details.page.scss'],
})
export class ConditionDetailsPage implements OnInit {

  items: any[] = [];
  parameterList: any[] = [];
  parameterListTrend: any[] = [];
  currentActiveItem: any;
  parameterChartForm: FormGroup;
  getProcessImg: any;


  customAlertOptions: any = {
    header: 'Select One',
    translucent: true,
  };


  // LIne char data declare


  lineChartData: ChartDataSets[] = [
    {
      data: [], label: 'Target',
      lineTension: 0,
      type: 'line',
      pointRadius: 0
    }
  ];


  lineChartLabels: Label[] = [];

  lineChartOptions: ChartOptions = {
    legend: {
      position: 'bottom',
      labels: {
        usePointStyle: true
      },
    },
    responsive: false,
    maintainAspectRatio: false,
    scales: {
      yAxes: [{
        ticks: {
          beginAtZero: false,
          fontColor: '#E2E2E2',
          fontSize: 10,
          fontFamily: 'Montserrat-Header'
        },
        gridLines: {
          display: false
        }
      }],
      xAxes: [{
        ticks: {
          beginAtZero: false,
          fontSize: 10,
          fontColor: '#A2A2A2',
          fontFamily: 'Montserrat-Header'
        },
        gridLines: {
          display: false
        }
      }]
    }
  };

  lineChartColors: Color[] = [
    {
      borderColor: '#93EE94',
      pointBorderColor: '#FFFFFF',
      borderWidth: 2,
      pointStyle: 'circle',
      backgroundColor: 'transparent'
    },
  ];

  lineChartLegend = true;
  lineChartPlugins = [];
  lineChartType = 'line';


  // Line data declaration end


  constructor(
    public activatedRoute: ActivatedRoute,
    public conditionService: ConditionService,
    public fb: FormBuilder,
    public datePicker: DatePicker,
    public sanitizer: DomSanitizer,
    public loadingProvider: LoadingProvider,
    public toastController: ToastController,
  ) {

    this.parameterChartForm = this.fb.group({
      ParameterName: '',
      SelectedDate: new Date(),
      ParameterThingName: ''
    })
  }

  ngOnInit() {
    this.getQueryParams();
  }

  getQueryParams() {
    new Promise((resolve) => {
      this.activatedRoute.queryParams.subscribe(getQueryParamsRes => {
        console.log({ getQueryParamsRes });
        if (getQueryParamsRes) {
          this.currentActiveItem = getQueryParamsRes || {};
          console.log('Query params active item',this.currentActiveItem);
          this.getProcessImage(this.currentActiveItem);
        }
      })
      resolve();
    }).then(() => {
      this.getPostAssemblyMechineList();
      this.getPostAssemblyTrendMechineParameter(this.currentActiveItem.ProcessName);
    }).then(() => {
      this.getPostAssemblyMechineParameter(this.currentActiveItem.ProcessName)
    });
  }


  getPostAssemblyMechineList() {
    console.log('GET_ASSESMBLY_MECHINE_LIST called');
    this.conditionService.getPostAssemblyMechineList().then((res: any) => {
      console.log('GET_ASSESMBLY_MECHINE_LIST_RESPONSE',res.rows);
      this.items = res.rows || [];
    }).catch((error) => {
      this.items = [];
    })
  }



  selectedValue(data) {
    console.log({ data });
    this.getProcessImage(data);
    this.getPostAssemblyMechineParameter(data.ProcessName)
  }

  openTrend() {
    new Promise((resolve) => {
      console.log('open treand called', this.parameterChartForm.value);
      // NOTE: dont need to load defaulty :: sugumar   
      // this.getPostAssemblyTrend(this.parameterChartForm.value); 
      resolve();
    }).then(() => {
      $('#conditionModalGraph').modal('show');
    })
  }

  getPostAssemblyMechineParameter(data) {
    this.conditionService.getPostAssemblyMechineParameter({ ProcessName: data }).then((res: any) => {
      this.parameterList = res.rows || [];
    }).catch((error) => {
      this.parameterList = [];
    })
  }

  getPostAssemblyTrendMechineParameter(data) {
    this.conditionService.getPostAssemblyTrendMechineParameter({ ProcessName: data }).then((res: any) => {
      this.parameterListTrend = res.rows || [];
      console.log(this.parameterListTrend);
      if (this.parameterListTrend.length > 0) {
        // this.parameterChartForm.patchValue({
        //   ParameterName: this.parameterListTrend[0].Name || ''
        // })
      }
    }).catch((error) => {
      this.parameterListTrend = [];
    })
  }

  async presentToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 1000,
      color: 'secondary'
    });
    toast.present();
  }

  getPostAssemblyTrend(data) {
    console.log('modal popup chart request', data);
    this.loadingProvider.showLoaderWithWithoutTimer();
    this.conditionService.getTrendDetails(data).then((res: any) => {
      console.log('modal popup chart response', res);
      this.loadingProvider.hideLoader();
      if (res && res!.rows!.length <= 0) {
        this.presentToast('No Data Found');
      }
      new Promise((resolve) => {
        this.lineChartData[0].data = res!.rows.map(el => el.Line1) || [];
        this.lineChartLabels = res!.rows.map(el => moment(el.timestamp).format('hh:mm')) || [];
        resolve();
      }).then(() => {
        console.log('lineChartLabels', this.lineChartLabels);
        console.log('lineChartData', this.lineChartData);
        this.loadingProvider.hideLoader();
      })
    }).catch((error) => {
      this.loadingProvider.hideLoader();
      console.log('modal popup chart error', error);
      // this.parameterListTrend = [];
    })
  }

  selectedValueChart(value) {
    console.log("SelectedValue event");
    console.log(value);
    // this.parameterChartForm.controls.ParameterName.setValue(value.Name);
    new Promise((resolve) => {
      this.parameterChartForm.patchValue({
        ParameterThingName: value.ParameterThingName || ''
      })
      resolve();
    }).then(() => {
      console.log({ value });
      let data = {
        ParameterName: value.ActualName || this.parameterChartForm.controls.ParameterName.value || '',
        SelectedDate: moment(this.parameterChartForm.controls.SelectedDate.value).format('x') || '',
        ParameterThingName: value.ParameterThingName || this.parameterChartForm.controls.ParameterThingName.value || '',
      }
      console.log(this.parameterChartForm.value);
      console.log('assemblly treading first time call , throw selected value ')
      console.log({ data });
      this.getPostAssemblyTrend(data);
    })
  }


  getDate() {
    console.log('Get Date', this.parameterChartForm.controls.SelectedDate.value);
    if (new Date(this.parameterChartForm.controls.SelectedDate.value).toLocaleString() == "Invalid Date") {
      return new Date();
    } else {
      return new Date(this.parameterChartForm.controls.SelectedDate.value);
    }
  }

  showDate() {
    this.datePicker.show({
      date: this.getDate(),
      mode: 'date',
      maxDate: new Date().valueOf(),
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
    }).then(
      date => {
        console.log('Got date: ', date);
        new Promise((resolve) => {
          this.parameterChartForm.patchValue({
            SelectedDate: date || ''
          })
          resolve();
        }).then(() => {
          let data = {
            ParameterName: this.parameterChartForm.controls.ParameterName.value || '',
            SelectedDate: moment(this.parameterChartForm.controls.SelectedDate.value).format('x') || '',
            ParameterThingName: this.parameterChartForm.controls.ParameterThingName.value || '',
          }
          this.getPostAssemblyTrend(data);
        })
      },
      err => console.log('Error occurred while getting date: ', err)
    );
  }

  getProcessImage(data) {
    console.log("getProcessImage data",data);
    this.conditionService.getProcessImage(data).then(
      (getProcessImageResponse: any) => {
        console.log({ getProcessImageResponse });
        let base64: any = '';
        console.log(getProcessImageResponse!.rows[0].result);

        // base64 = `${getProcessImageResponse!.rows && getProcessImageResponse!.rows[0].result || ''}` || ``;
        base64 = `${getProcessImageResponse!.rows && getProcessImageResponse!.rows[0].result || ''}`;
        this.getProcessImg = `data:image/png;base64,${base64}`
      }).catch((getProcessImageError) => {
        console.log({ getProcessImageError });
      })
  }
}
