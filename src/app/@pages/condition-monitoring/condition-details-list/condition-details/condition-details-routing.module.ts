import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConditionDetailsPage } from './condition-details.page';

const routes: Routes = [
  {
    path: '',
    component: ConditionDetailsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConditionDetailsPageRoutingModule {}
