import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConditionDetailsPageRoutingModule } from './condition-details-routing.module';

import { ConditionDetailsPage } from './condition-details.page';
import { ComponentModule } from 'src/app/@components/component.module';
import { SharedModule } from 'src/app/@shared/shared.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConditionDetailsPageRoutingModule,
    ComponentModule,
    SharedModule
  ],
  declarations: [ConditionDetailsPage]
})
export class ConditionDetailsPageModule {}
