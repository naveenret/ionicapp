import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ConditionDetailsListPage } from './condition-details-list.page';

const routes: Routes = [
  {
    path: '',
    component: ConditionDetailsListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConditionDetailsListPageRoutingModule {}
