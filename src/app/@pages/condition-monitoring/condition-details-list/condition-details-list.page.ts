import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ConditionService } from 'src/app/@service/condition.service';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'condition-details-list',
  templateUrl: './condition-details-list.page.html',
  styleUrls: ['./condition-details-list.page.scss'],
})
export class ConditionDetailsListPage implements OnInit {
  items: any = [];
  constructor(
    public router: Router,
    public conditionService: ConditionService,
    public navCtrl: NavController
  ) { }

  ngOnInit() {
    this.getPostAssemblyMechineList();
  }

  getPostAssemblyMechineList() {
    this.conditionService.getPostAssemblyMechineList().then((res:any) => {
      this.items = res.rows || [];
    }).catch((error) => {
      this.items = [];
    })
  }



  itemClick(item) {
    console.log({item});
    let data = item || {};
    // this.navCtrl.navigateForward(['/layouts/condition-details',{ animationDirection: 'forward', queryParams: { item : data }  }]);
    this.router.navigate(['/layouts/condition-details'], { queryParams:  item});

   }
}
 