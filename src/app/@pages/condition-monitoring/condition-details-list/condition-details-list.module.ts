import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ConditionDetailsListPageRoutingModule } from './condition-details-list-routing.module';

import { ConditionDetailsListPage } from './condition-details-list.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ConditionDetailsListPageRoutingModule
  ],
  declarations: [ConditionDetailsListPage]
})
export class ConditionDetailsListPageModule {}
