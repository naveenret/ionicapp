import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DowntimeReasonPageRoutingModule } from './downtime-reason-routing.module';

import { DowntimeReasonPage } from './downtime-reason.page';
import { SharedModule } from 'src/app/@shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    DowntimeReasonPageRoutingModule
  ],
  declarations: [DowntimeReasonPage]
})
export class DowntimeReasonPageModule {}
