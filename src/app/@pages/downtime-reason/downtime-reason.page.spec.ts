import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DowntimeReasonPage } from './downtime-reason.page';

describe('DowntimeReasonPage', () => {
  let component: DowntimeReasonPage;
  let fixture: ComponentFixture<DowntimeReasonPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DowntimeReasonPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DowntimeReasonPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
