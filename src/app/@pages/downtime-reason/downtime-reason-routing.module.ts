import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DowntimeReasonPage } from './downtime-reason.page';

const routes: Routes = [
  {
    path: '',
    component: DowntimeReasonPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DowntimeReasonPageRoutingModule {}
