import { Component, OnInit } from '@angular/core';
import { HomeService } from 'src/app/@service/home.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'downtime-reason',
  templateUrl: './downtime-reason.page.html',
  styleUrls: ['./downtime-reason.page.scss'],
})
export class DowntimeReasonPage implements OnInit {

  getDowntimeDetailDataList: any[] =[];
  currentActiveItem:any;

  constructor(
    public homeService: HomeService,
    public activatedRoute:ActivatedRoute
  ) { }

  ngOnInit() {
    this.getQueryParams();
    // this.getDowntimeDetailList();
  }

  getQueryParams() {
    this.activatedRoute.queryParams.subscribe(res => {
      console.log(res);
      // console.log(JSON.parse(res.item));
      new Promise((resolve) => {
        this.currentActiveItem = res.item || {};
        resolve();
      }).then(() => {
        this.getDowntimeDetailList();
      })
    })
  }

  getDowntimeDetailList() {
    this.homeService.getDowntimeDetailList().then((getDowntimeDetailDataResponse:any) => {
      this.getDowntimeDetailDataList = getDowntimeDetailDataResponse!.rows || [];
      console.log({ getDowntimeDetailDataResponse });
      console.log(getDowntimeDetailDataResponse.rows);
    }).catch((getDowntimeDetailError) => {
      this.getDowntimeDetailDataList = [];
      console.log({ getDowntimeDetailError });
    })
  }

}
