import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LayoutPage } from './layout.page';
import { AuthGuardGuard } from 'src/app/@core/guard/auth-guard.guard';

const routes: Routes = [
  {
    path: '',
    component: LayoutPage,
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      },
      {
        path: 'home',
        loadChildren: () => import('../../@pages/home/home.module').then(m => m.HomePageModule),
        canActivate:[AuthGuardGuard]
      },
      {
        path: 'production',
        loadChildren: () => import('../../@pages/production/production.module').then(m => m.ProductionPageModule),
        canActivate:[AuthGuardGuard]
      },
      {
        path: 'condition-monitoring',
        loadChildren: () => import('../../@pages/condition-monitoring/condition-monitoring.module').then(m => m.ConditionMonitoringPageModule),
        canActivate:[AuthGuardGuard]
      },
      {
        path: 'traceability',
        loadChildren: () => import('../../@pages/traceability/traceability.module').then(m => m.TraceabilityPageModule),
        canActivate:[AuthGuardGuard]
      }, {
        path: 'analytics',
        loadChildren: () => import('../../@pages/analytics/analytics.module').then(m => m.AnalyticsPageModule),
        canActivate:[AuthGuardGuard]
      }, {
        path: 'planning',
        loadChildren: () => import('../../@pages/planning/planning.module').then(m => m.PlanningPageModule),
        canActivate:[AuthGuardGuard]
      },
      {
        path: 'summary',
        loadChildren: () => import('../../@pages/summary/summary.module').then(m => m.SummaryPageModule),
        canActivate:[AuthGuardGuard]
      },
      {
        path: 'nopath',
        loadChildren: () => import('../../@pages/production/nopath/nopath.module').then(m => m.NopathPageModule),
        canActivate:[AuthGuardGuard]
      },
      {
        path: 'post-assembly-details',
        loadChildren: () => import('../../@pages/traceability/post-assembly-details/post-assembly-details.module').then(m => m.PostAssemblyDetailsPageModule),
        canActivate:[AuthGuardGuard]
      },
      {
        path: 'production-availability',
        loadChildren: () => import('../../@pages/production/production-availability/production-availability.module').then(m => m.ProductionAvailabilityPageModule),
        canActivate:[AuthGuardGuard]
      },
      {
        path: 'production-performance',
        loadChildren: () => import('../../@pages/production/production-performance/production-performance.module').then(m => m.ProductionPerformancePageModule),
        canActivate:[AuthGuardGuard]
      },
      {
        path: 'production-quality',
        loadChildren: () => import('../../@pages/production/production-quality/production-quality.module').then( m => m.ProductionQualityPageModule),
        canActivate:[AuthGuardGuard]
      },
      {
        path: 'condition-details-list',
        loadChildren: () => import('../../@pages/condition-monitoring/condition-details-list/condition-details-list.module').then( m => m.ConditionDetailsListPageModule),
        canActivate:[AuthGuardGuard]
      },
      {
        path: 'condition-details',
        loadChildren: () => import('../../@pages/condition-monitoring/condition-details-list/condition-details/condition-details.module').then( m => m.ConditionDetailsPageModule),
        canActivate:[AuthGuardGuard]
      },
      {
        path: 'traceablity-details',
        loadChildren: () => import('../../@pages/traceability/traceablity-details/traceablity-details.module').then( m => m.TraceablityDetailsPageModule),
        canActivate:[AuthGuardGuard]
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LayoutPageRoutingModule { }
