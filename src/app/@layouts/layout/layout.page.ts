import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { LoginService } from 'src/app/@service/login.service';
import { LoadingProvider } from 'src/app/@core/mock/loading';

@Component({
  selector: 'app-layout',
  templateUrl: './layout.page.html',
  styleUrls: ['./layout.page.scss'],
})
export class LayoutPage implements OnInit {

  homeActive: any = 'block';
  productionActive: any = 'none';
  conditionActive: any = 'none';
  traceActive: any = 'none';
  analyticsActive: any = 'none';
  planningActive: any = 'none';
  summaryActive: any = 'none';
  logoutActive: any = 'block';

  userData: any;
  layoutList: any[] = [];
  constructor(public navCtrl: NavController,
    public loginService: LoginService,
    public loadingProvider: LoadingProvider
  ) { }
  async ngOnInit() {
    // await this.loadingProvider.showLoaderWithBackDropWithoutTimer();
    this.userData = await JSON.parse(localStorage.getItem('userData'));
    await this.getLayoutsBasedOnUser(
      {
        "UserName": this.userData!.UserName || '',
        "UserGroup": this.userData!.UserGroup || '',
        "Email": this.userData!.Email || ''
      })
    // await this.loadingProvider.hideLoader();
  }



  getLayoutsBasedOnUser(data) {
    console.log('getLayoutsBasedOnUser request', data)
    this.loginService.getLayoutsBasedOnUser(data).then((res: any) => {
      console.log('getLayoutsBasedOnUser response', res);
      this.layoutList = res!.rows || [];
      this.layoutList.forEach((el) => {
        ;
        switch (el.ModuleName) {
          case 'Home':
            this.homeActive = 'block';
            break;
          case "Production":
            this.productionActive = 'block';
            break;
          case "Condition Monitoring":
            this.conditionActive = 'block';
            break;
          case "Traceability":
            this.traceActive = 'block';
            break;
          case "Analytics":
            this.analyticsActive = 'block';
            break;
          case "Planning":
            this.planningActive = 'block';
            break;
          case "Summary":
            this.summaryActive = 'block';
            break;
          case "Logout":
            this.logoutActive = 'block'; 
            break;

        }
      })
    }).catch((err: any) => {
      this.layoutList = []
      console.log('getLayoutsBasedOnUser error', err);

    })
  }
  logout() {
    console.log("logout clicked");
    localStorage.setItem('isUserLoggedIn', JSON.stringify(false));
    localStorage.removeItem('userData');
    console.log(JSON.parse(localStorage.getItem('isUserLoggedIn')));
    this.navCtrl.navigateRoot('/login');
  }
}
